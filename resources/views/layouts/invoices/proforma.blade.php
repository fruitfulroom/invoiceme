<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proforma Invoice @if($order->received)- Signed @endif</title>
    <style>
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #5D6975;
            text-decoration: underline;
        }

        body {
            position: relative;
            margin: 0 auto;
            color: #001028;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 12px;
            font-family: Arial;
        }

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid #5D6975;
            border-bottom: 1px solid #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(../../../../assets/invoices/dimension.png);
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 72px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            table-layout: fixed;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices {
            padding-bottom: 20px;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            border-top: 1px solid #C1CED9;
            padding: 8px 0;
            text-align: center;
        }

        #signature {
            height: 29px;
        }

        #signature img {
            margin-bottom: -6px;
            margin-top: -42px;
            height: 70px;
        }

        .certified-item {
            font-size: 24px;
            padding: 35px;
        }

        .certified-title {
            border-top: 1px solid black;
            font-size: 15px;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
        <img src="{{ $order->getSellingCompany()->image }}">
    </div>
    <h1>Proforma Invoice @if($order->received)- Signed @endif</h1>
    <div style="display: flex">
        <div id="project" style="flex: 1">
            <div><span>CLIENT</span> {{ $purchaser->name }}</div>

            @if($purchaser->address)
                <div><span>ADDRESS</span> {{ $purchaser->address }}</div>
            @endif

            @if($purchaser->email)
                <div><span>EMAIL</span> {{ $purchaser->email }}</div>
            @endif

            <div><span>DATE ISSUED</span> {{ $order->created_at->toDateTimeString() }}</div>

            <div><span>INVOICE #</span> {{ $order->id }}</div>

            @if($purchaser->phone)
                <div><span>PHONE</span> {{ $purchaser->phone }}</div>
            @endif
        </div>
        <div id="company" class="clearfix">
            <div style="font-weight: bold">Company details</div>
            <div>{{ $order->getSellingCompany()->name }}</div>
            <div>{{ $order->getSellingCompany()->city }}</div>
            <div>{{ $order->getSellingCompany()->address }}</div>
            <div>{{ $order->getSellingCompany()->phone }}</div>
            <div>{{ $order->getSellingCompany()->website }}</div>
        </div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="service">PRODUCT</th>
            <th class="desc">DESCRIPTION</th>
            <th>UNIT OF MEASURE</th>
            <th>QTY</th>
            <th>UNIT PRICE</th>
        </tr>
        </thead>
        <tbody>
        @foreach($order->orderedProducts as $orderedProduct)
            <tr>
                <td class="service">{{ $orderedProduct->product->name }}</td>
                <td class="desc">{{ $orderedProduct->product->description }}</td>
                <td class="unit">{{ $orderedProduct->product->weight_unit }}</td>
                <td class="qty">{{ $orderedProduct->quantity }}</td>
                <td class="unit">{{ $orderedProduct->product->formatted_price }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="4" class="grand total">SUBTOTAL</td>
            <td class="grand total">{{ formattedPrice($order->billing_subtotal) }}</td>
        </tr>
        <tr>
            <td colspan="4" class="grand total">TAXABLE</td>
            <td class="grand total">{{ formattedPrice($order->billing_subtotal) }}</td>
        </tr>
        <tr>
            <td colspan="4">TAX RATE</td>
            <td class="total">20%</td>
        </tr>
        <tr>
            <td colspan="4">TAX</td>
            <td class="total">{{ formattedPrice($order->billing_tax) }}</td>
        </tr>
        <tr>
            <td colspan="4" class="total">TOTAL</td>
            <td class="total">{{ formattedPrice($order->billing_total) }}</td>
        </tr>
        </tbody>
    </table>
    @if($order->notes)
        <div id="notices">
            <div>NOTES:</div>
            <div class="notice">{{ $order->notes }}</div>
        </div>
    @endif
    @if($order->received)
        <hr>

        <div class="clearfix">
            <div style="font-size: 24px; text-align: center">Certified that the above is true and correct.</div>
            <div style="display: flex">
                <div id="project" style="flex: 1">
                    <div class="certified-item">
                        <div>{{ $order->getSellingCompany()->name }}</div>
                        <div class="certified-title">Company name</div>
                    </div>
                </div>
                <div id="project" style="flex: 1">
                    <div class="certified-item">
                        <div id="signature">
                            <img src="{{ $order->getSellingCompany()->signature }}">
                        </div>
                        <div class="certified-title">Signature</div>
                    </div>
                </div>
                <div id="company" class="clearfix">
                    <div class="certified-item">
                        <div>{{ $order->updated_at->toDateString() }}</div>
                        <div class="certified-title">Date</div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
