@php
    $relationshipData = (isset($data)) ? $data : $dataTypeContent;

    $selected_values = isset($relationshipData) ? $relationshipData->belongsToMany($options->model, $options->pivot_table, $options->foreign_pivot_key ?? null, $options->related_pivot_key ?? null, $options->parent_key ?? null, $options->key)->get()->map(function ($item, $key) use ($options) {
        return [
            'name' => $item->{$options->label},
            'link' => '/admin/' . $options->table . '/' . $item->id,
        ];
    })->all() : array();
@endphp

@if($view == 'browse')
    @php
        $string_values = implode(", ", array_map(function ($selected_value) {
            return $selected_value['name'];
        }, $selected_values));

        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
    @endphp
    @if(empty($selected_values))
        <p>{{ __('voyager::generic.no_results') }}</p>
    @else
        <p>{{ $string_values }}</p>
    @endif
@else
    @if(empty($selected_values))
        <p>{{ __('voyager::generic.no_results') }}</p>
    @else
        <ul>
            @foreach($selected_values as $selected_value)
                <li><a href="{{ $selected_value['link'] }}">{{ $selected_value['name'] }}</a></li>
            @endforeach
        </ul>
    @endif
@endif
