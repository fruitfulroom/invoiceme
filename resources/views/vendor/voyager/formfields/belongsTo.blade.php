@php
    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
    $model = app($options->model);
    $query = $model::where($options->key,$relationshipData->{$options->column})->first();
@endphp

@if(isset($query))
    <a href="/admin/{{ $options->table }}/{{ $query->id }}">{{ $query->{$options->label} }}</a>
@else
    <p>{{ __('voyager::generic.no_results') }}</p>
@endif

