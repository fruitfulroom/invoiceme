@php
    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
    $model = app($options->model);

    $selected_values = $model::where($options->column, '=', $relationshipData->{$options->key})->get()->filter(function ($item) {
        return $item->documentable_type == 'App\Order';
    })->map(function ($item, $key) use ($options) {
        return $item->{$options->label};
    })->all();
@endphp

@if($view == 'browse')
    @php
        $string_values = implode(", ", $selected_values);
        if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
    @endphp
    @if(empty($selected_values))
        <p>{{ __('voyager::generic.no_results') }}</p>
    @else
        <p>{{ $string_values }}</p>
    @endif
@else
    @if(empty($selected_values))
        <p>{{ __('voyager::generic.no_results') }}</p>
    @else
        <ul>
            @foreach($selected_values as $selected_value)
                <li><a href="{{ asset('storage/' . $selected_value) }}"
                       target="_blank">{{ Str::afterLast($selected_value, '/') }}</a></li>
            @endforeach
        </ul>
    @endif
@endif
