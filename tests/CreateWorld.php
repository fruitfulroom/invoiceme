<?php

namespace Tests;

use App\Company;
use App\Product;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;

trait CreateWorld
{

    public $company, $secondCompany, $product, $trustedCompany, $trustedCompanyInstance, $companyProducts, $secondCompanyProducts, $authorizedCompany, $image;

    public function logMeIn($subscribed = false, $entry = Passport::class)
    {
        $user = factory(User::class);

        if ($subscribed) {
            $user = $user->state('subscribed');
        }

        $entry::actingAs($user->create());

        return $this;
    }

    public function createProduct()
    {
        $this->product = factory(Product::class)->create();

        return $this;
    }

    public function createCompany()
    {
        $this->company = factory(Company::class)->create();

        return $this;
    }

    public function createSecondCompany()
    {
        $this->secondCompany = factory(Company::class)->create();

        return $this;
    }

    public function createAuthorizedCompany()
    {
        $this->authorizedCompany = factory(Company::class)->create();

        return $this;
    }

    public function createTrustedCompany()
    {
        $this->trustedCompany = factory(Company::class)->create();

        $this->trustedCompanyInstance = $this->company->trusted_companies()
            ->create([
                'trusted_company_id' => $this->trustedCompany->id,
                'trusted_fund'       => 50000,
                'available'          => 50000,
                'trusted_period'     => '60',
            ]);

        return $this;
    }

    public function addMeAsEmployee($companyType)
    {
        $companyType->employees()
            ->create([
                'user_id' => auth()->user()->id,
            ]);

        return $this;
    }

    public function createProductsForCompany($quantity = 2)
    {
        $this->companyProducts = factory(Product::class, $quantity)->create([
            'company_id' => $this->company->id,
        ]);

        return $this;
    }

    public function createProductsForSecondCompany($quantity = 2)
    {
        $this->secondCompanyProducts = factory(Product::class, $quantity)->create([
            'company_id' => $this->secondCompany->id,
        ]);

        return $this;
    }

    public function createEnoughTrustFund()
    {
        $this->company->trusted_companies()
            ->create([
                'trusted_company_id' => $this->secondCompany->id,
                'trusted_fund'       => 5000000,
                'available'          => 5000000,
                'trusted_period'     => 60,
            ]);

        return $this;
    }

    public function makeCart($products = [], $type = 'default')
    {
        if (empty($products)) {
            $products = $this->companyProducts;
        }

        $products->each(function ($product) use ($type) {
            Cart::instance($type)
                ->add($product->id, $product->name, 1, $product->price / 100)
                ->associate(Product::class);
        });

        if ( ! empty($this->secondCompanyProducts)) {
            $this->secondCompanyProducts->each(function ($product) use ($type) {
                Cart::instance($type)
                    ->add($product->id, $product->name, 1, $product->price / 100)
                    ->associate(Product::class);
            });
        }

        return Cart::instance($type)
            ->content();
    }

    public function usesImageUpload()
    {
        $this->seed('MenusTableSeeder');
        $this->seed('VoyagerDeploymentOrchestratorSeeder');

        Storage::fake();

        $this->image = UploadedFile::fake()->image('image.jpg');

        return $this;
    }
}
