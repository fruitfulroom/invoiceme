<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ShowProductTest extends TestCase
{

    use RefreshDatabase;

    private $product;

    protected function setUp(): void
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();
    }

    /** @test */
    public function not_authenticated_user_may_not_access_the_product()
    {
        $response = $this->get('/api/products/' . $this->product->slug);

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_products()
    {
        Passport::actingAs(factory(User::class)->create());

        $response = $this->get('/api/products/' . $this->product->slug);

        $response->assertJson([
            'product' => [
                'name'            => $this->product->name,
                'slug'            => $this->product->slug,
                'subtitle'        => $this->product->subtitle,
                'price'           => $this->product->price,
                'quantity'        => $this->product->quantity,
                'company'         => [
                    'name' => $this->product->company->name,
                    'slug' => $this->product->company->slug,
                ],
            ],
        ]);
    }
}
