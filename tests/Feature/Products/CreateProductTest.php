<?php

namespace Tests\Feature;

use App\Company;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateProductTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_create_products()
    {
        $response = $this->post('/api/products');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_input_when_creating_a_product_product()
    {
        $this->logMeIn()->createCompany();

        $response = $this->post('/api/products', []);

        $response->assertJsonValidationErrors([
            'company_id', 'name', 'subtitle', 'description', 'price', 'quantity', 'confirmed',
        ]);

        $this->assertDatabaseMissing('products', [
            'user_id'    => auth()->user()->id,
            'company_id' => $this->company->id,
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_create_product()
    {
        $this->usesImageUpload();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        Passport::actingAs($companyOwner);

        $product = factory(Product::class)->make()->toArray();
        $product['company_id'] = $company->id;
        $product['image'] = $this->image;

        $this->post('/api/products', $product);

        $this->assertDatabaseHas('products', [
            'user_id'    => auth()->user()->id,
            'company_id' => $company->id,
            'name'       => $product['name'],
        ]);
    }

    /** @test */
    public function a_product_image_is_required()
    {
        $this->usesImageUpload();

        $company = factory(Company::class)->create([
            'user_id' => factory(User::class)->state('subscribed')->create()->id,
        ]);

        Passport::actingAs($company->owner);

        $product = factory(Product::class)->make()->toArray();
        $product['company_id'] = $company->id;
        $product['image'] = $this->image;

        $this->post('/api/products', $product);

        $this->assertStringStartsWith('products', Product::first()->image);
    }

    /** @test */
    public function a_product_gallery_may_be_submitted()
    {
        $this->usesImageUpload();

        $company = factory(Company::class)->create([
            'user_id' => factory(User::class)->state('subscribed')->create()->id,
        ]);

        Passport::actingAs($company->owner);

        $image1 = UploadedFile::fake()->image('image1.jpg');
        $image2 = UploadedFile::fake()->image('image2.jpg');

        $product = factory(Product::class)->make()->toArray();
        $product['company_id'] = $company->id;
        $product['image'] = $this->image;
        $product['gallery'] = [$image1, $image2];

        $this->post('/api/products', $product);

        foreach (\GuzzleHttp\json_decode(Product::first()->gallery) as $image) {
            $this->assertStringStartsWith('products', $image);
        }
    }
}
