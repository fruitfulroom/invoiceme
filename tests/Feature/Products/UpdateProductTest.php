<?php

namespace Tests\Feature;

use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Tests\CreateWorld;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_update_products()
    {
        $this->createProduct();

        $response = $this->patch('/api/products/' . $this->product->slug);

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_input_when_updating_a_product_product()
    {
        $this->logMeIn()->createProduct();

        $response = $this->patch('/api/products/' . $this->product->slug, [
            'name'        => true,
            'subtitle'    => 233,
            'description' => false,
            'price'       => 'should be integer',
            'quantity'    => 'should be integer',
            'confirmed'   => 'should be a boolean',
        ]);

        $response->assertJsonValidationErrors([
            'name',
            'subtitle',
            'description',
            'price',
            'quantity',
            'confirmed',
        ]);

        $this->assertDatabaseMissing('products', [
            'user_id' => auth()->user()->id,
            'slug'    => $this->product->slug,
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_update_product()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $this->patch('/api/products/' . $product->slug, [
            'name'        => 'New name',
            'subtitle'    => 'New subtitle',
            'description' => 'New description',
            'price'       => 777,
            'quantity'    => 7,
            'confirmed'   => true,
        ]);

        $this->assertDatabaseHas('products', [
            'user_id'     => auth()->user()->id,
            'name'        => 'New name',
            'subtitle'    => 'New subtitle',
            'description' => 'New description',
            'price'       => 777,
            'quantity'    => 7,
            'confirmed'   => true,
        ]);
    }

    /** @test */
    public function a_product_image_may_be_updated()
    {
        $this->logMeIn()->usesImageUpload();

        $product = factory(Product::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $this->patch('/api/products/' . $product->slug, [
            'image' => $this->image,
        ]);

        $this->assertStringStartsWith('products', $product->fresh()->image);
    }

    /** @test */
    public function a_product_gallery_may_be_updated()
    {
        $this->logMeIn()->usesImageUpload();

        $product = factory(Product::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $image1 = UploadedFile::fake()->image('image1.jpg');
        $image2 = UploadedFile::fake()->image('image2.jpg');

        $this->patch('/api/products/' . $product->slug, [
            'image' => $this->image,
            'gallery' => [$image1, $image2],
        ]);

        foreach (\GuzzleHttp\json_decode($product->fresh()->gallery) as $image) {
            $this->assertStringStartsWith('products', $image);
        }
    }
}
