<?php

namespace Tests\Feature;

use App\Company;
use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_delete_products()
    {
        $this->createProduct();

        $response = $this->delete('/api/products/' . $this->product->slug);

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->delete('/api/products/none-existing-slug');

        $this->assertResponseExceptionIsThrown($response,
            'No query results for model [App\Product] none-existing-slug');
    }

    /** @test */
    public function an_authenticated_user_may_delete_products()
    {
        $this->logMeIn()->createProduct();

        $product = factory(Product::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $this->assertDatabaseHas('products', [
            'id'   => $product->id,
            'name' => $product->name,
        ]);

        $this->delete('/api/products/' . $product->slug);

        $this->assertDatabaseMissing('products', [
            'id'         => $product->id,
            'name'       => $product->name,
            'deleted_at' => null,
        ]);
    }
}
