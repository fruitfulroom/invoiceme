<?php

namespace Tests\Feature;

use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Tests\CreateWorld;
use Tests\TestCase;

class GetProductsTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_the_products()
    {
        $response = $this->get('/api/products');

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_products()
    {
        $this->logMeIn();

        $response = $this->get('/api/products');

        $response->assertJson([
            'categories'         => [],
            'paginated_products' => [
                'data' => [],
            ],
            'selected_category'  => false,
        ]);
    }

    /** @test */
    public function products_may_be_filtered_by_category()
    {
        $this->logMeIn();

        $productWithCategories = factory(Product::class)
            ->state('with_categories')
            ->create();

        $selectedCategory = $productWithCategories->fresh()->categories->first();

        $response = $this->get('/api/products?category=' . $selectedCategory->slug);

        $response->assertJson([
            'selected_category' => $selectedCategory->slug,
        ]);
    }

    /** @test */
    public function products_may_be_not_filtered_by_category_with_empty_value()
    {
        $this->logMeIn();

        factory(Product::class)
            ->state('with_categories')
            ->create();

        $response = $this->get('/api/products?category=');

        $response->assertJsonValidationErrors(['category']);
    }

    /** @test */
    public function products_may_be_not_filtered_by_category_with_none_existing_category()
    {
        $this->logMeIn();

        factory(Product::class)
            ->state('with_categories')
            ->create();

        $response = $this->get('/api/products?category=none_existing_category');

        $response->assertJsonValidationErrors(['category']);
    }

    /** @test */
    public function products_may_be_sorted_from_highest_to_lowest()
    {
        $this->logMeIn();

        $lowPricedProduct = factory(Product::class)->create([
            'price' => 1,
        ]);

        $mediumPricedProduct = factory(Product::class)->create([
            'price' => 2,
        ]);

        $highPricedProduct = factory(Product::class)->create([
            'price' => 3,
        ]);

        $response = $this->get('/api/products?sort=high');

        $response->assertSeeInOrder([
            $highPricedProduct->name,
            $mediumPricedProduct->name,
            $lowPricedProduct->name,
        ]);
    }

    /** @test */
    public function products_may_not_sorted_from_lowest_to_highest()
    {
        $this->logMeIn();

        $lowPricedProduct = factory(Product::class)->create([
            'price' => 1,
        ]);

        $mediumPricedProduct = factory(Product::class)->create([
            'price' => 2,
        ]);

        $highPricedProduct = factory(Product::class)->create([
            'price' => 3,
        ]);

        $response = $this->get('/api/products?sort=low');

        $response->assertSeeInOrder([
            $lowPricedProduct->name,
            $mediumPricedProduct->name,
            $highPricedProduct->name,
        ]);
    }

    /** @test */
    public function it_uses_cache_for_getting_products()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProduct();

        $this->get('/api/products');

        Cache::shouldReceive('tags')
            ->once()
            ->with(['products']);

        $this->get('/api/products');
    }

    /** @test */
    public function it_uses_cache_for_getting_filtered_and_sorted_products()
    {
        $this->logMeIn();

        $productWithCategories = factory(Product::class)
            ->state('with_categories')
            ->create();

        $selectedCategory = $productWithCategories->fresh()->categories->first();

        $this->get("/api/products?category=$selectedCategory->slug&sort=high");

        Cache::shouldReceive('tags')
            ->once()
            ->with(['products']);

        $this->get("/api/products?category=$selectedCategory->slug&sort=high");
    }

    /** @test */
    public function it_invalidates_the_cache_when_product_is_updated()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProduct()
            ->get('/api/products')
            ->assertJson([
                'paginated_products' => [
                    'data' => [
                        [
                            'name' => $this->product->name,
                        ],
                    ],
                ],
            ]);

        $this->product->update([
            'name' => 'Updated name',
        ]);

        $this->get('/api/products')
            ->assertJson([
                'paginated_products' => [
                    'data' => [
                        [
                            'name' => 'Updated name',
                        ],
                    ],
                ],
            ]);
    }

    /** @test */
    public function it_invalidates_the_cache_when_product_is_deleted()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProduct()
            ->get('/api/products')
            ->assertJson([
                'paginated_products' => [
                    'data' => [
                        [
                            'name' => $this->product->name,
                        ],
                    ],
                ],
            ]);

        $this->product->delete();

        $this->get('/api/products')
            ->assertJsonMissing(['name' => $this->product->name]);
    }

    /** @test */
    public function it_invalidates_the_cache_when_product_is_created()
    {
        $response = $this->logMeIn()
            ->createCompany()
            ->get('/api/products');

        $this->assertCount(0, $response->json()['paginated_products']['data']);

        $this->createProduct()
            ->get('/api/products')
            ->assertJson([
                'paginated_products' => [
                    'data' => [
                        [
                            'name' => $this->product->name,
                        ],
                    ],
                ],
            ]);
    }
}
