<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ShowRelatedProductsTest extends TestCase
{

    use RefreshDatabase;

    private $product;

    protected function setUp(): void
    {
        parent::setUp();

        $this->product = factory(Product::class)->create();

        factory(Product::class, 7)->create();
    }

    /** @test */
    public function not_authenticated_user_may_not_access_the_related_product()
    {
        $response = $this->get('/api/products/related?slug=' . $this->product->slug);

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_related_products()
    {
        Passport::actingAs(factory(User::class)->create());

        $response = $this->post('/api/products/related?slug=' . $this->product->slug);

        $response->assertJson([
            'products' => [],
            'criterias' => [],
        ]);
    }

    /** @test */
    public function it_returns_six_related_products()
    {
        Passport::actingAs(factory(User::class)->create());

        $response = $this->post('/api/products/related?slug=' . $this->product->slug);

        $this->assertCount(6, $response->json()['products']);
    }

//    /** @test */
//    public function it_returns_related_products_excluding_the_current()
//    {
//    }
//
//    /** @test */
//    public function it_returns_related_products_from_the_same_company_if_any()
//    {
//    }
//
//    /** @test */
//    public function it_returns_related_products_from_the_same_category_if_any()
//    {
//    }
}
