<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class UpdateTrustedFundTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_update_any_trusted_fund()
    {
        $response = $this->patch('/api/trusted_fund/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function updating_trusted_fund_must_be_validated()
    {
        $this->logMeIn()->createCompany()->createAuthorizedCompany();

        $this->company->trusted_companies()->create([
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 500,
            'trusted_period'     => 60,
        ]);

        $response = $this->patch('/api/trusted_fund/' . $this->company->trusted_companies->first()->id);

        $response->assertJsonValidationErrors([
            'trusted_fund', 'trusted_period',
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_update_trusted_fund()
    {
        $this->createCompany()->createAuthorizedCompany();

        Passport::actingAs($this->company->owner);

        $this->company->trusted_companies()->create([
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 500,
            'trusted_period'     => 60,
        ]);

        $this->patch('/api/trusted_fund/' . $this->company->trusted_companies->first()->id, [
            'trusted_fund'   => 1000,
            'trusted_period' => 30,
        ]);

        $this->assertDatabaseHas('trusted_companies', [
            'company_id'         => $this->company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 1000,
            'available'          => 1000,
            'trusted_period'     => 30,
        ]);
    }
}
