<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class DeleteTrustedFundTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_delete_trusted_funds()
    {
        $response = $this->delete('/api/trusted_fund/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->delete('/api/trusted_fund/' . $someDoesNotExistingId = 77);

        $this->assertResponseExceptionIsThrown($response, 'No query results for model [App\TrustedCompany] 77');
    }

    /** @test */
    public function an_authenticated_user_may_delete_trusted_funds()
    {
        $this->createCompany()->createAuthorizedCompany();

        Passport::actingAs($this->company->owner);

        $this->company->trusted_companies()->create([
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 500,
            'trusted_period'     => 60,
        ]);

        $this->assertDatabaseHas('trusted_companies', [
            'trusted_company_id' => $this->authorizedCompany->id,
            'company_id'         => $this->company->id,
        ]);

        $this->delete('/api/trusted_fund/' . $this->company->trusted_companies->first()->id);

        $this->assertDatabaseMissing('trusted_companies', [
            'trusted_company_id' => $this->authorizedCompany->id,
            'company_id'         => $this->company->id,
        ]);
    }
}
