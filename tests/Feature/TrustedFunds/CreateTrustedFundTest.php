<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateTrustedFundTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_create_trusted_fund()
    {
        $response = $this->post('/api/trusted_fund');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->post('/api/trusted_fund', []);

        $response->assertJsonValidationErrors([
            'company_id',
            'trusted_company_id',
            'trusted_fund',
            'trusted_period',
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_create_trusted_fund()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        Passport::actingAs($companyOwner);

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertJson([
            'trusted_fund' => [
                'company_id'         => $company->id,
                'trusted_company_id' => $this->authorizedCompany->id,
                'trusted_fund'       => 5000,
                'trusted_period'     => 60,
                'available'          => 5000,
            ],
        ]);

        $this->assertDatabaseHas('trusted_companies', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
            'available'          => 5000,
        ]);
    }
}
