<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class GetTrustedFundTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_the_trusted_fund()
    {
        $this->createCompany();

        $response = $this->get('/api/trusted_fund/' . $this->company->id);

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_requires_valid_data_for_accessing_the_trusted_fund()
    {
        $this->logMeIn()->createCompany();

        $response = $this->get('/api/trusted_fund/' . $this->company->id);

        $response->assertJsonValidationErrors('authorized_company_id');
    }

    /** @test */
    public function an_authenticated_user_may_access_the_trusted_fund()
    {
        $this->logMeIn()->createCompany()->createAuthorizedCompany();

        $response = $this->get('/api/trusted_fund/' . $this->company->id . '?authorized_company_id=' . $this->authorizedCompany->id);

        $response->assertJson(['trusted_fund' => []]);

        $this->company->trusted_companies()->create([
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 500,
            'trusted_period'     => 60,
        ]);

        $this->company->employees()->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->get('/api/trusted_fund/' . $this->company->id . '?authorized_company_id=' . $this->authorizedCompany->id);

        $response->assertJson([
            'trusted_fund' => [
                'company_id'         => $this->company->id,
                'trusted_company_id' => $this->authorizedCompany->id,
            ],
        ]);
    }
}
