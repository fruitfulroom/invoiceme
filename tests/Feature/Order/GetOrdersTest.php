<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class GetOrdersTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function not_authenticated_user_may_not_access_the_orders()
    {
        $response = $this->get('/api/orders');

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_orders()
    {
        $user = factory(User::class)->state('with_orders')->create();

        Passport::actingAs($user);

        $response = $this->get('/api/orders');

        $response->assertJson([
            'orders' => [
                [
                    'user_id'         => auth()->user()->id,
                    'payment_gateway' => 'stripe',
                ],
                [
                    'user_id'         => auth()->user()->id,
                    'payment_gateway' => 'stripe',
                ],
            ],
        ]);
    }
}
