<?php

namespace Tests\Feature;

use App\Http\Payments\FakePayment;
use App\Notifications\OrderPayedNotification;
use App\Notifications\OrderReceivedNotification;
use App\Notifications\OrderShippedNotification;
use App\Order;
use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class UpdateOrderTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_update_any_order()
    {
        $response = $this->patch('/api/orders/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function updating_order_must_be_validated()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $response = $this->patch('/api/orders/' . auth()->user()->orders()->first()->id, [
            'shipped' => 'string but should be boolean',
        ]);

        $this->assertNotValidated($response);
    }

    /** @test */
    public function an_authenticated_user_may_update_order()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Notification::fake();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        $this->assertDatabaseHas('orders', [
            'user_id' => $userOrderOwner->id,
            'shipped' => false,
        ]);

        Passport::actingAs($this->company->owner);

        $this->patch('/api/orders/' . $userOrderOwner->id, [
            'shipped'     => true,
            'expected_at' => '05/28/2020 3:04 PM',
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id'     => $userOrderOwner->id,
            'shipped'     => true,
            'expected_at' => '2020-05-28 15:04:00',
        ]);

        Notification::assertSentTo(Order::first()->user, OrderShippedNotification::class);

        $this->patch('/api/orders/' . $userOrderOwner->id, [
            'payed' => true,
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => $userOrderOwner->id,
            'payed'   => true,
        ]);

        Notification::assertSentTo(Order::first()->user, OrderPayedNotification::class);

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'shipped' => false,
        ]);

        $response->assertJsonValidationErrors([
            'shipped' => 'Shipped status can not be reverted!',
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => $userOrderOwner->id,
            'shipped' => true,
        ]);
    }

    /** @test */
    public function disallows_buyers_to_change_shipped_status()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'shipped'     => true,
            'expected_at' => '05/28/2020 3:04 PM',
        ]);

        $response->assertJsonValidationErrors([
            'shipped' => 'Only seller company owner or employees may proceed!',
        ]);
    }

    /** @test */
    public function disallows_buyers_to_change_payed_status()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        $userOrderOwner->update([
            'payed' => false,
        ]);

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'payed' => true,
        ]);

        $response->assertJsonValidationErrors([
            'payed' => 'Only seller company owner or employees may proceed!',
        ]);
    }

    /** @test */
    public function updating_shipped_value_requires_date()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        Passport::actingAs($this->company->owner);

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'shipped' => true,
        ]);

        $response->assertJsonValidationErrors(['expected_at']);

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'shipped'     => true,
            'expected_at' => null,
        ]);

        $response->assertJsonValidationErrors(['expected_at']);
    }

    /** @test */
    public function disallows_none_buyers_setting_received_status()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        Passport::actingAs($this->company->owner);

        $response = $this->patch('/api/orders/' . $userOrderOwner->id, [
            'received' => true,
        ]);

        $response->assertJsonValidationErrors([
            'received' => 'Only buyers may proceed!',
        ]);
    }

    /** @test */
    public function buyers_may_set_received_status()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        Notification::fake();

        Cart::checkout(new FakePayment(), [
            'company_id' => $this->company->id,
        ]);

        $userOrderOwner = auth()->user()->orders()->first();

        $this->patch('/api/orders/' . $userOrderOwner->id, [
            'received' => true,
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id'  => $userOrderOwner->id,
            'received' => true,
        ]);

        Notification::assertSentTo(Order::first()->getSellingCompany()->owner, OrderReceivedNotification::class);
    }
}
