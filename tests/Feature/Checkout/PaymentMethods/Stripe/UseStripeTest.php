<?php

namespace Tests\Feature;

use App\Company;
use App\Mail\OrderPlaced;
use App\Product;
use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\CreateWorld;
use Tests\TestCase;

class UseStripeTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_throws_exception_if_payment_method_id_is_not_set()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        $response = $this->post('/api/checkout', [
            'gateway'    => 'Stripe',
            'company_id' => $this->company->id,
        ]);

        $this->assertResponseExceptionIsThrown($response, 'Invalid payment method id provided!');

        $response = $this->post('/api/checkout', [
            'gateway'           => 'Stripe',
            'company_id'        => $this->company->id,
            'payment_method_id' => '',
        ]);

        $response->assertJsonValidationErrors('payment_method_id');
    }
}
