<?php

namespace Tests\Feature;

use App\Mail\OrderPlaced;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\CreateWorld;
use Tests\TestCase;

class FakePaymentTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function company_products_can_be_checkout()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        $response = $this->post('/api/checkout', [
            'gateway'    => 'Fake',
            'company_id' => $this->company->id,
        ]);

        $response->assertStatus(201);

        $response->assertJson([
            'success' => true,
            'message' => 'Successfully charged the client!',
        ]);
    }

    /** @test */
    public function it_saves_the_order_when_checked_out()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        $this->post('/api/checkout', [
            'gateway'    => 'Fake',
            'company_id' => $this->company->id,
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id' => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_saves_the_products_when_checked_out()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        $this->post('/api/checkout', [
            'gateway'    => 'Fake',
            'company_id' => $this->company->id,
        ]);

        $this->assertDatabaseHas('order_product', [
            'order_id' => auth()->user()->orders()->first()->id,
        ]);
    }

    /** @test */
    public function it_removes_the_ordered_products_from_the_cart()
    {
        $this->logMeIn()->createCompany()->createSecondCompany()->createProductsForCompany()->createProductsForSecondCompany()->makeCart();

        $this->post('/api/checkout', [
            'gateway'    => 'Fake',
            'company_id' => $this->company->id,
        ]);

        $this->assertStringContainsString($this->secondCompanyProducts->first()->name,
            Cart::instance('default')->content()->first()->name);
        $this->assertStringContainsString($this->secondCompanyProducts->last()->name,
            Cart::instance('default')->content()->last()->name);
    }
}
