<?php

namespace Tests\Feature;

use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class UseTrustedFundTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function company_products_can_be_checkout()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->createProductsForSecondCompany()
            ->createEnoughTrustFund()
            ->makeCart();

        $this->assertSame(4, Cart::instance('default')->count());

        $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $this->assertStringNotContainsString($this->companyProducts->first()->name, Cart::instance('default')->content());
        $this->assertStringNotContainsString($this->companyProducts->last()->name, Cart::instance('default')->content());

        $this->assertStringContainsString($this->secondCompanyProducts->first()->name, Cart::instance('default')->content());
        $this->assertStringContainsString($this->secondCompanyProducts->last()->name, Cart::instance('default')->content());

        $this->assertSame(2, Cart::instance('default')->count());
    }

    /** @test */
    public function it_can_be_used_only_if_there_is_trusted_fund()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->makeCart();

        $response = $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->secondCompany->id,
        ]);

        $this->assertResponseExceptionIsThrown($response, 'The trust found does not exist!');
    }

    /** @test */
    public function it_can_be_used_only_if_there_is_enough_trusted_fund_fail()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->makeCart();

        $this->company->trusted_companies()
            ->create([
                'trusted_company_id' => $this->secondCompany->id,
                'trusted_fund'       => Cart::instance('default')->total(),
                'available'          => Cart::instance('default')->total() - 200,
                'trusted_period'     => 60,
            ]);

        $response = $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $response->assertJson(['message' => 'There are not enough funds!']);
    }

    /** @test */
    public function it_can_be_used_only_if_there_is_enough_trusted_fund()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->createEnoughTrustFund()
            ->makeCart();

        $response = $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $response->assertStatus(201);
    }

    /** @test */
    public function it_saves_the_orders_when_checked_out()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->createEnoughTrustFund()
            ->makeCart();

        $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id'         => auth()->user()->id,
            'payment_gateway' => 'trusted_fund',
            'payed'           => false,
        ]);
    }

    /** @test */
    public function it_saves_the_products_when_checked_out()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->createEnoughTrustFund()
            ->makeCart();

        $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $this->assertDatabaseHas('order_product', [
            'order_id' => auth()
                ->user()
                ->orders()
                ->first()->id,
        ]);
    }

    /** @test */
    public function it_reduces_the_trusted_fund_when_checked_out()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->createEnoughTrustFund()
            ->makeCart();

        $cartTotal = Cart::instance('default')->total();

        $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $this->assertDatabaseHas('trusted_companies', [
            'company_id'         => $this->company->id,
            'trusted_company_id' => $this->secondCompany->id,
            'available'          => 5000000 - $cartTotal * 100,
        ]);
    }

    /** @test */
    public function it_throws_exception_if_authorized_company_id_is_not_set()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProductsForCompany()
            ->makeCart();

        $response = $this->post('/api/checkout', [
            'gateway'    => 'TrustedFund',
            'company_id' => $this->company->id,
        ]);

        $this->assertResponseExceptionIsThrown($response, 'Invalid authorized company id provided!');

        $response = $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => '',
        ]);

        $response->assertJsonValidationErrors('authorized_company_id');
    }

    /** @test */
    public function it_does_no_increase_the_quantity_when_error_occurs()
    {
        $this->logMeIn()
            ->createCompany()
            ->createSecondCompany()
            ->createProductsForCompany()
            ->makeCart();

        $this->company->trusted_companies()
            ->create([
                'trusted_company_id' => $this->secondCompany->id,
                'trusted_fund'       => Cart::instance('default')->total(),
                'available'          => Cart::instance('default')->total() - 200,
                'trusted_period'     => 60,
            ]);

        $this->post('/api/checkout', [
            'gateway'               => 'TrustedFund',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->company->trusted_companies->first()->trusted_company_id,
        ]);

        $response = $this->get('/api/cart?type=default');

        $cartItems = collect($response->json()['carts'][$this->company->id]['cart']['content'])->values();

        $cartItems->each(function ($item) {
            $this->assertSame(1, $item['qty']);
        });
    }
}
