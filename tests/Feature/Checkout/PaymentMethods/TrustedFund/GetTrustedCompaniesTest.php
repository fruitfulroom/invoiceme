<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class GetTrustedCompaniesTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_the_trusted_companies()
    {
        $this->createCompany();

        $response = $this->get('/api/trusted_fund/' . $this->company->id);

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_trusted_companies()
    {
        $this
            ->logMeIn()
            ->createCompany()
            ->createTrustedCompany()
            ->createProductsForCompany()
            ->makeCart();

        $route = '/api/trusted_fund/'
            . $this->company->id
            . '?authorized_company_id='
            . $this->trustedCompany->id;

        $this->get($route)->assertJson([
            'trusted_fund' => [],
        ]);

       $this->addMeAsEmployee($this->trustedCompany);

        $this->get($route)->assertJson([
            'trusted_fund' => [
                'company_id'         => $this->company->id,
                'trusted_company_id' => $this->trustedCompany->id,
                'trusted_fund'      => '50000',
                'trusted_period'     => '60',
            ],
        ]);
    }

    /** @test */
    public function getting_trusted_companies_requires_authorized_company_id()
    {
        $this
            ->logMeIn()
            ->createCompany()
            ->createTrustedCompany()
            ->createProductsForCompany()
            ->makeCart();

        $response = $this->get('/api/trusted_fund/' . $this->company->id);

        $response->assertJsonValidationErrors([
            'authorized_company_id',
        ]);
    }
}
