<?php

namespace Tests\Feature;

use App\Http\Payments\FakePayment;
use App\Notifications\OrderPlacedNotification;
use App\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\CreateWorld;
use Facades\App\Cart;
use Tests\TestCase;

class ConsumeCartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_checkout()
    {
        $response = $this->post('/api/checkout');

        $response->assertUnauthorized();
    }

    /** @test */
    public function checking_out_requires_validated_data()
    {
        $this->logMeIn();

        $response = $this->post('/api/checkout');

        $this->assertNotValidated($response);
    }

    /** @test */
    public function an_authenticated_user_can_not_checkout_unauthorized_company()
    {
        $this->createCompany();

        $response = $this->post('/api/checkout', [
            'gateway'    => 'Stripe',
            'company_id' => $this->company->id,
        ]);

        $response->assertUnauthorized();
    }

    /** @test */
    public function company_products_can_be_checkout_by_authorized_company()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->createAuthorizedCompany()->makeCart();

        $this->post('/api/checkout', [
            'gateway'               => 'Fake',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->authorizedCompany->id,
        ]);

        $this->assertDatabaseHas('orders', [
            'user_id'    => auth()->user()->id,
            'company_id' => $this->authorizedCompany->id,
        ]);

        $order = Order::whereId(1)->with('documents')->first();

        $this->assertDatabaseHas('documents', [
            'documentable_id'   => 1,
            'documentable_type' => 'App\Order',
        ]);

        $order->documents->each(function ($document) {
            Storage::assertExists('public/' . $document->filepath);
        });

        //2 invoices + 1 proform
        $this->assertCount(3, $order->documents);
    }

    /** @test */
    public function it_notifies_for_payment_created_event()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->createAuthorizedCompany()->makeCart();

        Notification::fake();

        Cart::checkout(new FakePayment(), [
            'gateway'               => 'Fake',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->authorizedCompany->id,
        ]);

        Notification::assertSentTo(Order::first()->user, OrderPlacedNotification::class);
    }
}
