<?php

namespace Tests\Feature;

use App\Product;
use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class UpdateCartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_update_any_product_from_a_cart()
    {
        $this->createProduct();

        $response = $this->patch('/api/cart');

        $response->assertUnauthorized();
    }

    /** @test */
    public function updating_product_from_a_cart_must_be_validated()
    {
        $this->logMeIn()->createProduct();

        $response = $this->patch('/api/cart');

        $this->assertNotValidated($response);
    }

    /** @test */
    public function an_authenticated_user_may_update_product_from_their_cart()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->makeCart();

        $cartProduct = Cart::getProduct($this->companyProducts->first());

        $this->assertEquals($cartProduct->qty, 1);

        $this->patch('/api/cart', [
            'quantity' => 3,
            'type'     => 'default',
            'id'       => $cartProduct->id,
        ]);

        $this->assertEquals(3, $cartProduct->qty);
    }

    /** @test */
    public function it_returns_error_if_the_product_is_not_part_of_the_cart_when_updating()
    {
        $this->logMeIn()->createProduct();

        $response = $this->patch('/api/cart', [
            'type'     => 'default',
            'id'       => $this->product->id,
            'quantity' => 1,
        ]);

        $response->assertJson([
            'success' => false,
            'message' => 'The product was not found in the cart!',
        ]);
    }

    /** @test */
    public function it_returns_error_if_there_is_no_availability_of_the_product_for_updating()
    {
        $this->logMeIn()->createProduct();

        $product = factory(Product::class)->create([
            'quantity' => 1,
        ]);

        Cart::addProduct($product);

        $response = $this->patch('/api/cart', [
            'type'     => 'default',
            'id'       => $product->id,
            'quantity' => 2,
        ]);

        $response->assertJson([
            'success' => false,
            'message' => 'There is no availability for this product!',
        ]);
    }

    /** @test */
    public function a_product_quantity_can_be_updated()
    {
        $this->logMeIn()->createProduct();

        Cart::addProduct($this->product);

        $response = $this->patch('/api/cart', [
            'type'     => 'default',
            'id'       => $this->product->id,
            'quantity' => 2,
        ]);

        $response->assertJson([
            'product' => [
                'name' => $this->product->name,
                'qty'  => 2,
            ]
        ]);
    }
}
