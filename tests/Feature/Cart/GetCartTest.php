<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class GetCartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_any_cart()
    {
        $response = $this->get('/api/cart');

        $response->assertUnauthorized();
    }

    /** @test */
    public function cart_returns_all_the_stored_products()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->createSecondCompany()->createProductsForSecondCompany()->makeCart();

        $response = $this->get('/api/cart?type=default');

        $response->assertJson([
            'carts' => [
                $this->company->id       => [
                    'name' => $this->company->name,
                    'slug' => $this->company->slug,
                    'cart' => [
                        'content' => [],
                    ],
                ],
                $this->secondCompany->id => [
                    'name' => $this->secondCompany->name,
                    'slug' => $this->secondCompany->slug,
                    'cart' => [
                        'content' => [],
                    ],
                ],
            ],
        ]);
    }

    /** @test */
    public function cart_returns_filtered_products_by_company()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->createSecondCompany()->createProductsForSecondCompany()->makeCart();

        $response = $this->get('/api/cart?type=default&company_id=' . $this->company->id);

        $response->assertJsonMissing([
            'name' => $this->secondCompanyProducts->first()->name,
        ]);

        $response->assertJson([
            'carts' => [
                $this->company->id       => [
                    'name' => $this->company->name,
                    'slug' => $this->company->slug,
                    'cart' => [
                        'content' => [],
                    ],
                ],
            ],
        ]);
    }

    /** @test */
    public function cart_returns_empty_carts_if_empty()
    {
        $this->logMeIn()->createCompany()->createProductsForCompany()->createSecondCompany()->createProductsForSecondCompany();

        $response = $this->get('/api/cart?type=default');

        $response->assertJsonMissing([
            'name' => $this->secondCompanyProducts->first()->name,
        ]);

        $response->assertJsonMissing([
            'name' => $this->companyProducts->first()->name,
        ]);

        $response->assertJson([
            'carts' => [],
        ]);
    }

    /** @test */
    public function it_gets_products_without_changing_its_quantity()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProductsForCompany()
            ->makeCart();

        $firstResponse = $this->get('/api/cart?type=default')->json();

        $secondResponse = $this->get('/api/cart?type=default')->json();

        $this->assertSame($firstResponse, $secondResponse);
    }
}
