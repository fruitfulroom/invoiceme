<?php

namespace Tests\Feature;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class StoreCartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_store_to_any_cart()
    {
        $response = $this->post('/api/cart');

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_store_product_to_their_cart()
    {
        $this
            ->logMeIn()
            ->createProduct();

        $response = $this->post('/api/cart', [
            'type' => 'default',
            'id'   => $this->product->id,
        ]);

        $response->assertSee($this->product->name);
    }

    /** @test */
    public function cart_stores_to_parent_instance()
    {
        $this
            ->logMeIn()
            ->createProduct();

        $this->post('/api/cart', [
            'type' => 'default',
            'id'   => $this->product->id,
        ]);

        $cart = Cart::instance('default')->content()->first();

        $this->assertEquals([
            'rowId'    => $cart->rowId,
            'id'       => $this->product->id,
            'name'     => $this->product->name,
            'qty'      => 1,
            'price'    => $this->product->price / 100,
            'options'  => [],
            'tax'      => $cart->tax,
            'subtotal' => $cart->subtotal,
        ], $cart->toArray());
    }

    /** @test */
    public function cart_stores_to_companies_instance()
    {
        $this
            ->logMeIn()
            ->createProduct();

        $this->post('/api/cart', [
            'type' => 'default',
            'id'   => $this->product->id,
        ]);

        $cart = Cart::content()->first();

        $this->assertEquals([
            'rowId'    => $cart->rowId,
            'id'       => $this->product->id,
            'name'     => $this->product->name,
            'qty'      => 1,
            'price'    => $this->product->price / 100,
            'options'  => [],
            'tax'      => $cart->tax,
            'subtotal' => $cart->subtotal,
        ], $cart->toArray());
    }

    /** @test */
    public function it_returns_error_if_there_is_no_availability_of_the_product()
    {
        $this->logMeIn();

        $notAvailableProduct = factory(Product::class)->create([
            'quantity' => 0,
        ]);

        $response = $this->post('/api/cart', [
            'type' => 'default',
            'id'   => $notAvailableProduct->id,
        ]);

        $response->assertJson([
            'success' => false,
            'message' => 'There is no availability for this product!',
        ]);
    }
}
