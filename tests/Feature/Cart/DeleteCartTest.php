<?php

namespace Tests\Feature;

use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class DeleteCartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_delete_any_product_from_a_cart()
    {
        $this->createProduct();

        $response = $this->delete('/api/cart/' . $this->product->slug);

        $response->assertUnauthorized();
    }

    /** @test */
    public function deleting_product_from_a_cart_must_be_validated()
    {
        $this->logMeIn()->createProduct();

        $response = $this->delete('/api/cart/' . $this->product->slug);

        $this->assertNotValidated($response);
    }

    /** @test */
    public function an_authenticated_user_may_delete_product_from_their_cart()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProductsForCompany()
            ->makeCart();

        $this->assertEquals(Cart::count(), 2);

        $response = $this->delete('/api/cart/' . $this->companyProducts->first()->slug, [
            'type' => 'default',
        ]);

        $response->assertStatus(201);

        $this->assertEquals(Cart::count(), 1);
    }
}
