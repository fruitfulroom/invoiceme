<?php

namespace Tests\Unit;

use App\Company;
use App\Product;
use App\TrustedCompany;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Cashier\Subscription;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateTrustedCompaniesWithSubscriptionTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_validates_subscription_before_creating_trusted_company_if_not_subscribed()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_validates_subscription_before_creating_trusted_company_if_subscription_ended()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $endedSubscription = factory(Subscription::class)->create([
            'user_id'       => auth()->user()->id,
            'ends_at'       => '2019-01-01 20:20:20',
            'stripe_status' => 'canceled',
        ]);

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_unvalidates_if_user_exceeded_allowed_trusted_companies()
    {
        $this->logMeIn($subscribed = true)->createAuthorizedCompany();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $subscriptionName = config('invoiceme.stripe.product.name');
        $subscriptionPlans = collect(config('invoiceme.stripe.plans'));

        $userSubscription = $subscriptionPlans->where('id',
            auth()->user()->subscription($subscriptionName)->stripe_plan)->first();

        $trustedCompanies = factory(Company::class, (int) $userSubscription['trusted_companies_allowed'])->create();

        $trustedCompanies->each(function ($trustedCompany) use ($company) {
            $company->trusted_companies()->create([
                'trusted_company_id' => $trustedCompany->id,
                'trusted_fund'       => 5000,
                'available'          => 5000,
                'trusted_period'     => 60,
            ]);
        });

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company has reached their trusted buddies limit!',
        ]);
    }

    /** @test */
    public function it_uses_company_owner_allowance_when_employee_is_creating_a_trusted_company()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $company->employees()->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertStatus(201);
    }

    /** @test */
    public function only_owner_or_employee_can_create_new_trusted_company()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $response = $this->post('/api/trusted_fund', [
            'company_id'         => $company->id,
            'trusted_company_id' => $this->authorizedCompany->id,
            'trusted_fund'       => 5000,
            'trusted_period'     => 60,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'Only company owners or employees may proceed!',
        ]);
    }
}
