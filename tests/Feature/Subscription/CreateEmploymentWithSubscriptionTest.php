<?php

namespace Tests\Unit;

use App\Company;
use App\Employment;
use App\Product;
use App\TrustedCompany;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Cashier\Subscription;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateEmploymentWithSubscriptionTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_validates_subscription_before_creating_employment_if_not_subscribed()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_validates_subscription_before_creating_employment_if_subscription_ended()
    {
        $this->logMeIn()->createAuthorizedCompany();

        $endedSubscription = factory(Subscription::class)->create([
            'user_id'       => auth()->user()->id,
            'ends_at'       => '2019-01-01 20:20:20',
            'stripe_status' => 'canceled',
        ]);

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_unvalidates_if_user_exceeded_allowed_employments()
    {
        $this->logMeIn($subscribed = true)->createAuthorizedCompany();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $subscriptionName = config('invoiceme.stripe.product.name');
        $subscriptionPlans = collect(config('invoiceme.stripe.plans'));

        $userSubscription = $subscriptionPlans->where('id',
            auth()->user()->subscription($subscriptionName)->stripe_plan)->first();

        $users = factory(User::class, (int) $userSubscription['employees_allowed'])->create();

        $users->each(function ($user) use ($company) {
            $company->employees()->create([
                'user_id' => $user->id,
            ]);
        });

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company has reached their employees limitation!',
        ]);
    }

    /** @test */
    public function it_uses_company_owner_allowance_when_employee_is_creating_a_employee()
    {
        $this->logMeIn();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $company->employees()->create([
            'user_id' => auth()->user()->id,
        ]);

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertStatus(201);
    }

    /** @test */
    public function only_owner_or_employee_can_create_new_employee()
    {
        $this->logMeIn();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertJsonValidationErrors([
            'company_id' => 'Only company owners or employees may proceed!',
        ]);
    }
}
