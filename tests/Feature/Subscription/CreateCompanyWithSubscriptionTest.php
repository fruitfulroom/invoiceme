<?php

namespace Tests\Unit;

use App\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Cashier\Subscription;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateCompanyWithSubscriptionTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_applies_companies_allowed_factor()
    {
        $this->logMeIn($subscribed = true)->usesImageUpload();

        $company = factory(Company::class)
            ->make([
                'image'     => $this->image,
                'signature' => $this->image,
            ])->toArray();

        $company['stripe_secret_key'] = 'sk_some';

        $response = $this->post('/api/companies', $company);

        $response->assertStatus(201);
    }

    /** @test */
    public function it_validates_subscription_before_creating_company_if_not_subscribed()
    {
        $this->logMeIn()->usesImageUpload();

        $company = factory(Company::class)
            ->make([
                'image'     => $this->image,
                'signature' => $this->image,
            ])->toArray();

        $company['stripe_secret_key'] = 'sk_some';

        $response = $this->post('/api/companies', $company);

        $response->assertJsonValidationErrors([
            'id' => 'You must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_validates_subscription_before_creating_company_if_subscription_ended()
    {
        $this->logMeIn()->usesImageUpload();

        $company = factory(Company::class)
            ->make([
                'image'     => $this->image,
                'signature' => $this->image,
            ])->toArray();

        $company['stripe_secret_key'] = 'sk_some';

        $endedSubscription = factory(Subscription::class)->create([
            'user_id'       => auth()->user()->id,
            'ends_at'       => '2019-01-01 20:20:20',
            'stripe_status' => 'canceled',
        ]);

        $response = $this->post('/api/companies', $company);

        $response->assertJsonValidationErrors([
            'id' => 'You must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_unvalidates_if_user_exceeded_allowed_companies()
    {
        $this->logMeIn($subscribed = true)->usesImageUpload();

        $subscriptionName = config('invoiceme.stripe.product.name');
        $subscriptionPlans = collect(config('invoiceme.stripe.plans'));

        $userSubscription = $subscriptionPlans->where('id', auth()
            ->user()
            ->subscription($subscriptionName)->stripe_plan)
            ->first();

        $allowedCompaniesMaxed = factory(Company::class, (int)$userSubscription['companies_allowed'])->create([
            'user_id' => auth()->user()->id,
        ]);

        $company = factory(Company::class)
            ->make([
                'image'     => $this->image,
                'signature' => $this->image,
            ])->toArray();

        $company['stripe_secret_key'] = 'sk_some';

        $response = $this->post('/api/companies', $company);

        $response->assertJsonValidationErrors([
            'id' => 'You have reached your maximum company creations!',
        ]);
    }
}
