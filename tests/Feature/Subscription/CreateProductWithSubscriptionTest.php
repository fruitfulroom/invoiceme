<?php

namespace Tests\Unit;

use App\Company;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Cashier\Subscription;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateProductWithSubscriptionTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_validates_subscription_before_creating_product_if_not_subscribed()
    {
        $this->logMeIn()->usesImageUpload();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $product = factory(Product::class)->make([
            'image' => $this->image
        ])->toArray();

        $product['company_id'] = $company->id;

        $response = $this->post('/api/products', $product);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_validates_subscription_before_creating_product_if_subscription_ended()
    {
        $this->logMeIn()->usesImageUpload();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $product = factory(Product::class)->make([
            'image' => $this->image
        ])->toArray();

        $product['company_id'] = $company->id;

        $endedSubscription = factory(Subscription::class)->create([
            'user_id'       => auth()->user()->id,
            'ends_at'       => '2019-01-01 20:20:20',
            'stripe_status' => 'canceled',
        ]);

        $response = $this->post('/api/products', $product);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company owner must have a valid subscription!',
        ]);
    }

    /** @test */
    public function it_unvalidates_if_user_exceeded_allowed_products()
    {
        $this->logMeIn($subscribed = true)->usesImageUpload();

        $company = factory(Company::class)->create([
            'user_id' => auth()->user()->id,
        ]);

        $subscriptionName = config('invoiceme.stripe.product.name');
        $subscriptionPlans = collect(config('invoiceme.stripe.plans'));

        $userSubscription = $subscriptionPlans->where('id',
            auth()->user()->subscription($subscriptionName)->stripe_plan)->first();

        $allowedProductsMaxed = factory(Product::class, (int)$userSubscription['products_allowed'])->create([
            'user_id'    => auth()->user()->id,
            'company_id' => $company->id,
        ]);

        $product = factory(Product::class)->make([
            'image' => $this->image
        ])->toArray();

        $product['company_id'] = $company->id;

        $response = $this->post('/api/products', $product);

        $response->assertJsonValidationErrors([
            'company_id' => 'The company has reached their products creation limit!',
        ]);
    }

    /** @test */
    public function it_uses_company_owner_allowance_when_employee_is_creating_a_product()
    {
        $this->logMeIn()->usesImageUpload();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $company->employees()->create([
            'user_id' => auth()->user()->id,
        ]);

        $product = factory(Product::class)->make([
            'image' => $this->image
        ])->toArray();

        $product['company_id'] = $company->id;

        $response = $this->post('/api/products', $product);

        $response->assertStatus(201);
    }

    /** @test */
    public function only_owner_or_employee_can_create_new_product()
    {
        $this->logMeIn()->usesImageUpload();

        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        $product = factory(Product::class)->make([
            'image' => $this->image
        ])->toArray();

        $product['company_id'] = $company->id;

        $response = $this->post('/api/products', $product);

        $response->assertJsonValidationErrors([
            'company_id' => 'Only company owners or employees may proceed!',
        ]);
    }
}
