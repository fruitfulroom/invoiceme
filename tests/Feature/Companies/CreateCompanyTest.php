<?php

namespace Tests\Feature;

use App\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateCompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_create_companies()
    {
        $response = $this->post('/api/companies');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->post('/api/companies', []);

        $response->assertJsonValidationErrors([
            'name', 'description', 'stripe_publish_key', 'stripe_secret_key',
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_create_companies()
    {
        $this->logMeIn($subscribed = true)->usesImageUpload();

        $company = factory(Company::class)->make()->toArray();
        $company['stripe_secret_key'] = 'sk_some';
        $company['image'] = $this->image;
        $company['signature'] = $this->image;

        $response = $this->post('/api/companies', $company);

        $response->assertJson([
            'company' => [
                'name'               => $company['name'],
                'description'        => $company['description'],
                'stripe_publish_key' => $company['stripe_publish_key'],
            ],
        ]);

        $this->assertDatabaseHas('companies', [
            'name'               => $company['name'],
            'description'        => $company['description'],
            'stripe_publish_key' => $company['stripe_publish_key'],
        ]);
    }

    /** @test */
    public function a_company_image_is_required()
    {
        $this->logMeIn($subscribed = true)->usesImageUpload();

        $company = factory(Company::class)->make()->toArray();
        $company['stripe_secret_key'] = 'sk_some';
        $company['image'] = $this->image;
        $company['signature'] = $this->image;

        $this->post('/api/companies', $company);

        $this->assertStringStartsWith('companies', Company::first()->image);
    }
}
