<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class DeleteCompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_delete_companies()
    {
        $response = $this->delete('/api/companies/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->delete('/api/companies/' . $someDoesNotExistingId = 77);

        $this->assertResponseExceptionIsThrown($response, 'No query results for model [App\Company] 77');
    }

    /** @test */
    public function an_authenticated_user_may_delete_companies()
    {
        $this->createCompany();

        Passport::actingAs($this->company->owner);

        $this->assertDatabaseHas('companies', [
            'id'   => $this->company->id,
            'name' => $this->company->name,
        ]);

        $this->delete('/api/companies/' . $this->company->id);

        $this->assertDatabaseMissing('companies', [
            'id'         => $this->company->id,
            'name'       => $this->company->name,
            'deleted_at' => null,
        ]);
    }
}
