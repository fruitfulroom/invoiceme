<?php

namespace Tests\Feature;

use App\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class UpdateCompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_update_any_companies()
    {
        $response = $this->patch('/api/companies/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function updating_companies_must_be_validated()
    {
        $this->logMeIn()->createCompany();

        $response = $this->patch('/api/companies/' . $this->company->id, [
            'name'               => 777,
            'description'        => false,
            'stripe_publish_key' => false,
            'stripe_secret_key'  => true,
        ]);

        $response->assertJsonValidationErrors([
            'name',
            'description',
            'stripe_publish_key',
            'stripe_secret_key',
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_update_companies()
    {
        $this->createCompany();

        Passport::actingAs($this->company->owner);

        $this->patch('/api/companies/' . $this->company->id, [
            'name'               => 'Updated name',
            'description'        => 'New description',
            'stripe_publish_key' => 'pk_new',
            'stripe_secret_key'  => 'sk_new',
        ]);

        $this->assertDatabaseHas('companies', [
            'name'               => 'Updated name',
            'description'        => 'New description',
            'stripe_publish_key' => 'pk_new',
            'stripe_secret_key'  => 'sk_new',
        ]);
    }

    /** @test */
    public function a_company_image_may_be_updated()
    {
        $this->createCompany()->usesImageUpload();

        Passport::actingAs($this->company->owner);

        $this->patch('/api/companies/' . $this->company->id, [
            'image' => $this->image,
        ]);

        $this->assertStringStartsWith('companies', Company::first()->image);
    }
}
