<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class GetCompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_the_companies()
    {
        $this->createCompany();

        $response = $this->get('/api/companies/' . $this->company->id);

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_companies()
    {
        $this->logMeIn()->createCompany();

        $response = $this->get('/api/companies/' . $this->company->id);

        $response->assertStatus(201);

        $response->assertJson([
            'company' => [
                'name'               => $this->company->name,
                'slug'               => $this->company->slug,
                'description'        => $this->company->description,
                'stripe_publish_key' => $this->company->stripe_publish_key,
            ],
        ]);
    }
}
