<?php

namespace Tests\Feature;

use App\Company;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class CreateEmploymentsTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_create_employments()
    {
        $response = $this->post('/api/employments');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn()->createCompany();

        $response = $this->post('/api/employments', [
            'user_id' => auth()->user()->id,
        ]);

        $response->assertJsonValidationErrors([
            'company_id',
        ]);

        $response = $this->post('/api/employments', [
            'company_id' => $this->company->id,
        ]);

        $response->assertJsonValidationErrors([
            'user_id',
        ]);
    }

    /** @test */
    public function an_authenticated_user_may_create_employments()
    {
        $companyOwner = factory(User::class)->state('subscribed')->create();
        $company = factory(Company::class)->create([
            'user_id' => $companyOwner->id,
        ]);

        Passport::actingAs($companyOwner);

        $response = $this->post('/api/employments', [
            'company_id' => $company->id,
            'user_id'    => auth()->user()->id,
        ]);

        $response->assertJson([
            'employment' => [
                'user_id'    => auth()->user()->id,
                'company_id' => $company->id,
            ],
        ]);

        $this->assertDatabaseHas('employments', [
            'user_id'    => auth()->user()->id,
            'company_id' => $company->id,
        ]);
    }
}
