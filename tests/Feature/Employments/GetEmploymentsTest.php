<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class GetEmploymentsTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function not_authenticated_user_may_not_access_the_employments()
    {
        $response = $this->get('/api/employments');

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_the_employments()
    {
        $user = factory(User::class)->state('with_employments')->create();

        Passport::actingAs($user);

        $response = $this->get('/api/employments');

        $response->assertJson([
            'employments' => [
                [
                    'user_id' => auth()->user()->id,
                    'company' => [
                        'id'   => $user->employments->pluck('company')->first()->id,
                        'name' => $user->employments->pluck('company')->first()->name,
                    ],
                ],
                [
                    'user_id' => auth()->user()->id,
                    'company' => [
                        'id'   => $user->employments->pluck('company')->last()->id,
                        'name' => $user->employments->pluck('company')->last()->name,
                    ],
                ],
            ],
        ]);
    }
}
