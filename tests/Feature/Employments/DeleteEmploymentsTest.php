<?php

namespace Tests\Feature;

use App\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\CreateWorld;
use Tests\TestCase;

class DeleteEmploymentsTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_delete_employments()
    {
        $response = $this->delete('/api/employments/77');

        $response->assertUnauthorized();
    }

    /** @test */
    public function it_validates_the_given_data()
    {
        $this->logMeIn();

        $response = $this->delete('/api/employments/' . $someDoesNotExistingId = 77);

        $this->assertResponseExceptionIsThrown($response, 'No query results for model [App\Employment] 77');
    }

    /** @test */
    public function an_authenticated_user_may_delete_employments()
    {
        $companyWithEmployments = factory(Company::class)->state('with_employees')->create();

        Passport::actingAs($companyWithEmployments->owner);

        $employment = $companyWithEmployments->employees->first();

        $this->assertDatabaseHas('employments', [
            'user_id'    => $employment->user_id,
            'company_id' => $companyWithEmployments->id,
        ]);

        $this->delete('/api/employments/' . $employment->id);

        $this->assertDatabaseMissing('employments', [
            'user_id'    => $employment->user_id,
            'company_id' => $companyWithEmployments->id,
        ]);
    }
}
