<?php

namespace Tests\Feature;

use App\Http\Payments\FakePayment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\CreateWorld;
use Tests\TestCase;
use Facades\App\Cart;

class GetUserNotificationsTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function not_authenticated_user_may_not_access_the_notifications()
    {
        $response = $this->get('/api/user/notifications');

        $response->assertUnauthorized();
    }

    /** @test */
    public function an_authenticated_user_may_access_their_notifications()
    {
        $this->logMeIn($subscribed = false, $entry = Sanctum::class)
            ->createCompany()
            ->createProductsForCompany()
            ->createAuthorizedCompany()
            ->makeCart();

        Cart::checkout(new FakePayment(), [
            'gateway'               => 'Fake',
            'company_id'            => $this->company->id,
            'authorized_company_id' => $this->authorizedCompany->id,
        ]);

        $response = $this->get('/api/user/notifications');

        $response->assertJson([
            'success'       => true,
            'notifications' => [],
        ]);

        $this->assertEquals(1, auth()->user()->notifications->count());
    }
}
