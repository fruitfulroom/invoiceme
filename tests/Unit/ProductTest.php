<?php

namespace Tests\Unit;

use App\Category;
use App\Company;
use App\Order;
use App\Product;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ProductTest extends TestCase
{

    use RefreshDatabase;

    private $product;

    protected function setUp(): void
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());

        $this->product = factory(Product::class)->create();
    }

    /** @test */
    public function it_belongs_to_a_creator()
    {
        $this->assertInstanceOf(User::class, $this->product->creator);
    }

    /** @test */
    public function it_belongs_to_a_company()
    {
        $this->assertInstanceOf(Company::class, $this->product->company);
    }

    /** @test */
    public function it_belongs_to_a_categories()
    {
        $productWithCategories = factory(Product::class)
            ->state('with_categories')
            ->create();

        $this->assertCollectionOf(Category::class, $productWithCategories->fresh()->categories);
    }

    /** @test */
    public function it_belongs_to_an_orders()
    {
        $productWithOrders = factory(Product::class)
            ->state('with_orders')
            ->create();

        $this->assertCollectionOf(Order::class, $productWithOrders->fresh()->orders);
    }

    /** @test */
    public function it_can_determine_if_it_has_availability_from_the_cart()
    {
        $product = factory(Product::class)->create([
            'quantity' => 2,
        ]);

        $notAddedProductWithAvailability = factory(Product::class)->create([
            'quantity' => 2,
        ]);

        $notAddedProductWithNoAvailability = factory(Product::class)->create([
            'quantity' => 0,
        ]);

        Cart::add($product->id, $product->name, 1, $product->price / 100)
            ->associate(Product::class);

        $this->assertTrue($product->hasAvailabilityFromCart());

        Cart::update(Cart::content()
            ->first()->rowId, 2);

        $this->assertTrue($product->hasAvailabilityFromCart());

        Cart::update(Cart::content()
            ->first()->rowId, 3);

        $this->assertFalse($product->hasAvailabilityFromCart());

        $this->assertTrue($notAddedProductWithAvailability->hasAvailabilityFromCart());

        $this->assertFalse($notAddedProductWithNoAvailability->hasAvailabilityFromCart());
    }

    /** @test */
    public function it_logs_creation_of_a_product()
    {
        $productActivities = $this->product->activities;

        $this->assertEquals(1, $productActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => Product::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_of_a_product()
    {
        $this->assertEquals(1, $this->product->activities->count());

        $this->product->update([
            'name' => 'Updated name',
        ]);

        $this->assertEquals(2, $this->product->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated',
            'subject_type' => Product::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_can_determine_the_image_path()
    {
        $this->product->update([
            'image' => 'products/DATE/NAME.JPG'
        ]);

        $this->assertSame(url('products/DATE/NAME.JPG'), $this->product->fresh()->image_path);
    }

    /** @test */
    public function it_can_determine_the_gallery_paths()
    {
        $this->product->update([
            'gallery' => '["products\/DATE\/NAME1.JPG","products\/DATE\/NAME2.JPG","products\/DATE\/NAME3.JPG"]'
        ]);

        foreach ($this->product->fresh()->gallery_paths as $gallery_path) {
            $this->assertStringStartsWith(url('products/DATE/'), $gallery_path);
        }
    }

    /** @test */
    public function it_generates_slug()
    {
        $product = factory(Product::class)->create([
            'name' => 'Cool Name'
        ]);

        $this->assertSame('cool-name', $product->fresh()->slug);
    }
}
