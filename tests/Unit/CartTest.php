<?php

namespace Tests\Unit;

use App\Product;
use Facades\App\Cart;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class CartTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_returns_null_when_getting_a_product_from_a_cart_if_does_not_exist()
    {
        $this->createProduct();

        $this->assertNull(Cart::getProduct($this->product));
    }

    /** @test */
    public function it_returns_an_exception_if_the_product_has_not_been_added_to_cart_when_updating()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create([
            'quantity' => 4,
        ]);

        $this->expectExceptionMessage('The product was not found in the cart!');

        Cart::updateProductQuantity($product, 3);
    }

    /** @test */
    public function it_returns_an_exception_if_the_product_does_not_have_enough_availability_when_updating()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create([
            'quantity' => 4,
        ]);

        Cart::addProduct($product);

        $this->expectExceptionMessage('There is no availability for this product!');

        Cart::updateProductQuantity($product, 5);
    }

    /** @test */
    public function it_can_remove_product_from_cart()
    {
        $this->logMeIn()
            ->createCompany()
            ->createProductsForCompany()
            ->makeCart();

        Cart::instance('default')->remove(Cart::getProduct($this->companyProducts->first())->rowId);

        $this->assertSame(1, Cart::instance('default')->count());
    }

    /** @test */
    public function it_logs_adding_a_product_to_cart()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create();

        Cart::addProduct($product);

        $this->assertEquals(2, $product->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'added to cart',
            'subject_type' => Product::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_quantity_of_a_product_from_cart()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create();

        Cart::addProduct($product);

        Cart::updateProductQuantity($product, 6);

        $this->assertEquals(3, $product->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated from cart',
            'subject_type' => Product::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_removing_a_product_from_cart()
    {
        $this->logMeIn();

        $product = factory(Product::class)->create();

        Cart::addProduct($product);

        Cart::removeProduct($product);

        $this->assertEquals(3, $product->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'removed from cart',
            'subject_type' => Product::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }
}
