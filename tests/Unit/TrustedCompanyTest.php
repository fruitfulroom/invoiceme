<?php

namespace Tests\Unit;

use App\Company;
use App\TrustedCompany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class TrustedCompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_belongs_to_a_company()
    {
        $this->createCompany()
            ->createTrustedCompany();

        $this->assertInstanceOf(Company::class, $this->trustedCompanyInstance->company);
    }

    /** @test */
    public function it_belongs_to_a_trusted_company()
    {
        $this->createCompany()
            ->createTrustedCompany();

        $this->assertInstanceOf(Company::class, $this->trustedCompanyInstance->trusted_company);
    }

    /** @test */
    public function it_logs_creation_of_a_trusted_company()
    {
        $this->logMeIn()
            ->createCompany()
            ->createTrustedCompany();

        $trustedCompanyActivities = $this->trustedCompanyInstance->activities;

        $this->assertEquals(1, $trustedCompanyActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => TrustedCompany::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_of_a_trusted_company()
    {
        $this->logMeIn()
            ->createCompany()
            ->createTrustedCompany();

        $this->assertEquals(1, $this->trustedCompanyInstance->activities->count());

        $this->trustedCompanyInstance->update([
            'trusted_fund' => 777,
        ]);

        $this->assertEquals(2, $this->trustedCompanyInstance->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated',
            'subject_type' => TrustedCompany::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }
}
