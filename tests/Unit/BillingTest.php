<?php

namespace Tests\Unit;

use App\Billing;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class BillingTest extends TestCase
{

    use RefreshDatabase;

    private $billing;

    protected function setUp(): void
    {
        parent::setUp();

        Passport::actingAs(factory(User::class)->create());

        $this->billing = factory(Billing::class)->create();
    }

    /** @test */
    public function it_belongs_to_an_user()
    {
        $this->assertInstanceOf(User::class, $this->billing->user);
    }

    /** @test */
    public function it_logs_creation_of_a_billing()
    {
        $billingActivities = $this->billing->activities;

        $this->assertEquals(1, $billingActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => Billing::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_of_a_billing()
    {
        $this->assertEquals(1, $this->billing->activities->count());

        $this->billing->update([
            'city' => 'Liverpool',
        ]);

        $this->assertEquals(2, $this->billing->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated',
            'subject_type' => Billing::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }
}
