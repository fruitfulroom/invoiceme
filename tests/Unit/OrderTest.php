<?php

namespace Tests\Unit;

use App\Company;
use App\Document;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class OrderTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_has_products()
    {
        $order = factory(Order::class)
            ->state('with_products')
            ->create();

        $this->assertDatabaseHas('order_product', [
            'order_id' => $order->id,
        ]);

        $this->assertCollectionOf(Product::class, $order->fresh()->products);
    }

    /** @test */
    public function it_has_ordered_products()
    {
        $order = factory(Order::class)
            ->state('with_products')
            ->create();

        $this->assertDatabaseHas('order_product', [
            'order_id' => $order->id,
        ]);

        $this->assertCollectionOf(OrderProduct::class, $order->fresh()->orderedProducts);
    }

    /** @test */
    public function it_belongs_to_an_user()
    {
        $order = factory(Order::class)->create();

        $this->assertInstanceOf(User::class, $order->user);
    }

    /** @test */
    public function it_belongs_to_a_company()
    {
        $order = factory(Order::class)->create();

        $this->assertInstanceOf(Company::class, $order->company);
    }

    /** @test */
    public function it_has_documents()
    {
        $order = factory(Order::class)
            ->state('with_document')
            ->create();

        $this->assertCollectionOf(Document::class, $order->documents);
    }

    /** @test */
    public function it_logs_creation_of_an_order()
    {
        Passport::actingAs(factory(User::class)->create());

        $order = factory(Order::class)->create();

        $orderActivities = $order->activities;

        $this->assertEquals(1, $orderActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => Order::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_of_an_order()
    {
        Passport::actingAs(factory(User::class)->create());

        $order = factory(Order::class)->create();

        $this->assertEquals(1, $order->activities->count());

        $order->update([
            'payed' => false,
        ]);

        $this->assertEquals(2, $order->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated',
            'subject_type' => Order::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }
}
