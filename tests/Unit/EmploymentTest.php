<?php

namespace Tests\Unit;

use App\Company;
use App\Employment;
use App\TrustedCompany;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class EmploymentTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_belongs_to_a_company()
    {
        $this->logMeIn()
            ->createCompany()
            ->addMeAsEmployee($this->company);

        $this->assertInstanceOf(Company::class, $this->company->employees->first()->company);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $this->logMeIn()
            ->createCompany()
            ->addMeAsEmployee($this->company);

        $this->assertInstanceOf(User::class, $this->company->employees->first()->user);
    }

    /** @test */
    public function it_logs_creation_of_a_employment()
    {
        $this->logMeIn()
            ->createCompany()
            ->addMeAsEmployee($this->company);

        $employmentActivities = $this->company->employees->first()->activities;

        $this->assertEquals(1, $employmentActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => Employment::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }
}
