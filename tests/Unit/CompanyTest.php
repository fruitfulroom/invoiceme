<?php

namespace Tests\Unit;

use App\Company;
use App\Document;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreateWorld;
use Tests\TestCase;

class CompanyTest extends TestCase
{

    use RefreshDatabase, CreateWorld;

    /** @test */
    public function it_has_products()
    {
        $company = factory(Company::class)->state('with_products')->create();

        $this->assertCollectionOf(
            Product::class,
            $company->fresh()->products
        );
    }

    /** @test */
    public function it_has_owner()
    {
        $this->createCompany();

        $this->assertInstanceOf(User::class, $this->company->owner);
    }

    /** @test */
    public function it_has_employees()
    {
        $company = factory(Company::class)->state('with_employees')->create();

        $this->assertCollectionOf(
            User::class,
            $company->fresh()->employees->pluck('user')
        );
    }

    /** @test */
    public function it_has_trusted_companies()
    {
        $company = factory(Company::class)->state('with_trusted_companies')->create();

        $this->assertCollectionOf(
            Company::class,
            $company->trusted_companies->pluck('trusted_company')
        );
    }

    /** @test */
    public function it_can_apply_scope_for_anticipated_users()
    {
        $this->logMeIn();

        factory(Company::class)->state('with_employees')->create();

        $this->assertContains(auth()->user()->name, Company::canAnticipate()->first()->employees->pluck('user.name'));
    }

    /** @test */
    public function it_has_documents()
    {
        $company = factory(Company::class)->state('with_document')->create();

        $this->assertCollectionOf(
            Document::class,
            $company->documents
        );
    }

    /** @test */
    public function it_logs_creation_of_a_company()
    {
        $this->logMeIn()->createCompany();

        $companyActivities = $this->company->activities;

        $this->assertEquals(1, $companyActivities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'created',
            'subject_type' => Company::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_logs_updating_of_a_company()
    {
        $this->logMeIn()->createCompany();

        $this->assertEquals(1, $this->company->activities->count());

        $this->company->update([
            'name' => 'Updated name',
        ]);

        $this->assertEquals(2, $this->company->fresh()->activities->count());

        $this->assertDatabaseHas('activity_log', [
            'description'  => 'updated',
            'subject_type' => Company::class,
            'causer_id'    => auth()->user()->id,
        ]);
    }

    /** @test */
    public function it_can_determine_the_image_path()
    {
        $this->createCompany();

        $this->company->update([
            'image' => 'companies/DATE/NAME.JPG'
        ]);

        $this->assertSame(url('companies/DATE/NAME.JPG'), $this->company->fresh()->image_path);
    }

    /** @test */
    public function it_generates_slug()
    {
        $category = factory(Company::class)->create([
            'name' => 'Cool Name'
        ]);

        $this->assertSame('cool-name', $category->fresh()->slug);
    }
}
