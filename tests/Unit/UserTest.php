<?php

namespace Tests\Unit;

use App\Billing;
use App\Company;
use App\Document;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_has_orders()
    {
        $user = factory(User::class)->state('with_orders')->create();

        $this->assertCollectionOf(
            Order::class,
            $user->fresh()->orders
        );
    }

    /** @test */
    public function it_has_companies()
    {
        $user = factory(User::class)->state('with_companies')->create();

        $this->assertCollectionOf(
            Company::class,
            $user->fresh()->companies
        );
    }

    /** @test */
    public function it_has_products()
    {
        $user = factory(User::class)->state('with_products')->create();

        $this->assertCollectionOf(
            Product::class,
            $user->fresh()->products
        );
    }

    /** @test */
    public function it_is_employed()
    {
        $user = factory(User::class)->state('with_employments')->create();

        $this->assertCollectionOf(
            Company::class,
            $user->fresh()->employments->pluck('company')
        );
    }

    /** @test */
    public function it_has_billing_information()
    {
        $user = factory(User::class)->state('with_billing_information')->create();

        $this->assertInstanceOf(Billing::class, $user->billing);
    }

    /** @test */
    public function it_has_documents()
    {
        $user = factory(User::class)->state('with_document')->create();

        $this->assertCollectionOf(
            Document::class,
            $user->documents
        );
    }
}
