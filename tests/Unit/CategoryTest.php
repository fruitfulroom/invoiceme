<?php

namespace Tests\Unit;

use App\Category;
use App\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function it_has_products()
    {
        $category = factory(Category::class)->state('with_products')->create();

        $this->assertCollectionOf(
            Product::class,
            $category->fresh()->products
        );
    }

    /** @test */
    public function it_generates_slug()
    {
        $category = factory(Category::class)->create([
            'name' => 'Cool Name'
        ]);

        $this->assertSame('cool-name', $category->fresh()->slug);
    }
}
