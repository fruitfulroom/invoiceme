<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Collection;
use Illuminate\Testing\TestResponse;
use PHPUnit\Framework\InvalidArgumentException;

abstract class TestCase extends BaseTestCase
{

    use CreatesApplication;

    public function assertCollectionOf(string $expected, $actual, string $message = '')
    {
        if ( ! \class_exists($expected) && ! \interface_exists($expected)) {
            throw InvalidArgumentException::create(1, 'class or interface name');
        }

        if ( ! ($actual instanceof Collection)) {
            throw InvalidArgumentException::create(2, 'an instance of collection');
        }

        if ($actual->isEmpty()) {
            throw InvalidArgumentException::create(2, 'not empty collection');
        }

        $actual->each(function ($item) use ($expected, $message) {
            $this->assertInstanceOf($expected, $item, $message);
        });
    }

    public function assertNotValidated(TestResponse $response)
    {
        $response->assertStatus(422);
    }

    public function assertResponseExceptionIsThrown(TestResponse $response, $message)
    {
        if ( ! $response->exception) {
            throw InvalidArgumentException::create(1, 'an exception!');
        }

        $this->assertSame($message, $response->exception->getMessage());
    }

    public function get($uri, array $headers = [])
    {
        return $this->json('GET', $uri, $headers);
    }

    public function post($uri, array $data = [], array $headers = [])
    {
        return $this->json('POST', $uri, $data,$headers);
    }

    public function patch($uri, array $data = [], array $headers = [])
    {
        return $this->json('PATCH', $uri, $data,$headers);
    }

    public function delete($uri, array $data = [], array $headers = [])
    {
        return $this->json('DELETE', $uri, $data,$headers);
    }
}
