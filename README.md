# InvoiceMe

## Application endpoints

SPA on vue js

API on Laravel

ADMIN on laravel Voyager

## Specification

Laravel 7 - passport, sanctum

Sanctum for SPA accessing - login, logout, register, token generations for API

Passport for API calls

GIT flow for best practices

## Requirements
PHP 7.4

MySQL

nodeJS

NPM

Memcached

## Installation

```bash
brew services start memcached

composer install
php artisan migrate
php artisan storage:link
php artisan passport:keys

php artisan db:seed --class="MenusTableSeeder"
php artisan db:seed --class=VoyagerDeploymentOrchestratorSeeder
php artisan db:seed --class=VoyagerDatabaseSeeder
php artisan db:seed --class=Permission_roleTableSeeder

php artisan queue:listen
```

## Testing

```bash
cd to project
vendor/bin/phpunit
```

coverage can be found in `/tests/report`

## Development

```bash
php artisan voyager:admin email --create
php artisan db:seed
php artisan scout:import "App\Product"
```

#### Roles exportation

```bash
php artisan vdo:generate permissions,roles,permission_role
```

#### Adding/updating menu

```bash
php artisan vdo:generate menu_items
```

#### Creating new model

Requires adding it to config/voyager-deployment-orchestrator.php for bread creation

### Cache clearance

```bash
php artisan cache:clear
php artisan config:cache
php artisan view:clear
php artisan optimize
```

## API
OAuth 

login with SPA

generates a client token 

use the ID and the Token for the routes

## License

The project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## API Documentation 
`/api/documentation`

### Authentication

#### Example

1. Grant a token - Make a POST request for getting the access bearer to `/oauth/token` multipart params
- 'grant_type' => 'password',
- 'client_id' => '1', //taken from the SPA
- 'client_secret' => 'string', //taken from the SPA
- 'username' => 'email',
- 'password' => 'secret-password',

Use the token - Make a GET request for getting the products with the bearer to `/api/products` 
- with headers - 
  - 'Accept' => 'application/json',
  - 'Authorization' => 'Bearer eyJ0eXAiOiJK...',
- with params (query) - 
  - 'type' => 'default',
  - 'company_id' => '2',

#### Production notes
migrate to s3 and do not link the storage path but create tempUrls

## Third Party resources requiring subscriptions
Algolia, Nexmo, Mail, Stripe
