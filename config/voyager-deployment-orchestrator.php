<?php

return [
    'tables' => [
        'users',
        'products',
        'categories',
        'companies',
        'trusted_companies',
        'orders',
        'employments',
        'documents',
        'activity_log',
    ],
];
