<?php

return [
    'stripe' => [
        'product' => [
            'name' => env('STRIPE_PRODUCT_NAME', 'Invoice Me'),
        ],
        'plans'   => [
            [
                'name'                      => 'Basic',
                'id'                        => 'plan_HECqrZqZjAMq09',
                'position'                  => 1,
                'price'                     => '10',
                'companies_allowed'         => '1',
                'products_allowed'          => '20',
                'trusted_companies_allowed' => '1',
                'employees_allowed'         => '5',
                'special'                   => false,
            ],
            [
                'name'                      => 'Professional',
                'id'                        => 'plan_HECujEeSCUzjNv',
                'position'                  => 3,
                'price'                     => '25',
                'companies_allowed'         => '5',
                'products_allowed'          => '100',
                'trusted_companies_allowed' => '10',
                'employees_allowed'         => '50',
                'special'                   => false,
            ],
            [
                'name'                      => 'Enterprise',
                'id'                        => 'plan_HECu8fxYp6Eslw',
                'position'                  => 2,
                'price'                     => '70',
                'companies_allowed'         => '25',
                'products_allowed'          => '1000',
                'trusted_companies_allowed' => '20',
                'employees_allowed'         => '100',
                'special'                   => true,
            ],
        ],
    ],
];
