<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Billing;
use App\User;
use Faker\Generator as Faker;

$factory->define(Billing::class, function (Faker $faker) {
    return [
        'user_id'      => factory(User::class)->create()->id,
        'address'      => $faker->address,
        'city'         => $faker->city,
        'province'     => $faker->city,
        'postal_code'  => $faker->postcode,
        'phone'        => $faker->phoneNumber,
        'name_on_card' => $faker->name,
    ];
});
