<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Laravel\Cashier\Subscription;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id'       => factory(User::class)->create()->id,
        'name'          => 'Invoice Me',
        'stripe_id'     => 'sub_',
        'stripe_status' => 'active',
        'stripe_plan'   => 'plan_HECqrZqZjAMq09',
        'quantity'      => 1,
        'ends_at'       => '2022-01-01 20:20:20',
    ];
});
