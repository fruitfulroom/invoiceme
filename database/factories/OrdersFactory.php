<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use App\Document;
use App\Order;
use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $subtotal = $faker->numberBetween(1, 1000);

    return [
        'user_id'          => factory(User::class)->state('with_billing_information')->create()->id,
        'company_id'       => factory(Company::class)->create()->id,
        'billing_subtotal' => $subtotal,
        'billing_total'    => ($subtotal * 0.20) + $subtotal,
        'billing_tax'      => $subtotal * 0.20,
        'payment_gateway'  => 'stripe',
        'shipped'          => $faker->randomElement([true, false]),
    ];
});

$factory->afterCreatingState(Order::class, 'with_products', function (Order $order, $faker) {
    $products = factory(Product::class, 2)->create();

    $products->each(function ($product) use ($order, $faker) {
        $order->products()->attach($product->id, [
            'quantity' => $faker->randomNumber(),
        ]);
    });

    return $order;
});

$factory->afterCreatingState(Order::class, 'with_document', function (Order $order, Faker $faker) {
    $document = new Document([
        'filepath' => '/some/file/path'
    ]);

    $order->documents()->save($document);

    return $order;
});
