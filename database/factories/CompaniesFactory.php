<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use App\Document;
use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    $name = $faker->company;

    return [
        'name'               => $name,
        'description'        => $faker->sentence(40),
        'image'              => '',
        'website'            => 'http://invoiceme.test/',
        'stripe_publish_key' => 'pk_' . $faker->word,
        'stripe_secret_key'  => 'sk_' . $faker->word,
        'user_id'            => factory(User::class)->create()->id,
        'email'              => $faker->companyEmail,
        'address'            => $faker->address,
        'signature'          => 'https://www.docsketch.com/assets/vip-signatures/muhammad-ali-signature-6a40cd5a6c27559411db066f62d64886c42bbeb03b347237ffae98b0b15e0005.svg',
        'delivery_address'   => $faker->address,
        'city'               => $faker->city,
        'province'           => $faker->city,
        'postal_code'        => $faker->postcode,
        'phone'              => $faker->phoneNumber,
        'bank_name'          => $faker->name,
        'account_number'     => $faker->bankAccountNumber,
        'sort_code'          => $faker->numberBetween(10000000, 99999999),
        'iban'               => $faker->iban(),
        'bic_swift'          => $faker->swiftBicNumber,
    ];
});

$factory->afterCreatingState(Company::class, 'with_products', function (Company $company, $faker) {
    factory(Product::class, 2)->create([
        'company_id' => $company->id,
    ]);

    return $company;
});

$factory->afterCreatingState(Company::class, 'with_employees', function (Company $company, $faker) {
    $users = factory(User::class, 2)->create();

    $users->each(function ($user) use ($company, $faker) {
        $company->employees()->create([
            'user_id' => $user->id,
        ]);
    });

    if (auth()->user()) {
        $company->employees()->create([
            'user_id' => auth()->user()->id,
        ]);
    }

    return $company;
});

$factory->afterCreatingState(Company::class, 'with_trusted_companies', function (Company $company, Faker $faker) {
    $trustedCompanies = factory(Company::class, 2)->create();

    $trustedCompanies->each(function ($trustedCompany) use ($company, $faker) {
        $trustedFund = $faker->numberBetween(40000, 50000);

        $company->trusted_companies()->create([
            'trusted_company_id' => $trustedCompany->id,
            'trusted_fund'       => $trustedFund,
            'available'          => $trustedFund,
            'trusted_period'     => '2 months',
        ]);
    });

    return $company;
});

$factory->afterCreatingState(Company::class, 'with_document', function (Company $company, Faker $faker) {
    $document = new Document([
        'filepath' => '/some/file/path',
    ]);

    $company->documents()->save($document);

    return $company;
});
