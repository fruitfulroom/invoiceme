<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Billing;
use App\Company;
use App\Document;
use App\Order;
use App\Product;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Laravel\Cashier\Subscription;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'email'             => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token'    => Str::random(10),
    ];
});

$factory->afterCreatingState(User::class, 'with_orders', function (User $user, $faker) {
    factory(Order::class, 2)->create([
        'user_id' => $user->id,
    ]);

    factory(Order::class)->state('with_document')->create([
        'user_id' => $user->id,
    ]);

    return $user;
});

$factory->afterCreatingState(User::class, 'with_companies', function (User $user, $faker) {
    factory(Company::class, 2)->create([
        'user_id' => $user->id,
    ]);

    return $user;
});

$factory->afterCreatingState(User::class, 'with_products', function (User $user, $faker) {
    factory(Product::class, 2)->create([
        'user_id' => $user->id,
    ]);

    return $user;
});

$factory->afterCreatingState(User::class, 'with_employments', function (User $user, $faker) {
    $companies = factory(Company::class, 2)->create();

    $companies->each(function ($company) use ($user) {
        $company->employees()->create([
            'user_id' => $user->id,
        ]);
    });

    return $user;
});

$factory->afterCreatingState(User::class, 'with_billing_information', function (User $user, $faker) {
    factory(Billing::class)->create([
        'user_id' => $user->id,
    ]);

    return $user;
});

$factory->afterCreatingState(User::class, 'subscribed', function (User $user, $faker) {
    factory(Subscription::class)->create([
        'user_id' => $user->id,
    ]);

    return $user;
});

$factory->afterCreatingState(User::class, 'with_document', function (User $user, Faker $faker) {
    $user->documents()->save(new Document([
        'filepath' => '/some/file/path',
    ]));

    return $user;
});
