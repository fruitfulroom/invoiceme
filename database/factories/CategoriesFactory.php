<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->company;

    return [
        'name'        => $name,
        'description' => $faker->sentence(40),
    ];
});

$factory->afterCreatingState(Category::class, 'with_products', function (Category $category, $faker) {

    $products = factory(Product::class, 2)->create();

    $products->each(function ($product) use ($category) {
        $category->products()->attach($product->id);
    });

    return $category;
});
