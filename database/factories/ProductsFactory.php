<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Company;
use App\Order;
use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->sentence(2);

    return [
        'name'         => $name,
        'subtitle'     => $faker->sentence(5),
        'price'        => $faker->randomNumber(5),
        'image'        => '',
        'description'  => $faker->sentence(40),
        'user_id'      => factory(User::class)->create()->id,
        'quantity'     => $faker->randomNumber(2),
        'confirmed'    => $faker->randomElement([true, false]),
        'company_id'   => factory(Company::class)->create()->id,
        'size_height'  => $faker->randomNumber(3),
        'size_width'   => $faker->randomNumber(3),
        'size_depth'   => $faker->randomNumber(3),
        'gross_weight' => $faker->randomNumber(1),
        'cubic_weight' => $faker->randomNumber(1),
        'weight_unit'  => $faker->randomElement(['pounds', 'kgs']),

    ];
});

$factory->afterCreatingState(Product::class, 'with_categories', function (Product $product, $faker) {
    $categories = factory(Category::class, 2)->create();

    $categories->each(function ($category) use ($product) {
        $product->categories()->attach($category->id);
    });

    return $product;
});

$factory->afterCreatingState(Product::class, 'with_orders', function (Product $product, $faker) {
    $orders = factory(Order::class, 2)->create();

    $orders->each(function ($order) use ($product, $faker) {
        $product->orders()->attach($order->id, [
            'quantity' => $faker->randomNumber(),
        ]);
    });

    return $product;
});
