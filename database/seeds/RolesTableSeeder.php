<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $roles = collect([
            'admin'   => __('voyager::seeders.roles.admin'),
            'user'    => __('voyager::seeders.roles.user'),
            'creator' => 'Context Creator',
        ]);

        $roles->each(function ($displayName, $roleName) {
            $role = Role::firstOrNew(['name' => $roleName]);
            if ( ! $role->exists) {
                $role->fill([
                    'display_name' => $displayName,
                ])->save();
            }
        });
    }
}
