<?php

use App\Category;
use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allCategories = Category::all();

        factory(Product::class, 30)->create()->each(function (Product $product) use ($allCategories) {
            $product->categories()->attach(
                $allCategories->random(rand(1, 3))->pluck('id')
            );
        });
    }
}
