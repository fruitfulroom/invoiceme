<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\MenuItem;

class EmploymentsBreadTypeAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'employments')->first();

            if (is_bread_translatable($dataType)) {
                $dataType->deleteAttributeTranslations($dataType->getTranslatableAttributes());
            }

            if ($dataType) {
                DataType::where('name', 'employments')->delete();
            }

            \DB::table('data_types')->insert([
                'id'                    => 13,
                'name'                  => 'employments',
                'slug'                  => 'employments',
                'display_name_singular' => 'Employment',
                'display_name_plural'   => 'Employments',
                'icon'                  => 'voyager-group',
                'model_name'            => 'App\\Employment',
                'policy_name'           => 'App\\Policies\\EmploymentPolicy',
                'controller'            => null,
                'description'           => null,
                'generate_permissions'  => 1,
                'server_side'           => 0,
                'details'               => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":"participatable"}',
                'created_at'            => null,
                'updated_at'            => null,
            ]);


            Voyager::model('Permission')->generateFor('employments');

            $menu = Menu::where('name', config('voyager.bread.default_menu'))->firstOrFail();

            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Employments',
                'url'     => '',
                'route'   => 'voyager.employments.index',
            ]);

            $order = Voyager::model('MenuItem')->highestOrderMenuItem();

            if ( ! $menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-group',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => $order,
                ])->save();
            }
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
