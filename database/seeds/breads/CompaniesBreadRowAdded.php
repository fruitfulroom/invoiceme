<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class CompaniesBreadRowAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'companies')->first();

            \DB::table('data_rows')
                ->insert([
                    0  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'id',
                        'type'         => 'hidden',
                        'display_name' => 'Id',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 1,
                    ],
                    1  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'user_id',
                        'type'         => 'hidden',
                        'display_name' => 'User Id',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 1,
                        'delete'       => 0,
                        'details'      => '{"validation":{"rule":"required|exists:users,id","messages":{"required":"This owner must be set."}}}',
                        'order'        => 2,
                    ],
                    2  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'name',
                        'type'         => 'text',
                        'display_name' => 'Name',
                        'required'     => 1,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 3,
                    ],
                    3  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'slug',
                        'type'         => 'hidden',
                        'display_name' => 'Slug',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 4,
                    ],
                    4  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'image',
                        'type'         => 'image',
                        'display_name' => 'Image',
                        'required'     => 0,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|image"}}',
                        'order'        => 5,
                    ],
                    5  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'description',
                        'type'         => 'rich_text_box',
                        'display_name' => 'Description',
                        'required'     => 1,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|max:65534"}}',
                        'order'        => 6,
                    ],
                    6  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'website',
                        'type'         => 'text',
                        'display_name' => 'Website',
                        'required'     => 0,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255|url"}}',
                        'order'        => 6,
                    ],
                    7  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'stripe_publish_key',
                        'type'         => 'text',
                        'display_name' => 'Stripe Publish Key',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255|starts_with:pk_"}}',
                        'order'        => 7,
                    ],
                    8  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'stripe_secret_key',
                        'type'         => 'text',
                        'display_name' => 'Stripe Secret Key',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255|starts_with:sk_"}}',
                        'order'        => 8,
                    ],
                    9  => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'deleted_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Deleted At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 9,
                    ],
                    10 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'created_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Created At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 10,
                    ],
                    11 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'address',
                        'type'         => 'text',
                        'display_name' => 'Address',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 10,
                    ],
                    12 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'updated_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Updated At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 11,
                    ],
                    13 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'delivery_address',
                        'type'         => 'text',
                        'display_name' => 'Delivery Address',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 11,
                    ],
                    14 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'company_hasmany_product_relationship',
                        'type'         => 'relationship',
                        'display_name' => 'Products',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"model":"App\\\\Product","table":"products","type":"hasMany","column":"id","key":"id","label":"name","pivot_table":"categories","pivot":"0","taggable":"0"}',
                        'order'        => 12,
                    ],
                    15 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'email',
                        'type'         => 'text',
                        'display_name' => 'Email',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255|email"}}',
                        'order'        => 12,
                    ],
                    16 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'city',
                        'type'         => 'text',
                        'display_name' => 'City',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 12,
                    ],
                    17 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'company_belongsto_user_relationship',
                        'type'         => 'relationship',
                        'display_name' => 'Owner',
                        'required'     => 0,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 0,
                        'add'          => 1,
                        'delete'       => 0,
                        'details'      => '{"scope":"me","model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"billings","pivot":"0","taggable":"0"}',
                        'order'        => 13,
                    ],
                    18 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'province',
                        'type'         => 'text',
                        'display_name' => 'Province',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 13,
                    ],
                    19 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'company_belongstomany_user_relationship',
                        'type'         => 'relationship',
                        'display_name' => 'Employees',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"model":"App\\\\User","table":"users","type":"belongsToMany","column":"id","key":"id","label":"name","pivot_table":"employments","pivot":"1","taggable":"0"}',
                        'order'        => 14,
                    ],
                    20 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'postal_code',
                        'type'         => 'text',
                        'display_name' => 'Postal Code',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 14,
                    ],
                    21 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'company_hasmany_trusted_company_relationship',
                        'type'         => 'relationship',
                        'display_name' => 'Trusted Companies',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"model":"App\\\\TrustedCompany","table":"trusted_companies","type":"hasMany","column":"company_id","key":"id","label":"trusted_company_id","pivot_table":"billings","pivot":"0","taggable":"0"}',
                        'order'        => 15,
                    ],
                    22 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'phone',
                        'type'         => 'number',
                        'display_name' => 'Phone',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 15,
                    ],
                    23 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'bank_name',
                        'type'         => 'text',
                        'display_name' => 'Bank Name',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 16,
                    ],
                    24 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'account_number',
                        'type'         => 'text',
                        'display_name' => 'Account Number',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 17,
                    ],
                    25 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'sort_code',
                        'type'         => 'text',
                        'display_name' => 'Sort Code',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 18,
                    ],
                    26 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'iban',
                        'type'         => 'text',
                        'display_name' => 'Iban',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 19,
                    ],
                    27 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'bic_swift',
                        'type'         => 'text',
                        'display_name' => 'Bic Swift',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 20,
                    ],
                    28 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'signature',
                        'type'         => 'image',
                        'display_name' => 'Signature',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|image"}}',
                        'order'        => 22,
                    ],
                ]);
        } catch (Exception $e) {
            throw new Exception('exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}

