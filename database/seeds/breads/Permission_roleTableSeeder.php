<?php

use Illuminate\Database\Seeder;

class Permission_roleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            \DB::table('permission_role')
                ->delete();

            \DB::table('permission_role')
                ->insert([
                    0  => [
                        'permission_id' => 6,
                        'role_id'       => 1,
                    ],
                    1  => [
                        'permission_id' => 7,
                        'role_id'       => 1,
                    ],
                    2  => [
                        'permission_id' => 8,
                        'role_id'       => 1,
                    ],
                    3  => [
                        'permission_id' => 9,
                        'role_id'       => 1,
                    ],
                    4  => [
                        'permission_id' => 10,
                        'role_id'       => 1,
                    ],
                    5  => [
                        'permission_id' => 11,
                        'role_id'       => 1,
                    ],
                    6  => [
                        'permission_id' => 11,
                        'role_id'       => 3,
                    ],
                    7  => [
                        'permission_id' => 12,
                        'role_id'       => 1,
                    ],
                    8  => [
                        'permission_id' => 12,
                        'role_id'       => 3,
                    ],
                    9  => [
                        'permission_id' => 13,
                        'role_id'       => 1,
                    ],
                    10 => [
                        'permission_id' => 13,
                        'role_id'       => 3,
                    ],
                    11 => [
                        'permission_id' => 14,
                        'role_id'       => 1,
                    ],
                    12 => [
                        'permission_id' => 14,
                        'role_id'       => 3,
                    ],
                    13 => [
                        'permission_id' => 15,
                        'role_id'       => 1,
                    ],
                    14 => [
                        'permission_id' => 15,
                        'role_id'       => 3,
                    ],
                    15 => [
                        'permission_id' => 16,
                        'role_id'       => 1,
                    ],
                    16 => [
                        'permission_id' => 16,
                        'role_id'       => 3,
                    ],
                    17 => [
                        'permission_id' => 17,
                        'role_id'       => 1,
                    ],
                    18 => [
                        'permission_id' => 17,
                        'role_id'       => 3,
                    ],
                    19 => [
                        'permission_id' => 18,
                        'role_id'       => 1,
                    ],
                    20 => [
                        'permission_id' => 18,
                        'role_id'       => 3,
                    ],
                    21 => [
                        'permission_id' => 19,
                        'role_id'       => 1,
                    ],
                    22 => [
                        'permission_id' => 19,
                        'role_id'       => 3,
                    ],
                    23 => [
                        'permission_id' => 20,
                        'role_id'       => 1,
                    ],
                    24 => [
                        'permission_id' => 20,
                        'role_id'       => 3,
                    ],
                    25 => [
                        'permission_id' => 21,
                        'role_id'       => 1,
                    ],
                    26 => [
                        'permission_id' => 22,
                        'role_id'       => 1,
                    ],
                    27 => [
                        'permission_id' => 23,
                        'role_id'       => 1,
                    ],
                    28 => [
                        'permission_id' => 24,
                        'role_id'       => 1,
                    ],
                    29 => [
                        'permission_id' => 25,
                        'role_id'       => 1,
                    ],
                    30 => [
                        'permission_id' => 26,
                        'role_id'       => 1,
                    ],
                    31 => [
                        'permission_id' => 26,
                        'role_id'       => 3,
                    ],
                    32 => [
                        'permission_id' => 27,
                        'role_id'       => 1,
                    ],
                    33 => [
                        'permission_id' => 27,
                        'role_id'       => 3,
                    ],
                    34 => [
                        'permission_id' => 28,
                        'role_id'       => 1,
                    ],
                    35 => [
                        'permission_id' => 28,
                        'role_id'       => 3,
                    ],
                    36 => [
                        'permission_id' => 29,
                        'role_id'       => 1,
                    ],
                    37 => [
                        'permission_id' => 30,
                        'role_id'       => 1,
                    ],
                    38 => [
                        'permission_id' => 31,
                        'role_id'       => 1,
                    ],
                    39 => [
                        'permission_id' => 31,
                        'role_id'       => 3,
                    ],
                    40 => [
                        'permission_id' => 32,
                        'role_id'       => 1,
                    ],
                    41 => [
                        'permission_id' => 33,
                        'role_id'       => 1,
                    ],
                    42 => [
                        'permission_id' => 34,
                        'role_id'       => 1,
                    ],
                    43 => [
                        'permission_id' => 35,
                        'role_id'       => 1,
                    ],
                    44 => [
                        'permission_id' => 36,
                        'role_id'       => 1,
                    ],
                    45 => [
                        'permission_id' => 37,
                        'role_id'       => 1,
                    ],
                    46 => [
                        'permission_id' => 38,
                        'role_id'       => 1,
                    ],
                    47 => [
                        'permission_id' => 39,
                        'role_id'       => 1,
                    ],
                    48 => [
                        'permission_id' => 40,
                        'role_id'       => 1,
                    ],
                    49 => [
                        'permission_id' => 41,
                        'role_id'       => 1,
                    ],
                    50 => [
                        'permission_id' => 42,
                        'role_id'       => 1,
                    ],
                    51 => [
                        'permission_id' => 43,
                        'role_id'       => 1,
                    ],
                    52 => [
                        'permission_id' => 44,
                        'role_id'       => 1,
                    ],
                    53 => [
                        'permission_id' => 45,
                        'role_id'       => 1,
                    ],
                    54 => [
                        'permission_id' => 46,
                        'role_id'       => 1,
                    ],
                    55 => [
                        'permission_id' => 47,
                        'role_id'       => 1,
                    ],
                    56 => [
                        'permission_id' => 48,
                        'role_id'       => 1,
                    ],
                    57 => [
                        'permission_id' => 49,
                        'role_id'       => 1,
                    ],
                    58 => [
                        'permission_id' => 50,
                        'role_id'       => 1,
                    ],
                    59 => [
                        'permission_id' => 51,
                        'role_id'       => 1,
                    ],
                    60 => [
                        'permission_id' => 51,
                        'role_id'       => 3,
                    ],
                    61 => [
                        'permission_id' => 52,
                        'role_id'       => 1,
                    ],
                    62 => [
                        'permission_id' => 52,
                        'role_id'       => 3,
                    ],
                    63 => [
                        'permission_id' => 53,
                        'role_id'       => 1,
                    ],
                    64 => [
                        'permission_id' => 53,
                        'role_id'       => 3,
                    ],
                    65 => [
                        'permission_id' => 54,
                        'role_id'       => 1,
                    ],
                    66 => [
                        'permission_id' => 54,
                        'role_id'       => 3,
                    ],
                    67 => [
                        'permission_id' => 55,
                        'role_id'       => 1,
                    ],
                    68 => [
                        'permission_id' => 55,
                        'role_id'       => 3,
                    ],
                    69 => [
                        'permission_id' => 56,
                        'role_id'       => 1,
                    ],
                    70 => [
                        'permission_id' => 56,
                        'role_id'       => 3,
                    ],
                    71 => [
                        'permission_id' => 57,
                        'role_id'       => 1,
                    ],
                    72 => [
                        'permission_id' => 57,
                        'role_id'       => 3,
                    ],
                    73 => [
                        'permission_id' => 58,
                        'role_id'       => 1,
                    ],
                    74 => [
                        'permission_id' => 58,
                        'role_id'       => 3,
                    ],
                    75 => [
                        'permission_id' => 59,
                        'role_id'       => 1,
                    ],
                    76 => [
                        'permission_id' => 59,
                        'role_id'       => 3,
                    ],
                    77 => [
                        'permission_id' => 60,
                        'role_id'       => 1,
                    ],
                    78 => [
                        'permission_id' => 60,
                        'role_id'       => 3,
                    ],
                    79 => [
                        'permission_id' => 61,
                        'role_id'       => 1,
                    ],
                    80 => [
                        'permission_id' => 61,
                        'role_id'       => 3,
                    ],
                    81 => [
                        'permission_id' => 62,
                        'role_id'       => 1,
                    ],
                    82 => [
                        'permission_id' => 62,
                        'role_id'       => 3,
                    ],
                    83 => [
                        'permission_id' => 63,
                        'role_id'       => 1,
                    ],
                    84 => [
                        'permission_id' => 64,
                        'role_id'       => 1,
                    ],
                    85 => [
                        'permission_id' => 65,
                        'role_id'       => 1,
                    ],
                    86 => [
                        'permission_id' => 66,
                        'role_id'       => 1,
                    ],
                    87 => [
                        'permission_id' => 67,
                        'role_id'       => 1,
                    ],
                    88 => [
                        'permission_id' => 68,
                        'role_id'       => 1,
                    ],
                    89 => [
                        'permission_id' => 69,
                        'role_id'       => 1,
                    ],
                    90 => [
                        'permission_id' => 70,
                        'role_id'       => 1,
                    ],
                ]);
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
