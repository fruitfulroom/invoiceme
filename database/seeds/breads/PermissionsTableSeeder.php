<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            \DB::table('permissions')
                ->delete();

            \DB::table('permissions')
                ->insert([
                    0  => [
                        'id'         => 6,
                        'key'        => 'browse_categories',
                        'table_name' => 'categories',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    1  => [
                        'id'         => 7,
                        'key'        => 'read_categories',
                        'table_name' => 'categories',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    2  => [
                        'id'         => 8,
                        'key'        => 'edit_categories',
                        'table_name' => 'categories',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    3  => [
                        'id'         => 9,
                        'key'        => 'add_categories',
                        'table_name' => 'categories',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    4  => [
                        'id'         => 10,
                        'key'        => 'delete_categories',
                        'table_name' => 'categories',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    5  => [
                        'id'         => 11,
                        'key'        => 'browse_products',
                        'table_name' => 'products',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    6  => [
                        'id'         => 12,
                        'key'        => 'read_products',
                        'table_name' => 'products',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    7  => [
                        'id'         => 13,
                        'key'        => 'edit_products',
                        'table_name' => 'products',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    8  => [
                        'id'         => 14,
                        'key'        => 'add_products',
                        'table_name' => 'products',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    9  => [
                        'id'         => 15,
                        'key'        => 'delete_products',
                        'table_name' => 'products',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    10 => [
                        'id'         => 16,
                        'key'        => 'browse_companies',
                        'table_name' => 'companies',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    11 => [
                        'id'         => 17,
                        'key'        => 'read_companies',
                        'table_name' => 'companies',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    12 => [
                        'id'         => 18,
                        'key'        => 'edit_companies',
                        'table_name' => 'companies',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    13 => [
                        'id'         => 19,
                        'key'        => 'add_companies',
                        'table_name' => 'companies',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    14 => [
                        'id'         => 20,
                        'key'        => 'delete_companies',
                        'table_name' => 'companies',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    15 => [
                        'id'         => 21,
                        'key'        => 'browse_users',
                        'table_name' => 'users',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    16 => [
                        'id'         => 22,
                        'key'        => 'read_users',
                        'table_name' => 'users',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    17 => [
                        'id'         => 23,
                        'key'        => 'edit_users',
                        'table_name' => 'users',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    18 => [
                        'id'         => 24,
                        'key'        => 'add_users',
                        'table_name' => 'users',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    19 => [
                        'id'         => 25,
                        'key'        => 'delete_users',
                        'table_name' => 'users',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    20 => [
                        'id'         => 26,
                        'key'        => 'browse_orders',
                        'table_name' => 'orders',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    21 => [
                        'id'         => 27,
                        'key'        => 'read_orders',
                        'table_name' => 'orders',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    22 => [
                        'id'         => 28,
                        'key'        => 'edit_orders',
                        'table_name' => 'orders',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    23 => [
                        'id'         => 29,
                        'key'        => 'add_orders',
                        'table_name' => 'orders',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    24 => [
                        'id'         => 30,
                        'key'        => 'delete_orders',
                        'table_name' => 'orders',
                        'created_at' => '2020-04-19 17:04:57',
                        'updated_at' => '2020-04-19 17:04:57',
                    ],
                    25 => [
                        'id'         => 31,
                        'key'        => 'browse_admin',
                        'table_name' => null,
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    26 => [
                        'id'         => 32,
                        'key'        => 'browse_bread',
                        'table_name' => null,
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    27 => [
                        'id'         => 33,
                        'key'        => 'browse_database',
                        'table_name' => null,
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    28 => [
                        'id'         => 34,
                        'key'        => 'browse_media',
                        'table_name' => null,
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    29 => [
                        'id'         => 35,
                        'key'        => 'browse_compass',
                        'table_name' => null,
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    30 => [
                        'id'         => 36,
                        'key'        => 'browse_menus',
                        'table_name' => 'menus',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    31 => [
                        'id'         => 37,
                        'key'        => 'read_menus',
                        'table_name' => 'menus',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    32 => [
                        'id'         => 38,
                        'key'        => 'edit_menus',
                        'table_name' => 'menus',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    33 => [
                        'id'         => 39,
                        'key'        => 'add_menus',
                        'table_name' => 'menus',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    34 => [
                        'id'         => 40,
                        'key'        => 'delete_menus',
                        'table_name' => 'menus',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    35 => [
                        'id'         => 41,
                        'key'        => 'browse_roles',
                        'table_name' => 'roles',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    36 => [
                        'id'         => 42,
                        'key'        => 'read_roles',
                        'table_name' => 'roles',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    37 => [
                        'id'         => 43,
                        'key'        => 'edit_roles',
                        'table_name' => 'roles',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    38 => [
                        'id'         => 44,
                        'key'        => 'add_roles',
                        'table_name' => 'roles',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    39 => [
                        'id'         => 45,
                        'key'        => 'delete_roles',
                        'table_name' => 'roles',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    40 => [
                        'id'         => 46,
                        'key'        => 'browse_settings',
                        'table_name' => 'settings',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    41 => [
                        'id'         => 47,
                        'key'        => 'read_settings',
                        'table_name' => 'settings',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    42 => [
                        'id'         => 48,
                        'key'        => 'edit_settings',
                        'table_name' => 'settings',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    43 => [
                        'id'         => 49,
                        'key'        => 'add_settings',
                        'table_name' => 'settings',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    44 => [
                        'id'         => 50,
                        'key'        => 'delete_settings',
                        'table_name' => 'settings',
                        'created_at' => '2020-04-19 17:05:31',
                        'updated_at' => '2020-04-19 17:05:31',
                    ],
                    45 => [
                        'id'         => 51,
                        'key'        => 'browse_trusted_companies',
                        'table_name' => 'trusted_companies',
                        'created_at' => '2020-04-26 11:44:48',
                        'updated_at' => '2020-04-26 11:44:48',
                    ],
                    46 => [
                        'id'         => 52,
                        'key'        => 'read_trusted_companies',
                        'table_name' => 'trusted_companies',
                        'created_at' => '2020-04-26 11:44:48',
                        'updated_at' => '2020-04-26 11:44:48',
                    ],
                    47 => [
                        'id'         => 53,
                        'key'        => 'edit_trusted_companies',
                        'table_name' => 'trusted_companies',
                        'created_at' => '2020-04-26 11:44:48',
                        'updated_at' => '2020-04-26 11:44:48',
                    ],
                    48 => [
                        'id'         => 54,
                        'key'        => 'add_trusted_companies',
                        'table_name' => 'trusted_companies',
                        'created_at' => '2020-04-26 11:44:48',
                        'updated_at' => '2020-04-26 11:44:48',
                    ],
                    49 => [
                        'id'         => 55,
                        'key'        => 'delete_trusted_companies',
                        'table_name' => 'trusted_companies',
                        'created_at' => '2020-04-26 11:44:48',
                        'updated_at' => '2020-04-26 11:44:48',
                    ],
                    50 => [
                        'id'         => 56,
                        'key'        => 'browse_employments',
                        'table_name' => 'employments',
                        'created_at' => '2020-05-03 23:49:51',
                        'updated_at' => '2020-05-03 23:49:51',
                    ],
                    51 => [
                        'id'         => 57,
                        'key'        => 'read_employments',
                        'table_name' => 'employments',
                        'created_at' => '2020-05-03 23:49:51',
                        'updated_at' => '2020-05-03 23:49:51',
                    ],
                    52 => [
                        'id'         => 58,
                        'key'        => 'edit_employments',
                        'table_name' => 'employments',
                        'created_at' => '2020-05-03 23:49:51',
                        'updated_at' => '2020-05-03 23:49:51',
                    ],
                    53 => [
                        'id'         => 59,
                        'key'        => 'add_employments',
                        'table_name' => 'employments',
                        'created_at' => '2020-05-03 23:49:51',
                        'updated_at' => '2020-05-03 23:49:51',
                    ],
                    54 => [
                        'id'         => 60,
                        'key'        => 'delete_employments',
                        'table_name' => 'employments',
                        'created_at' => '2020-05-03 23:49:51',
                        'updated_at' => '2020-05-03 23:49:51',
                    ],
                    55 => [
                        'id'         => 61,
                        'key'        => 'browse_documents',
                        'table_name' => 'documents',
                        'created_at' => '2020-05-11 16:56:38',
                        'updated_at' => '2020-05-11 16:56:38',
                    ],
                    56 => [
                        'id'         => 62,
                        'key'        => 'read_documents',
                        'table_name' => 'documents',
                        'created_at' => '2020-05-11 16:56:38',
                        'updated_at' => '2020-05-11 16:56:38',
                    ],
                    57 => [
                        'id'         => 63,
                        'key'        => 'edit_documents',
                        'table_name' => 'documents',
                        'created_at' => '2020-05-11 16:56:38',
                        'updated_at' => '2020-05-11 16:56:38',
                    ],
                    58 => [
                        'id'         => 64,
                        'key'        => 'add_documents',
                        'table_name' => 'documents',
                        'created_at' => '2020-05-11 16:56:38',
                        'updated_at' => '2020-05-11 16:56:38',
                    ],
                    59 => [
                        'id'         => 65,
                        'key'        => 'delete_documents',
                        'table_name' => 'documents',
                        'created_at' => '2020-05-11 16:56:38',
                        'updated_at' => '2020-05-11 16:56:38',
                    ],
                    60 => [
                        'id'         => 66,
                        'key'        => 'browse_activity_log',
                        'table_name' => 'activity_log',
                        'created_at' => '2020-05-20 13:03:45',
                        'updated_at' => '2020-05-20 13:03:45',
                    ],
                    61 => [
                        'id'         => 67,
                        'key'        => 'read_activity_log',
                        'table_name' => 'activity_log',
                        'created_at' => '2020-05-20 13:03:45',
                        'updated_at' => '2020-05-20 13:03:45',
                    ],
                    62 => [
                        'id'         => 68,
                        'key'        => 'edit_activity_log',
                        'table_name' => 'activity_log',
                        'created_at' => '2020-05-20 13:03:45',
                        'updated_at' => '2020-05-20 13:03:45',
                    ],
                    63 => [
                        'id'         => 69,
                        'key'        => 'add_activity_log',
                        'table_name' => 'activity_log',
                        'created_at' => '2020-05-20 13:03:45',
                        'updated_at' => '2020-05-20 13:03:45',
                    ],
                    64 => [
                        'id'         => 70,
                        'key'        => 'delete_activity_log',
                        'table_name' => 'activity_log',
                        'created_at' => '2020-05-20 13:03:45',
                        'updated_at' => '2020-05-20 13:03:45',
                    ],
                ]);
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
