<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class EmploymentsBreadRowAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'employments')->first();

            \DB::table('data_rows')->insert([
                0 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'id',
                    'type'         => 'hidden',
                    'display_name' => 'Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 1,
                ],
                1 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'company_id',
                    'type'         => 'hidden',
                    'display_name' => 'Company Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 2,
                ],
                2 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'user_id',
                    'type'         => 'hidden',
                    'display_name' => 'User Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 3,
                ],
                3 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'created_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Created At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 4,
                ],
                4 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'updated_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Updated At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 5,
                ],
                5 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'employment_belongsto_company_relationship',
                    'type'         => 'relationship',
                    'display_name' => 'Company',
                    'required'     => 0,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{"scope":"canAnticipate","model":"App\\\\Company","table":"companies","type":"belongsTo","column":"company_id","key":"id","label":"name","pivot_table":"billings","pivot":"0","taggable":"0"}',
                    'order'        => 6,
                ],
                6 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'employment_belongsto_user_relationship',
                    'type'         => 'relationship',
                    'display_name' => 'User',
                    'required'     => 0,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{"model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"billings","pivot":"0","taggable":"0"}',
                    'order'        => 7,
                ],
            ]);
        } catch (Exception $e) {
            throw new Exception('exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}

