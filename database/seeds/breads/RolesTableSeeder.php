<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            \DB::table('roles')->delete();

            \DB::table('roles')->insert([
                0 => [
                    'id'           => 1,
                    'name'         => 'admin',
                    'display_name' => 'Administrator',
                    'created_at'   => '2020-04-30 13:56:30',
                    'updated_at'   => '2020-04-30 13:56:30',
                ],
                1 => [
                    'id'           => 2,
                    'name'         => 'user',
                    'display_name' => 'Normal User',
                    'created_at'   => '2020-04-30 13:56:30',
                    'updated_at'   => '2020-04-30 13:56:30',
                ],
                2 => [
                    'id'           => 3,
                    'name'         => 'creator',
                    'display_name' => 'Context Creator',
                    'created_at'   => '2020-04-30 13:56:30',
                    'updated_at'   => '2020-04-30 13:56:30',
                ],
            ]);
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
