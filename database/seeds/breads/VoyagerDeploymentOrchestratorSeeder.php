<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;

class VoyagerDeploymentOrchestratorSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = 'database/breads/seeds/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed(CategoriesBreadTypeAdded::class);
        $this->seed(CategoriesBreadRowAdded::class);
        $this->seed(ProductsBreadTypeAdded::class);
        $this->seed(ProductsBreadRowAdded::class);
        $this->seed(CompaniesBreadTypeAdded::class);
        $this->seed(CompaniesBreadRowAdded::class);
        $this->seed(Menu_itemsTableSeeder::class);
        $this->seed(UsersBreadTypeAdded::class);
        $this->seed(UsersBreadRowAdded::class);
        $this->seed(OrdersBreadTypeAdded::class);
        $this->seed(OrdersBreadRowAdded::class);
        $this->seed(TrustedCompaniesBreadTypeAdded::class);
        $this->seed(TrustedCompaniesBreadRowAdded::class);
        $this->seed(EmploymentsBreadTypeAdded::class);
        $this->seed(EmploymentsBreadRowAdded::class);
        $this->seed(DocumentsBreadTypeAdded::class);
        $this->seed(DocumentsBreadRowAdded::class);
        $this->seed(ActivityLogBreadTypeAdded::class);
        $this->seed(ActivityLogBreadRowAdded::class);
    }
}
