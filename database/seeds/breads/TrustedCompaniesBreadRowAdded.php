<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class TrustedCompaniesBreadRowAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'trusted_companies')->first();

            \DB::table('data_rows')->insert([
                0 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'trusted_company_belongsto_company_relationship',
                    'type'         => 'relationship',
                    'display_name' => 'Company',
                    'required'     => 0,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{"scope":"canAnticipate","model":"App\\\\Company","table":"companies","type":"belongsTo","column":"company_id","key":"id","label":"name","pivot_table":"billings","pivot":"0","taggable":"0"}',
                    'order'        => 1,
                ],
                1 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'trusted_company_belongsto_company_relationship_1',
                    'type'         => 'relationship',
                    'display_name' => 'Trusted Company',
                    'required'     => 0,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{"model":"App\\\\Company","table":"companies","type":"belongsTo","column":"trusted_company_id","key":"id","label":"name","pivot_table":"billings","pivot":"0","taggable":"0"}',
                    'order'        => 2,
                ],
                2 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'trusted_fund',
                    'type'         => 'text',
                    'display_name' => 'Trusted Fund',
                    'required'     => 1,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 1,
                    'add'          => 1,
                    'delete'       => 1,
                    'details'      => '{}',
                    'order'        => 3,
                ],
                3 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'trusted_period',
                    'type'         => 'text',
                    'display_name' => 'Trusted Period',
                    'required'     => 1,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 1,
                    'add'          => 1,
                    'delete'       => 1,
                    'details'      => '{}',
                    'order'        => 4,
                ],
                4 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'available',
                    'type'         => 'number',
                    'display_name' => 'Available Fund',
                    'required'     => 1,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 5,
                ],
                5 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'id',
                    'type'         => 'hidden',
                    'display_name' => 'Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 6,
                ],
                6 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'company_id',
                    'type'         => 'hidden',
                    'display_name' => 'Company Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 7,
                ],
                7 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'trusted_company_id',
                    'type'         => 'hidden',
                    'display_name' => 'Trusted Company Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 1,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 8,
                ],
                8 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'created_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Created At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 9,
                ],
                9 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'updated_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Updated At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 10,
                ],
            ]);
        } catch (Exception $e) {
            throw new Exception('exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}

