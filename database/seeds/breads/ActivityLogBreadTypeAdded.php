<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\MenuItem;

class ActivityLogBreadTypeAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'activity_log')
                ->first();

            if (is_bread_translatable($dataType)) {
                $dataType->deleteAttributeTranslations($dataType->getTranslatableAttributes());
            }

            if ($dataType) {
                DataType::where('name', 'activity_log')
                    ->delete();
            }

            \DB::table('data_types')
                ->insert([
                    'name'                  => 'activity_log',
                    'display_name_singular' => 'Activity Log',
                    'display_name_plural'   => 'Activity Logs',
                    'slug'                  => 'activity-log',
                    'icon'                  => 'voyager-logbook',
                    'model_name'            => 'Spatie\\Activitylog\\Models\\Activity',
                    'controller'            => null,
                    'policy_name'           => null,
                    'generate_permissions'  => 1,
                    'server_side'           => 1,
                    'details'               => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}',
                    'description'           => null,
                    'updated_at'            => null,
                    'created_at'            => null,
                    'id'                    => 16,
                ]);

            Voyager::model('Permission')
                ->generateFor('activity_log');

            $menu = Menu::where('name', config('voyager.bread.default_menu'))
                ->firstOrFail();

            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Activity Logs',
                'url'     => '',
                'route'   => 'voyager.activity-log.index',
            ]);

            $order = Voyager::model('MenuItem')
                ->highestOrderMenuItem();

            if ( ! $menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-logbook',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => $order,
                ])
                    ->save();
            }
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
