<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DocumentsBreadRowAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'documents')->first();

            \DB::table('data_rows')->insert([
                0 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'id',
                    'type'         => 'text',
                    'display_name' => 'Id',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 1,
                ],
                1 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'filepath',
                    'type'         => 'link',
                    'display_name' => 'Filepath',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 2,
                ],
                2 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'documentable_type',
                    'type'         => 'text',
                    'display_name' => 'Type',
                    'required'     => 1,
                    'browse'       => 0,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 3,
                ],
                3 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'documentable_id',
                    'type'         => 'number',
                    'display_name' => 'Document Type Id',
                    'required'     => 1,
                    'browse'       => 1,
                    'read'         => 1,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 4,
                ],
                4 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'created_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Created At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 5,
                ],
                5 => [
                    'data_type_id' => $dataType->id,
                    'field'        => 'updated_at',
                    'type'         => 'timestamp',
                    'display_name' => 'Updated At',
                    'required'     => 0,
                    'browse'       => 0,
                    'read'         => 0,
                    'edit'         => 0,
                    'add'          => 0,
                    'delete'       => 0,
                    'details'      => '{}',
                    'order'        => 6,
                ],
            ]);
        } catch (Exception $e) {
            throw new Exception('exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}

