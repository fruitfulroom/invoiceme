<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\MenuItem;

class CompaniesBreadTypeAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'companies')->first();

            if (is_bread_translatable($dataType)) {
                $dataType->deleteAttributeTranslations($dataType->getTranslatableAttributes());
            }

            if ($dataType) {
                DataType::where('name', 'companies')->delete();
            }

            \DB::table('data_types')
                ->insert([
                    'id'                    => 6,
                    'name'                  => 'companies',
                    'slug'                  => 'companies',
                    'display_name_singular' => 'Company',
                    'display_name_plural'   => 'Companies',
                    'icon'                  => 'voyager-company',
                    'model_name'            => 'App\\Company',
                    'policy_name'           => null,
                    'controller'            => null,
                    'description'           => null,
                    'generate_permissions'  => 1,
                    'server_side'           => 1,
                    'details'               => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}',
                    'created_at'            => null,
                    'updated_at'            => null,
                ]);


            Voyager::model('Permission')->generateFor('companies');

            $menu = Menu::where('name', config('voyager.bread.default_menu'))->firstOrFail();

            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Companies',
                'url'     => '',
                'route'   => 'voyager.companies.index',
            ]);

            $order = Voyager::model('MenuItem')->highestOrderMenuItem();

            if ( ! $menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-company',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => $order,
                ])->save();
            }
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
