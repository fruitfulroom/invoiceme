<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class CategoriesBreadRowAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'categories')
                ->first();

            \DB::table('data_rows')
                ->insert([
                    0 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'id',
                        'type'         => 'hidden',
                        'display_name' => 'Id',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 1,
                    ],
                    1 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'name',
                        'type'         => 'text',
                        'display_name' => 'Name',
                        'required'     => 1,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|min:3|max:255"}}',
                        'order'        => 2,
                    ],
                    2 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'slug',
                        'type'         => 'hidden',
                        'display_name' => 'Slug',
                        'required'     => 1,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 3,
                    ],
                    3 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'image',
                        'type'         => 'image',
                        'display_name' => 'Image',
                        'required'     => 0,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"image","add":{"rule":"required"},"edit":{"rule":"sometimes"}}}',
                        'order'        => 4,
                    ],
                    4 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'description',
                        'type'         => 'rich_text_box',
                        'display_name' => 'Description',
                        'required'     => 1,
                        'browse'       => 1,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"validation":{"rule":"required|max:65534"}}',
                        'order'        => 6,
                    ],
                    5 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'deleted_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Deleted At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 7,
                    ],
                    6 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'created_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Created At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 8,
                    ],
                    7 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'updated_at',
                        'type'         => 'timestamp',
                        'display_name' => 'Updated At',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 0,
                        'edit'         => 0,
                        'add'          => 0,
                        'delete'       => 0,
                        'details'      => '{}',
                        'order'        => 9,
                    ],
                    8 => [
                        'data_type_id' => $dataType->id,
                        'field'        => 'category_hasmany_product_relationship',
                        'type'         => 'relationship',
                        'display_name' => 'Products',
                        'required'     => 0,
                        'browse'       => 0,
                        'read'         => 1,
                        'edit'         => 1,
                        'add'          => 1,
                        'delete'       => 1,
                        'details'      => '{"model":"App\\\\Product","table":"products","type":"hasMany","column":"id","key":"id","label":"name","pivot_table":"categories","pivot":"0","taggable":"0"}',
                        'order'        => 10,
                    ],
                ]);
        } catch (Exception $e) {
            throw new Exception('exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}

