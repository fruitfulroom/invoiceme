<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\MenuItem;

class TrustedCompaniesBreadTypeAdded extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        try {
            \DB::beginTransaction();

            $dataType = DataType::where('name', 'trusted_companies')->first();

            if (is_bread_translatable($dataType)) {
                $dataType->deleteAttributeTranslations($dataType->getTranslatableAttributes());
            }

            if ($dataType) {
                DataType::where('name', 'trusted_companies')->delete();
            }

            \DB::table('data_types')->insert([
                'id'                    => 10,
                'name'                  => 'trusted_companies',
                'slug'                  => 'trusted-companies',
                'display_name_singular' => 'Trusted Company',
                'display_name_plural'   => 'Trusted Companies',
                'icon'                  => 'voyager-leaf',
                'model_name'            => 'App\\TrustedCompany',
                'policy_name'           => null,
                'controller'            => null,
                'description'           => null,
                'generate_permissions'  => 1,
                'server_side'           => 1,
                'details'               => '{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":"visible"}',
                'created_at'            => null,
                'updated_at'            => null,
            ]);


            Voyager::model('Permission')->generateFor('trusted_companies');

            $menu = Menu::where('name', config('voyager.bread.default_menu'))->firstOrFail();

            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Trusted Companies',
                'url'     => '',
                'route'   => 'voyager.trusted-companies.index',
            ]);

            $order = Voyager::model('MenuItem')->highestOrderMenuItem();

            if ( ! $menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-leaf',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => $order,
                ])->save();
            }
        } catch (Exception $e) {
            throw new Exception('Exception occur ' . $e);

            \DB::rollBack();
        }

        \DB::commit();
    }
}
