<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/test', function () {



    auth()->once([
        'email'    => 'm7vm7v@abv.bg',
        'password' => 'aaaaaaaa',
    ]);

    collect([1, 2, 3, 4, 5, 6])->each(function ($productId) {
        $product = \App\Product::whereId($productId)->first();

        Cart::add($product->id, $product->name, 1, $product->price / 100)->associate(Product::class);
    });

    $product = \App\Product::whereId(4)->first();

    $cartItem = Cart::search(function ($cartItem) use ($product) {
        return $cartItem->id === $product->id;
    })->first();

    dd($cartItem->qty);
});
