<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmploymentsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PaymentMethodsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SubscriptionsController;
use App\Http\Controllers\TrustedCompaniesController;
use App\Http\Controllers\UserNotificationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| SPA Routes
|
| - authenticating routes
| - sanctum/csrf-cookie
| - /oauth/clients/
| - /oauth/token
|--------------------------------------------------------------------------
*/
Route::middleware('auth:sanctum')->group(function () {
    Route::patch('/oauth/clients/{client}/passwordable', 'SanctumController@update');

    Route::get('/user', 'UsersController@show');
    Route::patch('/user', 'UsersController@update');
    Route::patch('/user/billing', 'UsersController@billing');
    Route::get('/user/intent', 'UsersController@intent');

    Route::get('/user/notifications', [UserNotificationsController::class, 'index']);

    Route::get('/user/payments', [PaymentMethodsController::class, 'index']);
    Route::post('/user/payments', [PaymentMethodsController::class, 'store']);
    Route::delete('/user/payments', [PaymentMethodsController::class, 'destroy']);

    Route::get('/subscriptions', [SubscriptionsController::class, 'index']);
    Route::post('/subscriptions', [SubscriptionsController::class, 'store']);
    Route::patch('/subscriptions', [SubscriptionsController::class, 'update']);
    Route::delete('/subscriptions', [SubscriptionsController::class, 'destroy']);
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->group(function () {
    Route::get('/products', [ProductsController::class, 'index']);
    Route::get('/products/{product}', [ProductsController::class, 'show']);
    Route::post('/products', [ProductsController::class, 'store']);
    Route::patch('/products/{product}', [ProductsController::class, 'update']);
    Route::delete('/products/{product}', [ProductsController::class, 'destroy']);

    Route::post('/products/related', 'ProductsController@getRelated');

    Route::get('/cart', [CartController::class, 'index']);
    Route::post('/cart', 'CartController@store');
    Route::patch('/cart', 'CartController@update');
    Route::delete('/cart/{product}', 'CartController@destroy');

    Route::post('/checkout', 'CheckoutController@store');

    Route::get('/orders', [OrdersController::class, 'index']);
    Route::patch('/orders/{order}', [OrdersController::class, 'update']);

    Route::get('/employments', [EmploymentsController::class, 'index']);
    Route::post('/employments', [EmploymentsController::class, 'store']);
    Route::delete('/employments/{employment}', [EmploymentsController::class, 'destroy']);

    Route::get('/trusted_fund/{company}', [TrustedCompaniesController::class, 'show']);
    Route::post('/trusted_fund', [TrustedCompaniesController::class, 'store']);
    Route::patch('/trusted_fund/{trustedFund}', [TrustedCompaniesController::class, 'update']);
    Route::delete('/trusted_fund/{trustedFund}', [TrustedCompaniesController::class, 'destroy']);

    Route::get('/companies/{company}', [CompaniesController::class, 'show']);
    Route::post('/companies', [CompaniesController::class, 'store']);
    Route::patch('/companies/{company}', [CompaniesController::class, 'update']);
    Route::delete('/companies/{company}', [CompaniesController::class, 'destroy']);
});
