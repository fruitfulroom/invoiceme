<?php

namespace App\Repositories;

use App\Product;

class EloquentProductRepository extends ProductRepository implements ProductRepositoryInterface
{

    /**
     * @return mixed
     */
    public function paginate()
    {
        return Product::take($this->limit)
            ->sorted($this->sort)
            ->byCategory($this->categorySlug)
            ->paginate($this->paginateLimit, ['*'], 'page', $this->page);
    }
}
