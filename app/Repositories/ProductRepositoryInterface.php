<?php

namespace App\Repositories;

interface ProductRepositoryInterface
{
    public function paginate();

    public function setSort($sort);

    public function setCategory($categorySlug);

    public function setLimit($limit);

    public function setPage($page);
}
