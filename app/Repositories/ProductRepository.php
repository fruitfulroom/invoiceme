<?php

namespace App\Repositories;

class ProductRepository
{

    protected $paginateLimit = 12;

    protected $sort;

    protected $categorySlug;

    protected $limit = '*';

    protected $page = 1;

    /**
     * @param $sort
     *
     * @return $this
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @param $categorySlug
     *
     * @return $this
     */
    public function setCategory($categorySlug)
    {
        $this->categorySlug = $categorySlug;

        return $this;
    }

    /**
     * @param $limit
     *
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param $page
     *
     * @return $this
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }
}
