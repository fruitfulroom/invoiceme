<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;

class CachingProductRepository extends ProductRepository implements ProductRepositoryInterface
{

    protected $products;

    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function paginate()
    {
        $categorySlug = $this->categorySlug ?: '*';
        $sort         = $this->sort ?: '*';

        $rememberKey = "products.$categorySlug.$sort.$this->limit.$this->page";

        return Cache::tags(['products'])->rememberForever($rememberKey, function () {
            return $this->products
                ->setSort($this->sort)
                ->setCategory($this->categorySlug)
                ->setLimit($this->limit)
                ->setPage($this->page)
                ->paginate();
        });
    }
}
