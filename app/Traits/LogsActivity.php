<?php

namespace App\Traits;

trait LogsActivity
{

    use \Spatie\Activitylog\Traits\LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;
}
