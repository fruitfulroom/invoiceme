<?php

namespace App\Listeners;

use App\Events\OrderReceived;
use App\Notifications\OrderReceivedNotification;
use Illuminate\Support\Facades\Notification;

class NotifyForOrderReceived
{

    /**
     * Handle the event.
     *
     * @param OrderReceived $event
     *
     * @return void
     */
    public function handle(OrderReceived $event)
    {
        $notification = new OrderReceivedNotification($event->order);

        $notifiableUsers = collect();

        if ($sellingCompany = $event->order->getSellingCompany()) {
            $notifiableUsers->push($sellingCompany->owner);

            $sellingCompany->notify($notification);
        }

        Notification::send($notifiableUsers, $notification);
    }
}
