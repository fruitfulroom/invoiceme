<?php

namespace App\Listeners;

use App\Events\OrderPayed;
use App\Notifications\OrderPayedNotification;
use Illuminate\Support\Facades\Notification;

class NotifyForOrderPayed
{

    /**
     * Handle the event.
     *
     * @param OrderPayed $event
     *
     * @return void
     */
    public function handle(OrderPayed $event)
    {
        $notification = new OrderPayedNotification($event->order);

        $notifiableUsers = collect([
            $event->order->user,
        ]);

        if ($event->order->company) {
            $notifiableUsers->push($event->order->company->owner);

            $event->order->company->notify($notification);
        }

        if ($sellingCompany = $event->order->getSellingCompany()) {
            $notifiableUsers->push($sellingCompany->owner);

            $sellingCompany->notify($notification);
        }

        Notification::send($notifiableUsers, $notification);
    }
}
