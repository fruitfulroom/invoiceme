<?php

namespace App\Listeners;

use App\Order;
use TCG\Voyager\Events\BreadDataUpdated;

class UpdateTheModelFromVoyager
{

    /**
     * Handle the event.
     *
     * @param BreadDataUpdated $event
     *
     * @return void
     */
    public function handle(BreadDataUpdated $event)
    {
        if ($event->data instanceof Order) {
            $order = $event->data;

            if (session()->has('triggeredEvent')) {
                $event = 'App\\Events\\' . session()->get('triggeredEvent');

                event(new $event($order));
            }
        }
    }
}
