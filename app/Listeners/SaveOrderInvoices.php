<?php

namespace App\Listeners;

use App\Document;
use App\TrustedCompany;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;

class SaveOrderInvoices implements ShouldQueue
{

    protected $order, $purchaser, $orderDueDate;

    /**
     * Handle the event.
     *
     * @param  $event
     *
     * @return bool
     */
    public function handle($event)
    {
        $this->setOrder($event->order)
            ->setPurchaser()
            ->setOrderDueDate()
            ->prepareStorage()
            ->createDocument('sales')
            ->createDocument('billing');

        return true;
    }

    /**
     * @param $order
     *
     * @return $this
     */
    protected function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return $this
     */
    protected function setPurchaser()
    {
        $purchaser = $this->order->company;

        if ( ! $purchaser) {
            $purchaser = $this->order->user;

            if ($this->order->user->billing) {
                $purchaser->address = $this->order->user->billing->address;
                $purchaser->phone = $this->order->user->billing->phone;
            }
        }

        $this->purchaser = $purchaser;

        return $this;
    }

    /**
     * @return $this
     */
    protected function setOrderDueDate()
    {
        $dueDate = now()->toDateString();

        if ($this->order->payment_gateway == 'trusted_fund') {
            $trustedPeriod = TrustedCompany::where('company_id',
                $this->order->getSellingCompany()->id)->where('trusted_company_id',
                $this->order->company_id)->first()->trusted_period;

            $dueDate = now()->addDays($trustedPeriod)->toDateString();
        }

        $this->orderDueDate = $dueDate;

        return $this;
    }

    /**
     * @return $this
     */
    protected function prepareStorage()
    {
        if ( ! File::isDirectory(storage_path('app/public/invoices/' . $this->order->id))) {
            File::makeDirectory(storage_path('app/public/invoices/' . $this->order->id), 0777, true, true);
        }

        return $this;
    }

    /**
     * @param $type
     *
     * @return $this
     */
    protected function createDocument($type)
    {
        $filename = $this->order->created_at->toDateTimeString() . ' - ' . ucfirst($type) . ' Invoice.pdf';
        $orderId  = $this->order->id;

        PDF::loadView("layouts.invoices.$type", [
            'purchaser' => $this->purchaser,
            'dueDate'   => $this->orderDueDate,
            'order'     => $this->order,
        ])->save(storage_path("app/public/invoices/$orderId/$filename"));

        $this->order->documents()->save(new Document([
            'filepath' => "invoices/$orderId/$filename",
        ]));

        return $this;
    }
}
