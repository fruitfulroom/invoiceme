<?php

namespace App\Listeners;

use App\Document;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;

class SaveOrderProforma implements ShouldQueue
{

    protected $order, $purchaser;

    /**
     * Handle the event.
     *
     * @param  $event
     *
     * @return bool
     */
    public function handle($event)
    {
        $this->setOrder($event->order)
            ->setPurchaser()
            ->prepareStorage()
            ->createDocument();

        return true;
    }

    /**
     * @param $order
     *
     * @return $this
     */
    protected function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return $this
     */
    protected function setPurchaser()
    {
        $purchaser = $this->order->company;

        if ( ! $purchaser) {
            $purchaser = $this->order->user;

            if ($this->order->user->billing) {
                $purchaser->address = $this->order->user->billing->address;
                $purchaser->phone = $this->order->user->billing->phone;
            }
        }

        $this->purchaser = $purchaser;

        return $this;
    }

    /**
     * @return $this
     */
    protected function prepareStorage()
    {
        if ( ! File::isDirectory(storage_path('app/public/invoices/' . $this->order->id))) {
            File::makeDirectory(storage_path('app/public/invoices/' . $this->order->id), 0777, true, true);
        }

        return $this;
    }

    /**
     * @param $type
     *
     * @return $this
     */
    protected function createDocument()
    {
        $filename = $this->order->created_at->toDateTimeString() . (
            $this->order->received
                ? ' - Invoice.pdf'
                : ' - Proforma.pdf'
            );

        $orderId = $this->order->id;

        PDF::loadView('layouts.invoices.proforma', [
            'purchaser' => $this->purchaser,
            'order'     => $this->order,
        ])->save(storage_path("app/public/invoices/$orderId/$filename"));

        $this->order->documents()->save(new Document([
            'filepath' => "invoices/$orderId/$filename",
        ]));

        return $this;
    }
}
