<?php

namespace App\Listeners;

use App\Events\OrderShipped;
use App\Notifications\OrderShippedNotification;
use Illuminate\Support\Facades\Notification;

class NotifyForOrderShipped
{

    /**
     * Handle the event.
     *
     * @param OrderShipped $event
     *
     * @return void
     */
    public function handle(OrderShipped $event)
    {
        $notification = new OrderShippedNotification($event->order);

        $notifiableUsers = collect([
            $event->order->user,
        ]);

        if ($event->order->company) {
            $notifiableUsers->push($event->order->company->owner);

            $event->order->company->notify($notification);
        }

        Notification::send($notifiableUsers, $notification);
    }
}
