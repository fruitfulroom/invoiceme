<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Notifications\OrderPlacedNotification;
use Illuminate\Support\Facades\Notification;

class NotifyForOrderPlaced
{

    /**
     * Handle the event.
     *
     * @param OrderCreated $event
     *
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        $notification = new OrderPlacedNotification($event->order);

        $notifiableUsers = collect([
            $event->order->user,
        ]);

        if ($event->order->company) {
            $notifiableUsers->push($event->order->company->owner);

            $event->order->company->notify($notification);
        }

        if ($sellingCompany = $event->order->getSellingCompany()) {
            $notifiableUsers->push($sellingCompany->owner);

            $sellingCompany->notify($notification);
        }

        Notification::send($notifiableUsers, $notification);
    }
}
