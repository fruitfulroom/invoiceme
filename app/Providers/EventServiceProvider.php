<?php

namespace App\Providers;

use App\Events\OrderCreated;
use App\Events\OrderPayed;
use App\Events\OrderReceived;
use App\Listeners\SaveOrderInvoices;
use App\Listeners\SaveOrderProforma;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderCreated::class => [
            SaveOrderInvoices::class,
            SaveOrderProforma::class,
        ],
        OrderPayed::class   => [
            SaveOrderInvoices::class,
            SaveOrderProforma::class,
        ],
        OrderReceived::class => [
            SaveOrderProforma::class,
        ],
    ];

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return true;
    }

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
