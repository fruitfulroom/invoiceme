<?php

namespace App\Providers;

use App\Category;
use App\Company;
use App\Employment;
use App\Order;
use App\Policies\CategoryPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\EmploymentPolicy;
use App\Policies\OrderPolicy;
use App\Policies\ProductPolicy;
use App\Policies\TrustedCompanyPolicy;
use App\Product;
use App\TrustedCompany;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Product::class        => ProductPolicy::class,
        Category::class       => CategoryPolicy::class,
        Company::class        => CompanyPolicy::class,
        Order::class          => OrderPolicy::class,
        TrustedCompany::class => TrustedCompanyPolicy::class,
        Employment::class     => EmploymentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
