<?php

namespace App\Providers;

use App\Repositories\CachingProductRepository;
use App\Repositories\EloquentProductRepository;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductRepositoryInterface::class, function ($app) {
            return new CachingProductRepository(new EloquentProductRepository);
        });
    }

    public function provides()
    {
        return [
            ProductRepositoryInterface::class,
        ];
    }
}
