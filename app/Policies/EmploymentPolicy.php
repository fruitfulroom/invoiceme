<?php

namespace App\Policies;

use App\Company;
use App\Employment;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Contracts\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;
use App\User as AppUser;

class EmploymentPolicy extends BasePolicy
{

    use HandlesAuthorization, VoyagerPolicy;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User       $user
     * @param Employment $employment
     *
     * @return mixed
     */
    public function view(User $user, Employment $employment)
    {
        $employments = $user->employments->pluck('company_id');
        $companies = $user->companies->pluck('id');

        if ($employments->concat($companies)->contains($employment->company_id)) {
            return true;
        }

        return $user->id == $employment->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if (request()->has('company_id')) {
            $subscriptionName = config('invoiceme.stripe.product.name');
            $subscriptionPlans = collect(config('invoiceme.stripe.plans'));
            $company = Company::whereId(request()->get('company_id'))->firstOrFail();
            $employees = $company->employees->pluck('user_id');

            if ( ! $employees->push($company->user_id)->contains($user->id)) {
                throw ValidationException::withMessages([
                    'company_id' => 'Only company owners or employees may proceed!',
                ]);
            }

            $user = AppUser::whereId($company->user_id)->first();

            if ( ! $user->subscribed($subscriptionName)) {
                throw ValidationException::withMessages([
                    'company_id' => 'The company owner must have a valid subscription!',
                ]);
            }

            $userSubscription = $subscriptionPlans->where('id',
                $user->subscription($subscriptionName)->stripe_plan)->first();

            if ($company->employees->count() >= $userSubscription['employees_allowed']) {
                throw ValidationException::withMessages([
                    'company_id' => 'The company has reached their employees limitation!',
                ]);
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User       $user
     * @param Employment $employment
     *
     * @return mixed
     */
    public function update(User $user, $employment)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User       $user
     * @param Employment $employment
     *
     * @return mixed
     */
    public function delete(User $user, $employment)
    {
        if ( ! is_object($employment->company)) {
            return false;
        }

        return $employment->company->user_id == $user->id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User       $user
     * @param Employment $employment
     *
     * @return mixed
     */
    public function restore(User $user, $employment)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User       $user
     * @param Employment $employment
     *
     * @return mixed
     */
    public function forceDelete(User $user, Employment $employment)
    {
        return false;
    }
}
