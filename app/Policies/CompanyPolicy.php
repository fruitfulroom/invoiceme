<?php

namespace App\Policies;

use App\Company;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Contracts\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;

class CompanyPolicy extends BasePolicy
{

    use HandlesAuthorization, VoyagerPolicy;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User    $user
     * @param Company $company
     *
     * @return mixed
     */
    public function view(User $user, Company $company)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        $subscriptionName = config('invoiceme.stripe.product.name');
        $subscriptionPlans = collect(config('invoiceme.stripe.plans'));

        if ( ! $user->subscribed($subscriptionName)) {
            throw ValidationException::withMessages([
                'id' => 'You must have a valid subscription!',
            ]);
        }

        $userSubscription = $subscriptionPlans->where('id',
            $user->subscription($subscriptionName)->stripe_plan)->first();

        if ($user->companies->count() >= $userSubscription['companies_allowed']) {
            throw ValidationException::withMessages([
                'id' => 'You have reached your maximum company creations!',
            ]);
        }

        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User    $user
     * @param Company $company
     *
     * @return mixed
     */
    public function update(User $user, $company)
    {
        $employees = $company->employees->pluck('user_id');

        return $employees->push($company->user_id)->contains($user->id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User    $user
     * @param Company $company
     *
     * @return mixed
     */
    public function delete(User $user, $company)
    {
        return $company->user_id == $user->id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User    $user
     * @param Company $company
     *
     * @return mixed
     */
    public function restore(User $user, $company)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User    $user
     * @param Company $company
     *
     * @return mixed
     */
    public function forceDelete(User $user, Company $company)
    {
        return false;
    }
}
