<?php

namespace App\Policies;

use App\User;

trait VoyagerPolicy
{

    public function before($user, $ability)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    //***************************************
    //               ____
    //              |  _ \
    //              | |_) |
    //              |  _ <
    //              | |_) |
    //              |____/
    //
    //      Browse our Data Type (B)READ
    //
    //****************************************
    //****************************************
    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @param      $model
     *
     * @return mixed
     */
    public function browse(User $user, $model)
    {
        return $this->viewAny($user, $model);
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of Voyager's Data Type B(R)EAD
    //
    //****************************************
    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @param      $model
     *
     * @return mixed
     */
    public function read(User $user, $model)
    {
        return $this->view($user, $model);
    }

    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************
    public function edit(User $user, $model)
    {
        return $this->update($user, $model);
    }

    //***************************************
    //
    //                   /\
    //                  /  \
    //                 / /\ \
    //                / ____ \
    //               /_/    \_\
    //
    //
    // Add a new item of our Data Type BRE(A)D
    //
    //****************************************
    public function add(User $user, $model)
    {
        return $this->create($user, $model);
    }

    //***************************************
    //                _____
    //               |  __ \
    //               | |  | |
    //               | |  | |
    //               | |__| |
    //               |_____/
    //
    //         Delete an item BREA(D)
    //
    //****************************************
    public function delete(User $user, $model)
    {
        return $this->delete($user, $model);
    }
}
