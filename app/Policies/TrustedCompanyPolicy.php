<?php

namespace App\Policies;

use App\Company;
use App\TrustedCompany;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Contracts\User;
use App\User as AppUser;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;

class TrustedCompanyPolicy extends BasePolicy
{

    use HandlesAuthorization, VoyagerPolicy;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User           $user
     * @param TrustedCompany $trustedCompany
     *
     * @return mixed
     */
    public function view(User $user, TrustedCompany $trustedCompany)
    {
        $companyEmployees = $trustedCompany->company->employees->pluck('user.id');

        if ($companyEmployees->push($trustedCompany->company->user_id)->contains($user->id)) {
            return true;
        }

        $trustedCompanyEmployees = $trustedCompany->trusted_company->employees->pluck('user.id');

        return $trustedCompanyEmployees->push($trustedCompany->trusted_company->user_id)->contains($user->id);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if (request()->has('company_id')) {
            $subscriptionName = config('invoiceme.stripe.product.name');
            $subscriptionPlans = collect(config('invoiceme.stripe.plans'));
            $company = Company::whereId(request()->get('company_id'))->firstOrFail();
            $employees = $company->employees->pluck('user_id');

            if (!$employees->push($company->user_id)->contains($user->id)) {
                throw ValidationException::withMessages([
                    'company_id' => 'Only company owners or employees may proceed!',
                ]);
            }

            $user = AppUser::whereId($company->user_id)->first();

            if ( ! $user->subscribed($subscriptionName)) {
                throw ValidationException::withMessages([
                    'company_id' => 'The company owner must have a valid subscription!',
                ]);
            }

            $userSubscription = $subscriptionPlans->where('id',
                $user->subscription($subscriptionName)->stripe_plan)->first();

            if ($company->trusted_companies->count() >= $userSubscription['trusted_companies_allowed']) {
                throw ValidationException::withMessages([
                    'company_id' => 'The company has reached their trusted buddies limit!',
                ]);
            }
        }

        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User           $user
     * @param TrustedCompany $trustedCompany
     *
     * @return mixed
     */
    public function update(User $user, $trustedCompany)
    {
        if ( ! is_object($trustedCompany->company)) {
            return false;
        }

        $employees = $trustedCompany->company->employees->pluck('user.id');

        return $employees->push($trustedCompany->company->user_id)->contains($user->id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User           $user
     * @param TrustedCompany $trustedCompany
     *
     * @return mixed
     */
    public function delete(User $user, $trustedCompany)
    {
        if ( ! is_object($trustedCompany->company)) {
            return false;
        }

        $employees = $trustedCompany->company->employees->pluck('user_id');

        return $employees->push($trustedCompany->company->user_id)->contains($user->id);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User           $user
     * @param TrustedCompany $trustedCompany
     *
     * @return mixed
     */
    public function restore(User $user, $trustedCompany)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User           $user
     * @param TrustedCompany $trustedCompany
     *
     * @return mixed
     */
    public function forceDelete(User $user, TrustedCompany $trustedCompany)
    {
        return false;
    }
}
