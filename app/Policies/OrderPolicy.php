<?php

namespace App\Policies;

use App\Order;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;
use TCG\Voyager\Contracts\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use TCG\Voyager\Policies\BasePolicy;

class OrderPolicy extends BasePolicy
{

    use HandlesAuthorization, VoyagerPolicy;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User  $user
     * @param Order $order
     *
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        $accessibleUsers = collect($order->user_id);

        if ($order->company) {
            $order->company->employees->each(function ($employee) use ($accessibleUsers) {
                $accessibleUsers->push($employee->user_id);
            });

            $accessibleUsers->push($order->company->user_id);
        }

        if ($sellingCompany = $order->getSellingCompany()) {
            $sellingCompany->employees->each(function ($employee) use ($accessibleUsers) {
                $accessibleUsers->push($employee->user_id);
            });

            $accessibleUsers->push($sellingCompany->user_id);
        }

        return $accessibleUsers->contains($user->id);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User  $user
     * @param Order $order
     *
     * @return mixed
     */
    public function update(User $user, $order)
    {
        $this->disallowSwitchingOff('payed', $order);
        $this->disallowSwitchingOff('shipped', $order);
        $this->disallowSwitchingOff('received', $order);

        $this->disallowNoneSellerChanges('shipped', $order, $user);
        $this->disallowNoneSellerChanges('payed', $order, $user);
        $this->disallowNoneSellerChanges('expected_at', $order, $user);

        $this->disallowNoneBuyersChanges('received', $order, $user);

        //manually flash the event that needs triggering
        $flashableInputs = [
            'received' => 'OrderReceived',
            'payed'    => 'OrderPayed',
            'shipped'  => 'OrderShipped',
        ];

        foreach (request()->all() as $inputName => $inputValue) {
            if ($this->inputHasBeenChanged($inputName, $order)) {
                if (array_key_exists($inputName, $flashableInputs)) {
                    session()->flash('triggeredEvent', $flashableInputs[$inputName]);
                }
            }
        }

        return $this->view($user, $order);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User  $user
     * @param Order $order
     *
     * @return mixed
     */
    public function delete(User $user, $order)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User  $user
     * @param Order $order
     *
     * @return mixed
     */
    public function restore(User $user, $order)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User  $user
     * @param Order $order
     *
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
        return false;
    }

    /**
     * @param $field
     * @param $order
     */
    protected function disallowSwitchingOff($field, $order)
    {
        $isSwitchedOff = request()->has($field) && request($field) == false
            || (request()->isMethod('PUT') && $order->$field && request()->missing($field));

        if ($isSwitchedOff && $this->inputHasBeenChanged($field, $order)) {
            throw ValidationException::withMessages([
                $field => ucfirst($field) . ' status can not be reverted!',
            ]);
        }
    }

    /**
     * @param $field
     * @param $order
     * @param $user
     */
    protected function disallowNoneSellerChanges($field, $order, $user)
    {
        $sellingCompany = $order->getSellingCompany();

        if (request()->has($field) && ! is_null(request($field)) && $sellingCompany) {
            $accessibleUsers = collect($sellingCompany->user_id);

            $sellingCompany->employees->each(function ($employee) use ($accessibleUsers) {
                $accessibleUsers->push($employee->user_id);
            });

            if ( ! $accessibleUsers->contains($user->id) && $this->inputHasBeenChanged($field, $order)) {
                throw ValidationException::withMessages([
                    $field => 'Only seller company owner or employees may proceed!',
                ]);
            }
        }
    }

    /**
     * @param $field
     * @param $order
     * @param $user
     */
    protected function disallowNoneBuyersChanges($field, $order, $user)
    {
        if (request()->has($field)) {
            $accessibleUsers = collect($order->user_id);

            if ($order->company) {
                $order->company->employees->each(function ($employee) use ($accessibleUsers) {
                    $accessibleUsers->push($employee->user_id);
                });

                $accessibleUsers->push($order->company->user_id);
            }

            if ( ! $accessibleUsers->contains($user->id) && $this->inputHasBeenChanged($field, $order)) {
                throw ValidationException::withMessages([
                    $field => 'Only buyers may proceed!',
                ]);
            }
        }
    }

    /**
     * @param $field
     * @param $order
     *
     * @return bool
     */
    protected function inputHasBeenChanged($field, $order)
    {
        if (request()->has($field)) {
            if (request($field) === 'on') {
                request()->merge([$field => true]);
            }

            if (request($field) === 'off') {
                request()->merge([$field => false]);
            }

            if ($field == 'expected_at') {
                try {
                    $dateField = Carbon::createFromIsoFormat('MM/DD/YYYY h:mm A', request($field));

                    request()->merge([$field => $dateField]);
                } catch (\Exception $exception) {
                }
            }
        }

        return request($field) != $order->$field;
    }
}
