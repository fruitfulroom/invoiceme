<?php

namespace App;

use App\Traits\LogsActivity;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Laravel\Scout\Searchable;
use Facades\App\Cart;
use TCG\Voyager\Http\Controllers\ContentTypes\Image;
use TCG\Voyager\Http\Controllers\ContentTypes\MultipleImage;
use TCG\Voyager\Models\DataType;

class Product extends Model
{

    //    use SoftDeletes, LogsActivity, Searchable;
    use SoftDeletes, LogsActivity, Sluggable;

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'user_id', 'company_id'];

    protected $guarded = [];

    protected $appends = ['formatted_price', 'image_path', 'gallery_paths'];

    protected $with = ['company', 'categories'];

    protected static $flushableEvents = ['created', 'updated', 'deleted'];

    protected static function boot()
    {
        parent::boot();

        foreach (self::$flushableEvents as $event)
        {
            static::$event(function (Product $product) {
                Cache::tags(['products'])->flush();
            });
        }
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return ['slug'];
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return string
     */
    public function getFormattedPriceAttribute()
    {
        return formattedPrice($this->price);
    }

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        if ($this->image) {
            return url($this->image);
        }

        return url('products/default.jpeg');
    }

    /**
     * @return string
     */
    public function getGalleryPathsAttribute()
    {
        if ($this->gallery) {
            $gallery = collect(\GuzzleHttp\json_decode($this->gallery));

            return $gallery->map(function ($imagePath) {
                return url($imagePath);
            });
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * @return array
     */
    public function toSearchableArray()
    {
        $product = $this->toArray();

        $product['categories'] = $this->categories->pluck('name')->toArray();

        return $product;
    }

    /**
     * @param mixed $hardCheckedQuantityValue
     *
     * @return bool
     */
    public function hasAvailabilityFromCart($hardCheckedQuantityValue = false)
    {
        if ($hardCheckedQuantityValue) {
            return $this->quantity >= $hardCheckedQuantityValue;
        }

        $cartProduct = Cart::getProduct($this);

        if (is_null($cartProduct)) {
            return $this->quantity >= 1;
        }

        return $this->quantity >= $cartProduct->qty;
    }

    /**
     * @param $query
     * @param $sort
     *
     * @return mixed
     */
    public function scopeSorted($query, $sort)
    {
        if ($sort == 'high') {
            $query->orderByDesc('price');
        }

        if ($sort == 'low') {
            $query->orderBy('price');
        }

        return $query;
    }

    /**
     * @param $query
     * @param $categorySlug
     *
     * @return mixed
     */
    public function scopeByCategory($query, $categorySlug)
    {
        if (is_null($categorySlug)) {
            return $query;
        }

        $query->whereHas('categories', function ($query) use ($categorySlug) {
            $query->whereSlug($categorySlug);
        });

        return $query;
    }

    /**
     * @param $validatedInput
     *
     * @return string
     */
    public function saveImage($validatedInput)
    {
        if (!array_key_exists('image', $validatedInput)) {
            return '';
        }

        $dataType = DataType::whereModelName(self::class)->with('rows')->first();
        $dataRow  = $dataType->rows->where('field', 'image')->first();

        return (new Image(request(), 'products', $dataRow, new \stdClass))->handle();
    }

    /**
     * @param $validatedInput
     *
     * @return string
     */
    public function saveGallery($validatedInput)
    {
        if (!array_key_exists('gallery', $validatedInput)) {
            return '';
        }

        $dataType = DataType::whereModelName(self::class)->with('rows')->first();
        $dataRow  = $dataType->rows->where('field', 'gallery')->first();

        return (new MultipleImage(request(), 'products', $dataRow, new \stdClass))->handle();
    }

    //voyager specific
    public function getPriceBrowseAttribute()
    {
        return formattedPrice($this->price);
    }

    public function getPriceReadAttribute()
    {
        return formattedPrice($this->price);
    }
}
