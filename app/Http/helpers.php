<?php

if ( ! function_exists('formattedPrice')) {
    function formattedPrice($price)
    {
        return @money_format('$%i', $price / 100);
    }
}
