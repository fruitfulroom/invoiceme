<?php

namespace App\Http\Controllers;

use App\Events\OrderPayed;
use App\Events\OrderReceived;
use App\Events\OrderShipped;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * @OA\Get(path="/api/orders",
     *   tags={"Orders"},
     *   summary="Returns orders",
     *   description="Returns a collection of orders",
     *   operationId="getOrder",
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "orders":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = auth()->user()->orders->map(function ($order) {
            $order->documents;

            return $order;
        });

        return response([
            'orders' => $orders,
        ], 201);
    }

    /**
     * @OA\Patch(path="/api/orders/{order}",
     *   tags={"Orders"},
     *   summary="Updates the order",
     *   description="Updates the order",
     *   operationId="updateOrder",
     *   @OA\Parameter(
     *     name="order",
     *     in="query",
     *     description="The order's id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="shipped",
     *     in="query",
     *     description="The order's shipped status",
     *     required=false,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="expected_at",
     *     in="query",
     *     description="The order's expected_at status",
     *     required=false,
     *     @OA\Schema(
     *       type="date"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="payed",
     *     in="query",
     *     description="The order's payed status",
     *     required=false,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="received",
     *     in="query",
     *     description="The order's received status",
     *     required=false,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="notes",
     *     in="query",
     *     description="The order's notes",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully updated the order",
     *     @OA\Schema(
     *       additionalProperties={
     *         "order":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @param Order                     $order
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Order $order)
    {
        $validatedInput = $request->validate([
            'shipped'     => 'boolean',
            'expected_at' => 'nullable|required_if:shipped,true|date',
            'payed'       => 'boolean',
            'received'    => 'boolean',
            'notes'       => 'string',
        ]);

        $this->authorize('update', $order);

        $order->update($validatedInput);

        if (request()->has('shipped')) {
            event(new OrderShipped($order));
        }

        if (request()->has('payed')) {
            event(new OrderPayed($order));
        }

        if (request()->has('received')) {
            event(new OrderReceived($order));
        }

        return response([
            'order' => $order,
        ], 201);
    }
}
