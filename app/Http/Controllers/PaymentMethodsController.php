<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentMethodsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $methods = collect();

        if (auth()->user()->hasPaymentMethod()) {
            foreach (auth()->user()->paymentMethods() as $method) {
                $methods->add([
                    'id'        => $method->id,
                    'brand'     => $method->card->brand,
                    'last_four' => $method->card->last4,
                    'exp_month' => $method->card->exp_month,
                    'exp_year'  => $method->card->exp_year,
                ]);
            }
        }

        return response([
            'success' => true,
            'methods' => $methods,
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'payment_method' => 'required'
        ]);

        $user = auth()->user();

        if ($user->stripe_id == null) {
            $user->createAsStripeCustomer();
        }

        $user->addPaymentMethod($validatedInput['payment_method']);
        $user->updateDefaultPaymentMethod($validatedInput['payment_method']);

        return response([
            'success' => true,
            'message' => 'Payment method successfully added!',
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validatedRequest = $request->validate([
            'id' => 'required',
        ]);

        foreach (auth()->user()->paymentMethods() as $method) {
            if ($method->id == $validatedRequest['id']) {
                $method->delete();

                break;
            }
        }

        return response([
            'success' => true,
            'message' => 'Successfully removed payment method!',
        ], 201);
    }
}
