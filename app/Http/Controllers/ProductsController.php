<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Repositories\CachingProductRepository;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    private $relatedProductsNumber = 6;

    private $repository;

    public function __construct(CachingProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @OA\Get(path="/api/products",
     *   tags={"Products"},
     *   summary="Returns products",
     *   description="Returns a collection of products",
     *   operationId="getProducts",
     *   @OA\Parameter(
     *     name="category",
     *     in="query",
     *     description="The filtered category slug",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="sort",
     *     in="query",
     *     description="The sort string as: 'high, low'",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "categories":"object",
     *         "products":"object",
     *         "selected_category":"string",
     *         "pagination":"array",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validatedInput = $request->validate([
            'category' => 'exists:categories,slug',
            'sort'     => 'in:high,low',
            'page'     => 'numeric',
        ]);

        $sort     = array_key_exists('sort', $validatedInput) ? $validatedInput['sort'] : null;
        $category = array_key_exists('category', $validatedInput) ? $validatedInput['category'] : null;
        $page     = array_key_exists('page', $validatedInput) ? $validatedInput['page'] : null;

        $products = $this->repository
            ->setSort($sort)
            ->setCategory($category)
            ->setLimit(12)
            ->setPage($page)
            ->paginate();

        return response([
            'categories'         => Category::all(),
            'paginated_products' => $products,
            'selected_category'  => $category,
        ], 201);
    }

    /**
     * @OA\Get(path="/api/products/related",
     *   tags={"Products"},
     *   summary="Returns related products",
     *   description="Returns a collection of realted products",
     *   operationId="getRelatedProducts",
     *   @OA\Parameter(
     *     name="slug",
     *     in="query",
     *     description="The product's slug",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "products":"object",
     *         "criterias":"array",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getRelated(Request $request)
    {
        $criterias = $request->validate([
            'slug' => 'sometimes|string',
        ]);

        if ($request->has('slug')) {
            $relatedProduct = Product::whereSlug($criterias['slug'])
                ->first();
        }

        $products = Product::with('company')
            ->take($this->relatedProductsNumber)
            ->get();

        return response([
            'products'  => $products,
            'criterias' => $criterias,
        ], 201);
    }

    /**
     * @OA\Get(path="/api/products/{product}",
     *   tags={"Products"},
     *   summary="Returns more information about a product",
     *   description="Returns more information about a product",
     *   operationId="showProduct",
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "product":"array",
     *       }
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="product",
     *     in="path",
     *     description="The product's slug",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     *
     * @param Product $product
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function show(Product $product)
    {
        return response([
            'product' => $product,
        ], 201);
    }

    /**
     * @OA\Post(path="/api/products",
     *   tags={"Products"},
     *   summary="Stores the product",
     *   description="Stores the product in the database",
     *   operationId="storeProduct",
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="The company id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="The product's name",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="subtitle",
     *     in="query",
     *     description="The product's subtitle",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="image",
     *     in="query",
     *     description="The product's image",
     *     required=true,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="gallery",
     *     in="query",
     *     description="The product's gallery",
     *     required=false,
     *     @OA\Schema(
     *       type="files"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="description",
     *     in="query",
     *     description="The product's description",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="price",
     *     in="query",
     *     description="The product's price",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="quantity",
     *     in="query",
     *     description="The product's quantity",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="confirmed",
     *     in="query",
     *     description="The product's confirmation status",
     *     required=true,
     *     @OA\Schema(
     *       type="boolean"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_height",
     *     in="query",
     *     description="The product's size height",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_width",
     *     in="query",
     *     description="The product's size width",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_depth",
     *     in="query",
     *     description="The product's size depth",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="gross_weight",
     *     in="query",
     *     description="The product's gross weight",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="cubic_weight",
     *     in="query",
     *     description="The product's cubic weight",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="weight_unit",
     *     in="query",
     *     description="The product's weight unit",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully stored the product",
     *     @OA\Schema(
     *       additionalProperties={
     *         "product":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'company_id'   => 'required|exists:companies,id',
            'name'         => 'required|string',
            'image'        => 'required|image',
            'gallery.*'    => 'image',
            'subtitle'     => 'required|string',
            'description'  => 'required|string',
            'price'        => 'required|numeric',
            'quantity'     => 'required|numeric',
            'confirmed'    => 'required|boolean',
            'size_height'  => 'required|numeric',
            'size_width'   => 'required|numeric',
            'size_depth'   => 'required|numeric',
            'gross_weight' => 'required|numeric',
            'cubic_weight' => 'required|numeric',
            'weight_unit'  => 'required|string',
        ]);

        $this->authorize('create', Product::class);

        $validatedInput['image']   = (new Product)->saveImage($validatedInput);
        $validatedInput['gallery'] = (new Product)->saveGallery($validatedInput);

        $product = auth()
            ->user()
            ->products()
            ->create($validatedInput);

        return response([
            'product' => $product,
        ], 201);
    }

    /**
     * @OA\Patch(path="/api/products/{product}",
     *   tags={"Products"},
     *   summary="Updates the product",
     *   description="Updates the product's data",
     *   operationId="updateProduct",
     *   @OA\Parameter(
     *     name="product",
     *     in="query",
     *     description="The product's slug",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="The product's name",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="subtitle",
     *     in="query",
     *     description="The product's subtitle",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="image",
     *     in="query",
     *     description="The product's image",
     *     required=false,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="gallery",
     *     in="query",
     *     description="The product's gallery",
     *     required=false,
     *     @OA\Schema(
     *       type="files"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="description",
     *     in="query",
     *     description="The product's description",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="price",
     *     in="query",
     *     description="The product's price",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="quantity",
     *     in="query",
     *     description="The product's quantity",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="confirmed",
     *     in="query",
     *     description="The product's confirmation status",
     *     required=false,
     *     @OA\Schema(
     *       type="boolean"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_height",
     *     in="query",
     *     description="The product's size height",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_width",
     *     in="query",
     *     description="The product's size width",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="size_depth",
     *     in="query",
     *     description="The product's size depth",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="gross_weight",
     *     in="query",
     *     description="The product's gross weight",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="cubic_weight",
     *     in="query",
     *     description="The product's cubic weight",
     *     required=false,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="weight_unit",
     *     in="query",
     *     description="The product's weight unit",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully updated the product",
     *     @OA\Schema(
     *       additionalProperties={
     *         "product":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     * @param Product                   $product
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Product $product)
    {
        $validatedInput = $request->validate([
            'name'         => 'string',
            'subtitle'     => 'string',
            'description'  => 'string',
            'image'        => 'image',
            'gallery.*'    => 'image',
            'price'        => 'numeric',
            'quantity'     => 'numeric',
            'confirmed'    => 'boolean',
            'size_height'  => 'numeric',
            'size_width'   => 'numeric',
            'size_depth'   => 'numeric',
            'gross_weight' => 'numeric',
            'cubic_weight' => 'numeric',
            'weight_unit'  => 'string',
        ]);

        $this->authorize('update', $product);

        $validatedInput['image']   = $product->saveImage($validatedInput);
        $validatedInput['gallery'] = $product->saveGallery($validatedInput);

        $product->update($validatedInput);

        return response([
            'product' => $product->fresh(),
        ], 201);
    }

    /**
     * @OA\Delete(path="/api/products/{product}",
     *   tags={"Products"},
     *   summary="Removes the product",
     *   description="Removes the product from a database",
     *   operationId="removeProduct",
     *   @OA\Parameter(
     *     name="product",
     *     in="path",
     *     description="The product's slug",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully removed the product from the database",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="Error: The product was not found in the cart!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Product $product
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        $product->delete();

        return response([
            'message' => 'Product removed from database!',
        ], 201);
    }
}
