<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{

    /**
     * @OA\Get(path="/api/companies/{company}",
     *   tags={"Companies"},
     *   summary="Returns information about company",
     *   description="Returns company information",
     *   operationId="showCompany",
     *   @OA\Parameter(
     *     name="company",
     *     in="path",
     *     description="The company id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "companies":"array",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Company $company
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $this->authorize('view', $company);

        return response([
            'company' => $company,
        ], 201);
    }

    /**
     * @OA\Post(path="/api/companies",
     *   tags={"Companies"},
     *   summary="Stores new company",
     *   description="Stores the company to the database",
     *   operationId="storeCompany",
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="The company's name",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="description",
     *     in="query",
     *     description="The company's description",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="image",
     *     in="query",
     *     description="The company's image",
     *     required=true,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="signature",
     *     in="query",
     *     description="The company's signature",
     *     required=true,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="stripe_publish_key",
     *     in="query",
     *     description="The company's stripe publish key",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="stripe_secret_key",
     *     in="query",
     *     description="The company's stripe secret key",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="The company's email",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="website",
     *     in="query",
     *     description="The company's website",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address",
     *     in="query",
     *     description="The company's address",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address",
     *     in="query",
     *     description="The company's address",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="delivery_address",
     *     in="query",
     *     description="The company's delivery_address",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="city",
     *     in="query",
     *     description="The company's city",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="province",
     *     in="query",
     *     description="The company's province",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="postal_code",
     *     in="query",
     *     description="The company's postal_code",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     description="The company's phone",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="bank_name",
     *     in="query",
     *     description="The company's bank_name",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="account_number",
     *     in="query",
     *     description="The company's account_number",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="sort_code",
     *     in="query",
     *     description="The company's sort_code",
     *     required=true,
     *     @OA\Schema(
     *       type="number"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="iban",
     *     in="query",
     *     description="The company's iban",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="bic_swift",
     *     in="query",
     *     description="The company's bic_swift",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully stored the company",
     *     @OA\Schema(
     *       additionalProperties={
     *         "company":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'name'               => 'required|string|min:3|max:255',
            'description'        => 'required|string|max:65534',
            'image'              => 'required|image',
            'signature'          => 'required|image',
            'stripe_publish_key' => 'required|string|min:3|max:255|starts_with:pk_',
            'stripe_secret_key'  => 'required|string|min:3|max:255|starts_with:sk_',
            'email'              => 'required|min:3|max:255|email',
            'website'            => 'required|string|min:3|max:255|url',
            'address'            => 'required|string|min:3|max:255',
            'delivery_address'   => 'required|string|min:3|max:255',
            'city'               => 'required|string|min:3|max:255',
            'province'           => 'required|string|min:3|max:255',
            'postal_code'        => 'required|string|min:3|max:255',
            'phone'              => 'required|string|min:3|max:255',
            'bank_name'          => 'required|string|min:3|max:255',
            'account_number'     => 'required|string|min:3|max:255',
            'sort_code'          => 'required|numeric',
            'iban'               => 'required|string|min:3|max:255',
            'bic_swift'          => 'required|string|min:3|max:255',
        ]);

        $this->authorize('create', Company::class);

        $validatedInput['image'] = (new Company)->saveImage($validatedInput);
        $validatedInput['signature'] = (new Company)->saveSignature($validatedInput);

        $company = auth()->user()->companies()->create($validatedInput);

        return response([
            'company' => $company,
        ], 201);
    }

    /**
     * @OA\Patch(path="/api/companies/{company}",
     *   tags={"Companies"},
     *   summary="Updates the company",
     *   description="Updates the company's data",
     *   operationId="updateCompany",
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="The company's name",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="description",
     *     in="query",
     *     description="The company's description",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="image",
     *     in="query",
     *     description="The company's image",
     *     required=false,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="signature",
     *     in="query",
     *     description="The company's signature",
     *     required=false,
     *     @OA\Schema(
     *       type="file"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="stripe_publish_key",
     *     in="query",
     *     description="The company's stripe publish key",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="stripe_secret_key",
     *     in="query",
     *     description="The company's stripe secret key",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     in="query",
     *     description="The company's email",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="website",
     *     in="query",
     *     description="The company's website",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address",
     *     in="query",
     *     description="The company's address",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="delivery_address",
     *     in="query",
     *     description="The company's delivery_address",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="city",
     *     in="query",
     *     description="The company's city",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="province",
     *     in="query",
     *     description="The company's province",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="postal_code",
     *     in="query",
     *     description="The company's postal_code",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     in="query",
     *     description="The company's phone",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="bank_name",
     *     in="query",
     *     description="The company's bank_name",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="account_number",
     *     in="query",
     *     description="The company's account_number",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="sort_code",
     *     in="query",
     *     description="The company's sort_code",
     *     required=false,
     *     @OA\Schema(
     *       type="number"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="iban",
     *     in="query",
     *     description="The company's iban",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="bic_swift",
     *     in="query",
     *     description="The company's bic_swift",
     *     required=false,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully updated the company",
     *     @OA\Schema(
     *       additionalProperties={
     *         "company":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     * @param Company                   $company
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Company $company)
    {
        $validatedInput = $request->validate([
            'name'               => 'string|min:3|max:255',
            'description'        => 'string|max:65534',
            'image'              => 'image',
            'signature'          => 'image',
            'stripe_publish_key' => 'string|min:3|max:255|starts_with:pk_',
            'stripe_secret_key'  => 'string|min:3|max:255|starts_with:sk_',
            'email'              => 'min:3|max:255|email',
            'website'            => 'string|min:3|max:255|url',
            'address'            => 'string|min:3|max:255',
            'delivery_address'   => 'string|min:3|max:255',
            'city'               => 'string|min:3|max:255',
            'province'           => 'string|min:3|max:255',
            'postal_code'        => 'string|min:3|max:255',
            'phone'              => 'string|min:3|max:255',
            'bank_name'          => 'string|min:3|max:255',
            'account_number'     => 'string|min:3|max:255',
            'sort_code'          => 'numeric',
            'iban'               => 'string|min:3|max:255',
            'bic_swift'          => 'string|min:3|max:255',
        ]);

        $this->authorize('update', $company);

        $validatedInput['image'] = $company->saveImage($validatedInput);

        $company->update($validatedInput);

        return response([
            'company' => $company->fresh(),
        ], 201);
    }

    /**
     * @OA\Delete(path="/api/companies/{company}",
     *   tags={"Companies"},
     *   summary="Removes the company",
     *   description="Removes the company from a database",
     *   operationId="removeCompany",
     *   @OA\Parameter(
     *     name="company",
     *     in="path",
     *     description="The company's id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully removed the company from the database",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Company $company
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Company $company)
    {
        $this->authorize('delete', $company);

        $company->delete();

        return response([
            'message' => 'The company had been removed from database!',
        ], 201);
    }
}
