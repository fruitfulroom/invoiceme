<?php

namespace App\Http\Controllers;

use Facades\App\Carts;
use App\Product;
use Illuminate\Http\Request;
use Facades\App\Cart;

class CartController extends Controller
{

    /**
     * @OA\Get(path="/api/cart",
     *   tags={"Cart"},
     *   summary="Returns carts",
     *   description="Returns a map of companies with products",
     *   operationId="getCart",
     *   @OA\Parameter(
     *     name="type",
     *     in="query",
     *     description="The cart type",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="The filtered company id",
     *     required=false,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "carts":"array",
     *         "type":"string"
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validatedInput = $request->validate([
            'type'       => 'required',
            'company_id' => '',
        ]);

        $carts = Carts::setCarts($validatedInput['type'])
            ->filterByCompany($validatedInput)
            ->mapModel()
            ->groupByCompany()
            ->mapDynamically()
            ->getCarts();

        return response([
            'carts' => $carts,
            'type'  => $validatedInput['type'],
        ], 200);
    }

    /**
     * @OA\Post(path="/api/cart",
     *   tags={"Cart"},
     *   summary="Stores the product",
     *   description="Stores the product to the given cart",
     *   operationId="storeCart",
     *   @OA\Parameter(
     *     name="type",
     *     in="query",
     *     description="The cart type",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="The product id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully stored the product to the cart",
     *     @OA\Schema(
     *       additionalProperties={
     *         "product":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="There is no availability for this product!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'type' => 'required',
            'id'   => 'required|exists:products,id',
        ]);

        $product = Product::whereId($validatedInput['id'])->with('company')->firstOrFail();

        try {
            $cartProduct = Cart::addProduct($product, $validatedInput['type']);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return response([
            'product' => $cartProduct,
        ], 201);
    }

    /**
     * @OA\Patch(path="/api/cart",
     *   tags={"Cart"},
     *   summary="Updates the product",
     *   description="Updates the product's quantity",
     *   operationId="updateCart",
     *   @OA\Parameter(
     *     name="type",
     *     in="query",
     *     description="The cart type",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="quantity",
     *     in="query",
     *     description="The quantity",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="The product's id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully updated the product in the cart",
     *     @OA\Schema(
     *       additionalProperties={
     *         "product":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="There is no availability for this product!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedInput = $request->validate([
            'type'     => 'required',
            'quantity' => 'required|numeric',
            'id'       => 'required|exists:products,id',
        ]);

        $product = Product::whereId($validatedInput['id'])->with('company')->first();

        try {
            $cartProduct = Cart::updateProductQuantity($product, $validatedInput['quantity'], $validatedInput['type']);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }

        return response([
            'product' => $cartProduct,
        ], 201);
    }

    /**
     * @OA\Delete(path="/api/cart/{product}",
     *   tags={"Cart"},
     *   summary="Removes the product",
     *   description="Removes the product from a cart",
     *   operationId="removeCartProduct",
     *   @OA\Parameter(
     *     name="type",
     *     in="query",
     *     description="The cart type",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="product",
     *     in="path",
     *     description="The product's slug",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully removed the product from the cart",
     *     @OA\Schema(
     *       additionalProperties={
     *         "cart":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="Error: The product was not found in the cart!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     * @param Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $validatedInput = $request->validate([
            'type' => 'required',
        ]);

        Cart::removeProduct($product, $validatedInput['type']);

        return response([
            'message' => 'Product removed from cart!',
        ], 201);
    }
}
