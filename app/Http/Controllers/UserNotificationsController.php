<?php

namespace App\Http\Controllers;

class UserNotificationsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = tap(auth()->user()->notifications)->markAsRead();

        return response([
            'success'       => true,
            'notifications' => $notifications,
        ]);
    }
}
