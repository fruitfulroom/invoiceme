<?php

namespace App\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Http\Request;

class VoyagerController extends VoyagerBaseController
{
    public function show(Request $request, $id)
    {
        $data = parent::show($request, $id);
        $data->dataType->readRows = $data->dataType->readRows->transform(function($item, $key) {

            if (property_exists($item->details, 'type')) {
                if ($item->details->type == 'belongsTo') {
                    $details = $item->details;
                    $details->view = 'vendor.voyager.formfields.belongsTo';
                    $item->details = $details;
                }

                if ($item->details->type == 'belongsToMany') {
                    $details = $item->details;
                    $details->view = 'vendor.voyager.formfields.belongsToMany';
                    $item->details = $details;
                }
            }

            if (property_exists($item->details, 'scope')) {
                if ($item->details->scope == 'orderable') {
                    $details = $item->details;
                    $details->view = 'vendor.voyager.formfields.hasManyDocuments';
                    $item->details = $details;
                }
            }

            return $item;
        });

        return $data;
    }
}
