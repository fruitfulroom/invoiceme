<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::whereId($request->user()->id)->with('billing')->first();

        return response([
            'user' => $user,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedInput = $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|max:255|email|unique:users,email,' . auth()->id(),
            'password' => 'sometimes|nullable|string|min:8|confirmed',
        ]);

        $user = auth()->user();

        $userAttributes = [
            'name'  => $validatedInput['name'],
            'email' => $validatedInput['email'],
        ];

        if ($request->filled('password')) {
            $userAttributes['password'] = bcrypt($validatedInput['password']);
        }

        $user->update($userAttributes);

        return response([
            'user' => $user->fresh(),
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function billing(Request $request)
    {
        $validatedInput = $request->validate([
            'address'      => 'required|string|max:255',
            'city'         => 'required|string|max:255',
            'province'     => 'required|string|max:255',
            'postal_code'  => 'required|string',
            'phone'        => 'required|string',
            'name_on_card' => 'required|string|max:255',
        ]);

        $user = auth()->user();

        $user->billing()->updateOrCreate([
            'user_id' => $user->id,
        ], $validatedInput);

        return response([
            'user' => User::whereId($user->id)->with('billing')->first(),
        ], 200);
    }

    /**
     * Creates an intent for payment so we can capture the payment
     * method for the user.
     *
     * @param Request $request The request data from the user.
     *
     * @return StripeSetupIntent
     */
    public function intent(Request $request)
    {
        return $request->user()->createSetupIntent();
    }
}
