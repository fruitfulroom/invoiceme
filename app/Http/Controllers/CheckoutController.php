<?php

namespace App\Http\Controllers;

use Facades\App\Cart;
use App\Rules\PaymentGatewayExists;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

    /**
     * @OA\Post(path="/api/checkout",
     *   tags={"Checkout"},
     *   summary="Checkout",
     *   description="Checkout the company's products from the cart",
     *   operationId="checkoutCart",
     *   @OA\Parameter(
     *     name="gateway",
     *     in="query",
     *     description="The gateway: 'TrustFound'",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="The company id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="authorized_company_id",
     *     in="query",
     *     description="The authorized company id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully checked out the company's products from the cart",
     *     @OA\Schema(
     *       additionalProperties={
     *         "data":"array",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="There were not enough items to proceed this checkout!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'gateway'               => ['required', 'string', new PaymentGatewayExists()],
            'stripe_token'          => 'sometimes|string',
            'payment_method_id'     => 'sometimes|string',
            'company_id'            => 'required|exists:companies,id',
            'authorized_company_id' => 'sometimes|exists:companies,id',
        ]);

        $gateway = '\App\Http\Payments\\' . $validatedInput['gateway'] . 'Payment';

        Cart::checkout(new $gateway(), $validatedInput);

        return response([
            'success' => true,
            'message' => 'Successfully charged the client!',
        ], 201);
    }
}
