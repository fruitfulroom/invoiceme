<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employment;
use Illuminate\Http\Request;

class EmploymentsController extends Controller
{

    /**
     * @OA\Get(path="/api/employments",
     *   tags={"Employments"},
     *   summary="Returns employments",
     *   description="Returns a collection of employments",
     *   operationId="getEmployments",
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "employments":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response([
            'employments' => auth()->user()->employments,
        ], 201);
    }

    /**
     * @OA\Post(path="/api/employments",
     *   tags={"Employments"},
     *   summary="Stores new employment",
     *   description="Stores the employment to the database",
     *   operationId="storeEmployment",
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="The company id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     description="The user id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully stored the employment",
     *     @OA\Schema(
     *       additionalProperties={
     *         "employment":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'company_id' => 'required|exists:companies,id',
            'user_id'    => 'required|exists:users,id',
        ]);

        $this->authorize('create', Employment::class);

        $company = Company::whereId($validatedInput['company_id'])->first();

        $employment = $company->employees()->create([
            'user_id' => $validatedInput['user_id'],
        ]);

        return response([
            'employment' => $employment,
        ], 201);
    }

    /**
     * @OA\Delete(path="/api/employments/{employment}",
     *   tags={"Employments"},
     *   summary="Removes the employment",
     *   description="Removes the employments from the database",
     *   operationId="removeEmployment",
     *   @OA\Parameter(
     *     name="employment",
     *     in="query",
     *     description="The employment id",
     *     required=true,
     *     @OA\Schema(
     *       type="string"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully removed the employment",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=409,
     *     description="Error: The employment was not found!",
     *     @OA\Schema(
     *       additionalProperties={
     *         "success":"boolean",
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request    $request
     * @param Employment $employment
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Employment $employment)
    {
        $this->authorize('delete', $employment);

        $employment->delete();

        return response([
            'message' => 'Employment removed from employment!',
        ], 201);
    }
}
