<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Passport\Client;

class SanctumController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param Client $client
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client)
    {
        if ($client->user()->firstOrFail()->id !== auth()->user()->id) {
            abort(401, 'Unauthorised');
        }

        $client->update([
            'password_client' => true,
        ]);

        return response($client, 200);
    }
}
