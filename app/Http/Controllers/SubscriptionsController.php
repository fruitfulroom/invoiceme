<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stripeProductName = config('invoiceme.stripe.product.name');

        return response([
            'success'              => true,
            'subscriptions'        => config('invoiceme.stripe.plans'),
            'current_subscription' => auth()->user()->subscription($stripeProductName),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'planId'    => 'required|string',
            'paymentId' => 'required|string',
        ]);

        $request->user()->newSubscription(config('invoiceme.stripe.product.name'),
            $validatedInput['planId'])->create($validatedInput['paymentId']);

        return response([
            'success' => true,
            'message' => 'Successfully subscribed!',
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedInput = $request->validate([
            'planId'         => 'required|string',
            'originalPlanId' => 'required|string',
        ]);

        $request->user()->subscription(config('invoiceme.stripe.product.name'))->swap($validatedInput['planId'],
            $validatedInput['originalPlanId']);

        return response([
            'success' => true,
            'message' => 'Successfully updated your subscription!',
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->user()->subscription(config('invoiceme.stripe.product.name'))->cancelNow();

        return response([
            'success' => true,
            'message' => 'Successfully cancelled your subscription!',
        ], 201);
    }
}
