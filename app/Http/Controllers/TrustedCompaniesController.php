<?php

namespace App\Http\Controllers;

use App\Company;
use App\TrustedCompany;
use Illuminate\Http\Request;

class TrustedCompaniesController extends Controller
{

    /**
     * @OA\Get(path="/api/trusted_fund/{company}",
     *   tags={"Trusted Fund"},
     *   summary="Returns trusted fund information",
     *   description="Returns trusted fund information",
     *   operationId="showTrustedFund",
     *   @OA\Parameter(
     *     name="authorized_company_id",
     *     in="query",
     *     description="The authorized company id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company",
     *     in="path",
     *     description="The filtered company id",
     *     required=true,
     *     @OA\Schema(
     *       type="int"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully returned data",
     *     @OA\Schema(
     *       additionalProperties={
     *         "trusted_fund":"array",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=422,
     *     description="Error: Unprocessable Entity",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *         "errors":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     * @param Company $company
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Company $company)
    {
        $validatedInput = $request->validate([
            'authorized_company_id' => 'required|exists:companies,id',
        ]);

        $authorizedCompany = Company::whereId($validatedInput['authorized_company_id'])->first();
        $trustedInstance = $company->trusted_companies->where('trusted_company_id', $authorizedCompany->id)->first();

        try {
            $this->authorize('view', $trustedInstance);
        } catch (\Exception $exception) {
            $trustedInstance = [];
        }

        return response([
            'trusted_fund' => $trustedInstance,
        ], 201);
    }

    /**
     * @OA\Post(path="/api/trusted_fund",
     *   tags={"Trusted Fund"},
     *   summary="Stores new trusted fund",
     *   description="Stores the trusted fund to the database",
     *   operationId="storeTrustedFund",
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="The company id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="trusted_company_id",
     *     in="query",
     *     description="The trusted company id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="trusted_fund",
     *     in="query",
     *     description="The trusted fund",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="trusted_period",
     *     in="query",
     *     description="The trusted period in days",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully stored the trusted fund",
     *     @OA\Schema(
     *       additionalProperties={
     *         "trust_fund":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $validatedInput = $request->validate([
            'company_id'         => 'required|exists:companies,id',
            'trusted_company_id' => 'required|exists:companies,id',
            'trusted_fund'       => 'required|numeric',
            'trusted_period'     => 'required|numeric',
        ]);

        $this->authorize('create', TrustedCompany::class);

        $company = Company::whereId($validatedInput['company_id'])->first();

        $trustedFund = $company->trusted_companies()->create($validatedInput);

        return response([
            'trusted_fund' => $trustedFund,
        ], 201);
    }

    /**
     * @OA\Patch(path="/api/trusted_fund/{trustedFund}",
     *   tags={"Trusted Fund"},
     *   summary="Updates the trusted fund",
     *   description="Updates the trusted fund's data",
     *   operationId="updateTrustedFund",
     *   @OA\Parameter(
     *     name="trusted_fund",
     *     in="query",
     *     description="The trusted fund",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="trusted_period",
     *     in="query",
     *     description="The trusted period in days",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully updated the trusted fund",
     *     @OA\Schema(
     *       additionalProperties={
     *         "trust_fund":"object",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param  \Illuminate\Http\Request $request
     * @param TrustedCompany            $trustedFund
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, TrustedCompany $trustedFund)
    {
        $validatedInput = $request->validate([
            'trusted_fund'   => 'required|numeric',
            'trusted_period' => 'required|numeric',
        ]);

        $this->authorize('update', $trustedFund);

        $trustedFund->update($validatedInput);

        return response([
            'trust_fund' => $trustedFund->fresh(),
        ], 201);
    }

    /**
     * @OA\Delete(path="/api/trusted_fund/{trustedFund}",
     *   tags={"Trusted Fund"},
     *   summary="Removes the trusted fund",
     *   description="Removes the trusted fund from a database",
     *   operationId="removeTrustedFund",
     *   @OA\Parameter(
     *     name="trustedFund",
     *     in="path",
     *     description="The trustedFund's id",
     *     required=true,
     *     @OA\Schema(
     *       type="integer"
     *     )
     *   ),
     *   @OA\Response(
     *     response=201,
     *     description="Successfully removed the trusted fund from the database",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   @OA\Response(
     *     response=401,
     *     description="Error: Unauthorized",
     *     @OA\Schema(
     *       additionalProperties={
     *         "message":"string",
     *       }
     *     )
     *   ),
     *   security={{
     *     "passport":{}
     *   }}
     * )
     *
     * @param TrustedCompany $trustedFund
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(TrustedCompany $trustedFund)
    {
        $this->authorize('delete', $trustedFund);

        $trustedFund->delete();

        return response([
            'message' => 'The rusted fund has been removed from database!',
        ], 201);
    }
}
