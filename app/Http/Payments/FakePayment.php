<?php

namespace App\Http\Payments;

use App\OrderProduct;

class FakePayment extends CheckoutablePayment
{

    public function authorizeCharge(): CheckoutablePayment
    {
        return $this;
    }

    public function proceed(): CheckoutablePayment
    {
        return $this;
    }

    public function makeOrder(): CheckoutablePayment
    {
        $this->order = auth()->user()->orders()->create([
            'billing_subtotal' => $this->cart->subtotal() * 100,
            'billing_tax'      => $this->cart->tax() * 100,
            'billing_total'    => $this->cart->total() * 100,
            'company_id'       => $this->authorizedCompanyId,
            'payed'            => true,
            'payment_gateway'  => 'fake',
        ]);

        $this->cart->content()->each(function ($item) {
            OrderProduct::create([
                'order_id'   => $this->order->id,
                'product_id' => $item->model->id,
                'quantity'   => $item->qty,
            ]);
        });

        return $this;
    }
}
