<?php

namespace App\Http\Payments;

use App\Company;
use App\Events\OrderCreated;

abstract class CheckoutablePayment
{

    protected $cart, $company, $cartContents, $cartTotal, $order, $authorizedCompanyId, $paymentMethodId;

    abstract public function authorizeCharge(): self;

    public function setValidatedInputs(array $validatedInput): self
    {
        $this->authorizedCompanyId = array_key_exists('authorized_company_id',
            $validatedInput) ? $validatedInput['authorized_company_id'] : false;

        $this->paymentMethodId = array_key_exists('payment_method_id',
            $validatedInput) ? $validatedInput['payment_method_id'] : false;

        return $this;
    }

    public function setCart($cart): self
    {
        $this->cart = $cart;

        $this->cartContents = $cart->content()->map(function ($item) {
            return $item->model->slug . ', ' . $item->qty;
        })->values()->toJson();

        $this->cartTotal = ((double)str_replace(' ', '', $cart->total())) * 100;

        return $this;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    abstract public function proceed(): self;

    abstract public function makeOrder(): self;

    public function triggerAfterEvents(): self
    {
        event(new OrderCreated($this->order));

        return $this;
    }
}
