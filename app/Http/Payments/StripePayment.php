<?php

namespace App\Http\Payments;

use App\OrderProduct;
use Stripe\Stripe;

class StripePayment extends CheckoutablePayment
{

    public function authorizeCharge(): CheckoutablePayment
    {
        if ( ! $this->paymentMethodId) {
            throw new \Exception('Invalid payment method id provided!', 401);
        }

        return $this;
    }

    public function proceed(): CheckoutablePayment
    {
        Stripe::setApiKey($this->company->stripe_secret_key);

        auth()->user()->charge($this->cartTotal, $this->paymentMethodId, [
            'amount'        => $this->cartTotal,
            'currency'      => 'USD',
            'description'   => 'Order',
            'receipt_email' => auth()->user()->email,
            'metadata'      => [
                'contents' => $this->cartContents,
                'quantity' => $this->cart->count(),
            ],
        ]);

        return $this;
    }

    public function makeOrder(): CheckoutablePayment
    {
        $this->order = auth()->user()->orders()->create([
            'billing_subtotal' => $this->cart->subtotal() * 100,
            'billing_tax'      => $this->cart->tax() * 100,
            'billing_total'    => $this->cart->total() * 100,
            'company_id'       => $this->authorizedCompanyId,
            'payed'            => true,
            'payment_gateway'  => 'stripe',
        ]);

        $this->cart->content()->each(function ($item) {
            OrderProduct::create([
                'order_id'   => $this->order->id,
                'product_id' => $item->model->id,
                'quantity'   => $item->qty,
            ]);
        });

        return $this;
    }
}
