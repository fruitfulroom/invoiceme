<?php

namespace App\Http\Payments;

use App\OrderProduct;

class TrustedFundPayment extends CheckoutablePayment
{

    public function authorizeCharge(): CheckoutablePayment
    {
        $trustedCompanies = $this->company->trusted_companies;

        if ( ! $this->authorizedCompanyId) {
            throw new \Exception('Invalid authorized company id provided!', 401);
        }

        if ( ! $trustedCompanies->contains('trusted_company_id', $this->authorizedCompanyId)) {
            throw new \Exception('The trust found does not exist!', 401);
        }

        $funds = $trustedCompanies->where('trusted_company_id', $this->authorizedCompanyId)->first();

        if ($this->cartTotal > $funds->available) {
            throw new \Exception('There are not enough funds!', 401);
        }

        return $this;
    }

    public function proceed(): CheckoutablePayment
    {
        $trustedFund = $this->company->trusted_companies->where('trusted_company_id', $this->authorizedCompanyId)->first();

        $trustedFund->update([
            'available' => $trustedFund->available - $this->cartTotal,
        ]);

        return $this;
    }

    public function makeOrder(): CheckoutablePayment
    {
        $this->order = auth()->user()->orders()->create([
            'billing_subtotal' => $this->cart->subtotal() * 100,
            'billing_tax'      => $this->cart->tax() * 100,
            'billing_total'    => $this->cart->total() * 100,
            'company_id'       => $this->authorizedCompanyId,
            'payed'            => false,
            'payment_gateway'  => 'trusted_fund',
        ]);

        $this->cart->content()->each(function ($item) {
            OrderProduct::create([
                'order_id'   => $this->order->id,
                'product_id' => $item->model->id,
                'quantity'   => $item->qty,
            ]);
        });

        return $this;
    }
}
