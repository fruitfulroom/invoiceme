<?php

namespace App;

use Facades\App\Cart;

class Carts
{

    protected $carts;

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setCarts($type = 'default')
    {
        $this->carts = Cart::instance($type)->content();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarts()
    {
        return $this->carts;
    }

    /**
     * @param $validatedInput
     *
     * @return $this
     */
    public function filterByCompany($validatedInput)
    {
        $this->carts = $this->carts->filter(function ($cartItem) use ($validatedInput) {
            return array_key_exists('company_id', $validatedInput)
                ? in_array($validatedInput['company_id'], [$cartItem->model->company->id, '0', null])
                : true;
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function mapModel()
    {
        $this->carts = $this->carts->map(function ($cartItem) {
            $cartItem->product = $cartItem->model;

            return $cartItem;
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function groupByCompany()
    {
        $this->carts = $this->carts->groupBy('product.company.id');

        return $this;
    }

    /**
     * @return $this
     */
    public function mapDynamically()
    {
        $this->carts = $this->carts->map(function ($companyItems) {
            $company = $companyItems->first()->product->company;

            Cart::instance('dynamic-' . $company->id)->destroy();

            $companyItems->each(function ($cartItem) {
                Cart::instance('dynamic-' . $cartItem->product->company->id)
                    ->add($cartItem->id, $cartItem->name, $cartItem->qty, $cartItem->price)
                    ->associate(Product::class);
            });

            $cart = Cart::getCartData('dynamic-' . $company->id);

            $cart['content'] = $cart['content']->map(function ($cartItem) {
                $item = $cartItem->toArray();
                $item['model'] = $cartItem->model;

                return $item;
            });

            return [
                'id'                 => $company->id,
                'name'               => $company->name,
                'slug'               => $company->slug,
                'stripe_publish_key' => $company->stripe_publish_key,
                'cart'               => $cart,
            ];
        });

        return $this;
    }
}
