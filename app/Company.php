<?php

namespace App;

use App\Traits\LogsActivity;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use TCG\Voyager\Http\Controllers\ContentTypes\Image;
use TCG\Voyager\Models\DataType;

class Company extends Model
{

    use SoftDeletes, Notifiable, LogsActivity, Sluggable;

    private $cart_products = [];

    protected $hidden = ['stripe_secret_key'];

    protected $guarded = [];

    protected $appends = ['cart_products', 'image_path'];

    /**
     * @param Collection $products
     */
    public function setCartProductsAttribute(Collection $products)
    {
        $this->cart_products = $products;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return ['slug'];
    }

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        if ($this->image) {
            return url($this->image);
        }

        return url('companies/default.png');
    }

    /**
     * @return array
     */
    public function getCartProductsAttribute()
    {
        return $this->cart_products;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employment::class)->with('user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trusted_companies()
    {
        return $this->hasMany(TrustedCompany::class, 'company_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Document::class, 'documentable');
    }

    /**
     * Returns only the companies where the logged user is owner or employee
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeCanAnticipate($query)
    {
        return $query->where('user_id', auth()->user()->id)->orWhereHas('employees', function (Builder $query) {
            $query->where('employments.user_id', auth()->user()->id);
        });
    }

    /**
     * @param $validatedInput
     *
     * @return mixed|string
     */
    public function saveImage($validatedInput)
    {
        if (!array_key_exists('image', $validatedInput)) {
            return '';
        }

        $dataType = DataType::whereModelName(self::class)->with('rows')->first();
        $dataRow  = $dataType->rows->where('field', 'image')->first();

        return (new Image(request(), 'companies', $dataRow, new \stdClass))->handle();
    }

    /**
     * @param $validatedInput
     *
     * @return mixed|string
     */
    public function saveSignature($validatedInput)
    {
        if (!array_key_exists('signature', $validatedInput)) {
            return '';
        }

        $dataType = DataType::whereModelName(self::class)->with('rows')->first();
        $dataRow  = $dataType->rows->where('field', 'signature')->first();

        return (new Image(request(), 'companies', $dataRow, new \stdClass))->handle();
    }
}
