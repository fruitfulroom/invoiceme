<?php

namespace App;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Order extends Model
{

    use SoftDeletes, LogsActivity;

    protected $guarded = [];

    protected $appends = ['formatted_total_price'];

    protected $dates = ['expected_at'];

    protected $casts = [
        'payed'    => 'boolean',
        'shipped'  => 'boolean',
        'received' => 'boolean',
    ];

    /**
     * @return string
     */
    public function getFormattedTotalPriceAttribute()
    {
        return formattedPrice($this->billing_total);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSellingCompany()
    {
        return $this->products->first() ? $this->products->first()->company : false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderedProducts()
    {
        return $this->hasMany(OrderProduct::class)->with('product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Document::class, 'documentable');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeOwned($query)
    {
        $user = auth()->user();

        if ($user->hasRole('admin')) {
            return $query;
        }

        $employments = $user->employments->pluck('company_id');

        return $query->where('user_id', $user->id)
            ->orWhere(function (Builder $query) use ($employments) {
                $query->whereIn('company_id', $employments);
            })
            ->orWhereHas('products', function (Builder $query) use ($employments) {
                $query->whereIn('products.company_id', $employments);
            });
    }

    public function path()
    {
        return '/admin/orders/' . $this->id;
    }

    //voyager specific
    public function getBillingTotalBrowseAttribute()
    {
        return formattedPrice($this->billing_total);
    }

    public function getBillingTaxBrowseAttribute()
    {
        return formattedPrice($this->billing_tax);
    }

    public function getBillingSubtotalBrowseAttribute()
    {
        return formattedPrice($this->billing_subtotal);
    }

    public function getBillingTotalReadAttribute()
    {
        return formattedPrice($this->billing_total);
    }

    public function getBillingTaxReadAttribute()
    {
        return formattedPrice($this->billing_tax);
    }

    public function getBillingSubtotalReadAttribute()
    {
        return formattedPrice($this->billing_subtotal);
    }
}
