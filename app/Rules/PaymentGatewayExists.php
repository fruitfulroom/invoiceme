<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PaymentGatewayExists implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $gateway = '\App\Http\Payments\\' . $value . 'Payment';

        return class_exists($gateway);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The gateway ':value' must be created first.";
    }
}
