<?php

namespace App;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Employment extends Model
{
    use LogsActivity;

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeParticipatable($query)
    {
        $user = auth()->user();

        if ($user->hasRole('admin')) {
            return $query;
        }

        $employments = $user->employments->pluck('company_id');
        $companies   = $user->companies->pluck('id');

        return $query->where('user_id', $user->id)->orWhere(function (Builder $query) use ($employments, $companies) {
            $query->whereIn('company_id', $companies->concat($employments));
        });
    }
}
