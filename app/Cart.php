<?php

namespace App;

use App\Http\Payments\CheckoutablePayment;
use Gloudemans\Shoppingcart\Cart as RealCart;

class Cart extends RealCart
{

    /**
     * @param CheckoutablePayment $payment
     * @param                     $validatedInput
     *
     * @return bool
     * @throws \Exception
     */
    public function checkout(CheckoutablePayment $payment, $validatedInput)
    {
        self::checkForAvailability();

        $company = Company::whereId($validatedInput['company_id'])->first();
        $cart    = self::getCompanyCart($company->id);

        $payment
            ->setValidatedInputs($validatedInput)
            ->setCart($cart)
            ->setCompany($company)
            ->authorizeCharge($validatedInput)
            ->proceed($cart)
            ->makeOrder()
            ->triggerAfterEvents();

        self::removeOrderedItemsFromCart($company->id);

        return true;
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getCartData($type = 'default')
    {
        return [
            'content'  => self::instance($type)->content(),
            'subtotal' => self::instance($type)->subtotal(),
            'tax'      => self::instance($type)->tax(),
            'total'    => self::instance($type)->total(),
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function checkForAvailability()
    {
        foreach (self::instance('default')->content() as $item) {
            if ( ! $item->model->hasAvailabilityFromCart()) {
                throw new \Exception('There were not enough items to proceed this checkout!');
            }
        };

        return false;
    }

    /**
     * @param $companyId
     *
     * @return mixed
     */
    public function getCompanyCart($companyId)
    {
        self::instance('dynamic-' . $companyId)->destroy();

        self::instance('default')->content()->each(function ($cartItem) use ($companyId) {
            if ($cartItem->model->company_id == $companyId) {
                self::instance('dynamic-' . $companyId)
                    ->add($cartItem->id, $cartItem->name, $cartItem->qty, $cartItem->price)
                    ->associate(Product::class);
            }
        });

        return self::instance('dynamic-' . $companyId);
    }

    /**
     * @param $companyId
     *
     * @return mixed
     */
    public function getProductsFromCompany($companyId)
    {
        $products = collect();

        foreach (self::instance('default')->content() as $product) {
            if ($product->model->company_id == $companyId) {
                $products->push($product);
            }
        };

        return $products;
    }

    /**
     * @param $companyId
     *
     * @return mixed
     * @throws \Exception
     */
    public function removeOrderedItemsFromCart($companyId)
    {
        foreach (self::getCompanyCart($companyId)->content() as $item) {
            $cartProduct = $this->getProduct($item->model);

            if (is_null($cartProduct)) {
                throw new \Exception('The product was not found in the cart!');
            }

            self::instance('default')->remove($cartProduct->rowId);
        };

        return true;
    }

    /**
     * @param Product $product
     * @param string  $type
     *
     * @return mixed
     */
    public function getProduct(Product $product, $type = 'default')
    {
        return self::instance($type)->search(function ($cartItem) use ($product) {
            return $cartItem->id === $product->id;
        })->first();
    }

    /**
     * @param Product $product
     * @param string  $type
     *
     * @return mixed
     * @throws \Exception
     */
    public function addProduct(Product $product, $type = 'default')
    {
        if ( ! $product->hasAvailabilityFromCart()) {
            throw new \Exception('There is no availability for this product!');
        }

        Cart::instance($type)
            ->add($product->id, $product->name, 1, $product->price / 100)
            ->associate(Product::class);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($product)
            ->withProperties(['type' => $type])
            ->log('added to cart');

        return $this->getProduct($product);
    }

    /**
     * @param Product $product
     * @param         $quantity
     *
     * @return mixed
     * @throws \Exception
     */
    public function updateProductQuantity(Product $product, $quantity)
    {
        $cartProduct = $this->getProduct($product);

        if (is_null($cartProduct)) {
            throw new \Exception('The product was not found in the cart!');
        }

        if ( ! $product->hasAvailabilityFromCart($quantity)) {
            throw new \Exception('There is no availability for this product!');
        }

        Cart::instance('default')->update($cartProduct->rowId, $quantity);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($product)
            ->withProperties(['quantity' => $quantity])
            ->log('updated from cart');

        return $this->getProduct($product);
    }

    /**
     * @param Product $product
     * @param         $type
     *
     * @return mixed
     * @throws \Exception
     */
    public function removeProduct(Product $product, $type = 'default')
    {
        $cartProduct = $this->getProduct($product, $type);

        if (is_null($cartProduct)) {
            throw new \Exception('The product was not found in the cart!');
        }

        Cart::instance($type)->remove($cartProduct->rowId);

        activity()
            ->causedBy(auth()->user())
            ->performedOn($product)
            ->withProperties(['type' => $type])
            ->log('removed from cart');

        return true;
    }
}
