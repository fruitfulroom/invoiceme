<?php

namespace App;

use App\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;

class TrustedCompany extends Model
{

    use LogsActivity;

    protected $guarded = [];

    protected $appends = ['formatted_available_price'];

    /**
     * Override the default boot method to register some extra stuff for every child model.
     */
    protected static function boot()
    {
        static::creating(function ($trustedCompany) {
            $trustedCompany->available = $trustedCompany->trusted_fund;
        });

        static::updating(function ($trustedCompany) {
            if ($trustedCompany->isDirty('trusted_fund')) {
                $trustedCompany->available = $trustedCompany->trusted_fund;
            }
        });

        parent::boot();
    }

    /**
     * @return string
     */
    public function getFormattedAvailablePriceAttribute()
    {
        return formattedPrice($this->available);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trusted_company()
    {
        return $this->belongsTo(Company::class, 'trusted_company_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeVisible($query)
    {
        $user = auth()->user();

        if ($user->hasRole('admin')) {
            return $query;
        }

        $query
            ->whereIn('company_id', $user->companies->pluck('id'))
            ->orWhereIn('company_id', $user->employments->pluck('company_id'))
            ->orWhereIn('trusted_company_id', $user->companies->pluck('id'))
            ->orWhereIn('trusted_company_id', $user->employments->pluck('company_id'));
    }

    //voyager specific
    public function getTrustedFundBrowseAttribute()
    {
        return formattedPrice($this->trusted_fund);
    }

    public function getTrustedFundReadAttribute()
    {
        return formattedPrice($this->trusted_fund);
    }

    public function getAvailableBrowseAttribute()
    {
        return formattedPrice($this->available);
    }

    public function getAvailableReadAttribute()
    {
        return formattedPrice($this->available);
    }

    public function getTrustedPeriodBrowseAttribute()
    {
        return $this->trusted_period . ' days';
    }

    public function getTrustedPeriodReadAttribute()
    {
        return $this->trusted_period . ' days';
    }
}
